<?php include 'components/header.php' ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlStartedSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Get Rated</li>
                        <li class="breadcrumb-item active" aria-current="page">Rating Symbols and Definitions</li>
                    </ol>
                </nav>
                <div
                    class="d-flex justify-content-md-between align-items-xl-center align-items-baseline flex-column flex-md-row">
                    <h1 class="heading-1 text-white">Rating Symbols and Definitions</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="faq-acc faq_acc_php padding-100 pt-0" id="renderHtmlRatingFaqsSectiondata">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2 class="heading-1 text-dark">Rating Symbols</h2>
            </div>
        </div>
    </div>

    <div class="accordian-style-1 style-2 accordion">
        <div class="accordion-item">
            <h2 class="accordion-header">
                <button id="btnId0" class="accordion-button" type="button" jf-ext-button-ct="1
                    bank loan ratings">
                    <span class="acc_srno">1</span>
                    <span>
                        <h1>Bank Loan Ratings</h1>
                    </span>
                </button>
            </h2>
            <div id="collapse1" class="accordion-collapse collapse show">
                <div class="accordion-body">


                    <div class="card-body">
                        <div>
                            <h3>A. Rating Definitions for Financial Instruments in Mauritius</h3>
                            <div>
                                <table class="table table-bordered" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr>
                                            <th>Rating</th>
                                            <th>Definition</th>
                                            <th>Credit Risk</th>
                                        </tr>
                                        <tr>
                                            <td>CARE MAU AAA</td>
                                            <td>Instruments with this rating are considered to have the highest
                                                degree of safety in Mauritius.</td>
                                            <td>Lowest</td>
                                        </tr>
                                        <tr>
                                            <td>CARE MAU AA</td>
                                            <td>Instruments with this rating are considered to have a high degree of
                                                safety in Mauritius.</td>
                                            <td>Very low</td>
                                        </tr>
                                        <tr>
                                            <td>CARE MAU A</td>
                                            <td>Instruments with this rating are considered to have an adequate
                                                degree of safety in Mauritius.</td>
                                            <td>Low</td>
                                        </tr>
                                        <tr>
                                            <td>CARE MAU BBB</td>
                                            <td>Instruments with this rating are considered to have a moderate
                                                degree of safety in Mauritius.</td>
                                            <td>Moderate</td>
                                        </tr>
                                        <tr>
                                            <td>CARE MAU BB</td>
                                            <td>Instruments with this rating are considered to have a moderate risk
                                                of default in Mauritius.</td>
                                            <td>Moderate-high</td>
                                        </tr>
                                        <tr>
                                            <td>CARE MAU B</td>
                                            <td>Instruments with this rating are considered to have a high risk of
                                                default in Mauritius.</td>
                                            <td>High</td>
                                        </tr>
                                        <tr>
                                            <td>CARE MAU C</td>
                                            <td>Instruments with this rating are considered to have a very high risk
                                                of default in Mauritius.</td>
                                            <td>Very high</td>
                                        </tr>
                                        <tr>
                                            <td>CARE MAU D</td>
                                            <td>Instruments with this rating are in default or are expected to be in
                                                default soon in Mauritius.</td>
                                            <td>Default or soon</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <p>Modifiers {"+" (plus) / "-"(minus)} can be used with the rating symbols for the
                                categories CARE MAU AA to CARE MAU C. The modifiers reflect the comparative standing
                                within the category.</p>
                        </div>
                        <div>
                            <h3>Rating Outlook</h3>
                            <p>The rating outlook can be ‘Positive’, ‘Stable’ or ‘Negative’.</p>
                            <p>A ‘Positive’ outlook indicates an expected upgrade in the credit ratings in the
                                medium term on account of expected positive impact on the credit risk profile of the
                                entity in the medium term.</p>
                            <p>A ‘Negative’ outlook would indicate an expected downgrade in the credit ratings in
                                the medium term on account of expected negative impact on the credit risk profile of
                                the entity in the medium term.</p>
                            <p>A ‘Stable’ outlook would indicate expected stability (or retention) of the credit
                                ratings in the medium term on account of stable credit risk profile of the entity in
                                the medium term.</p>
                        </div>
                        <div>
                            <h3>B. Rating Definitions for Short Term Debt Instruments in Mauritius</h3>
                            <table class="table table-bordered" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <th>Rating</th>
                                        <th>Definition</th>
                                        <th>Credit Risk</th>
                                    </tr>
                                    <tr>
                                        <td>CARE MAU A1</td>
                                        <td>Instruments with this rating are considered to have a very strong degree
                                            of safety regarding timely payment of financial obligations in
                                            Mauritius.</td>
                                        <td>Lowest</td>
                                    </tr>
                                    <tr>
                                        <td>CARE MAU A2</td>
                                        <td>Instruments with this rating are considered to have a strong degree of
                                            safety regarding timely payment of financial obligations in Mauritius.
                                        </td>
                                        <td>Low</td>
                                    </tr>
                                    <tr>
                                        <td>CARE MAU A3</td>
                                        <td>Instruments with this rating are considered to have a moderate degree of
                                            safety regarding timely payment of financial obligations in Mauritius.
                                            They carry higher credit risk compared to instruments rated in the two
                                            higher categories.</td>
                                        <td>Higher</td>
                                    </tr>
                                    <tr>
                                        <td>CARE MAU A4</td>
                                        <td>Instruments with this rating are considered to have a minimal degree of
                                            safety regarding timely payment of financial obligations in Mauritius.
                                            They carry very high credit risk and are susceptible to default.</td>
                                        <td>Very high</td>
                                    </tr>
                                    <tr>
                                        <td>CARE MAU D</td>
                                        <td>Instruments with this rating are in default or expected to be in default
                                            on maturity in Mauritius.</td>
                                        <td>Default or soon</td>
                                    </tr>
                                </tbody>
                            </table>
                            <p>Modifiers {"+" (plus)} can be used with the rating symbols for the categories CARE
                                MAU A1 to CARE MAU A4. The modifiers reflect the comparative standing within the
                                category.</p>
                        </div>
                    </div>



                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header">
                <button id="btnId1" class="accordion-button collapsed" type="button" jf-ext-button-ct="2
                credit rating of debt instruments">
                    <span class="acc_srno">2</span>
                    <span>
                        <h1>Credit Rating of Debt instruments</h1>
                    </span>
                </button>
            </h2>
            <div id="collapse2" class="accordion-collapse collapse">
                <div class="accordion-body">


                    <div class="card-body">
                        <div>
                            <div>
                                <h3>A. Long / Medium-term Instruments (Bonds/NCD/FD/CD/SO/CPS/RPS/L)</h3>
                                <div>
                                    <table class="table table-bordered" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <th>Rating</th>
                                                <th>Definition</th>
                                                <th>Credit Risk</th>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU AAA</td>
                                                <td>Instruments with this rating are considered to have the highest
                                                    degree of safety regarding timely servicing of financial
                                                    obligations in Mauritius.</td>
                                                <td>Lowest</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU AA</td>
                                                <td>Instruments with this rating are considered to have a high
                                                    degree of safety regarding timely servicing of financial
                                                    obligations in Mauritius.</td>
                                                <td>Very low</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU A</td>
                                                <td>Instruments with this rating are considered to have an adequate
                                                    degree of safety regarding timely servicing of financial
                                                    obligations in Mauritius.</td>
                                                <td>Low</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU BBB</td>
                                                <td>Instruments with this rating are considered to have a moderate
                                                    degree of safety regarding timely servicing of financial
                                                    obligations in Mauritius.</td>
                                                <td>Moderate</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU BB</td>
                                                <td>Instruments with this rating are considered to have a moderate
                                                    risk of default regarding timely servicing of financial
                                                    obligations in Mauritius.</td>
                                                <td>Moderate-high</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU B</td>
                                                <td>Instruments with this rating are considered to have a high risk
                                                    of default regarding timely servicing of financial obligations
                                                    in Mauritius.</td>
                                                <td>High</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU C</td>
                                                <td>Instruments with this rating are considered to have a very high
                                                    risk of default regarding timely servicing of financial
                                                    obligations in Mauritius.</td>
                                                <td>Very high</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU D</td>
                                                <td>Instruments with this rating are in default or are expected to
                                                    be in default soon in Mauritius.</td>
                                                <td>Default or soon</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <p>Modifiers {"+" (plus) / "-"(minus)} can be used with the rating symbols for the
                                    categories CARE MAU AA to CARE MAU C. The modifiers reflect the comparative
                                    standing within the category.</p>
                                <div>
                                    <h3>Rating Outlook</h3>
                                    <p>The rating outlook can be ‘Positive’, ‘Stable’ or ‘Negative’.</p>
                                    <p>A ‘Positive’ outlook indicates an expected upgrade in the credit ratings in
                                        the medium term on account of expected positive impact on the credit risk
                                        profile of the entity in the medium term.</p>
                                    <p>A ‘Negative’ outlook would indicate an expected downgrade in the credit
                                        ratings in the medium term on account of expected negative impact on the
                                        credit risk profile of the entity in the medium term.</p>
                                    <p>A ‘Stable’ outlook would indicate expected stability (or retention) of the
                                        credit ratings in the medium term on account of the stable credit risk
                                        profile of the entity in the medium term.</p>
                                    <h3>Rating Symbols and Definitions for Short Term Debt Instruments
                                        (NCD/CP/CD/FD/SO/CPS/RPS/L)</h3>
                                    <table class="table table-bordered" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <th>Symbols</th>
                                                <th>Definition</th>
                                                <th>Credit Risk</th>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU A1</td>
                                                <td>Instruments with this rating are considered to have a very
                                                    strong degree of safety regarding timely payment of financial
                                                    obligations in Mauritius.</td>
                                                <td>Lowest</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU A2</td>
                                                <td>Instruments with this rating are considered to have a strong
                                                    degree of safety regarding timely payment of financial
                                                    obligations in Mauritius.</td>
                                                <td>Low</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU A3</td>
                                                <td>Instruments with this rating are considered to have a moderate
                                                    degree of safety regarding timely payment of financial
                                                    obligations in Mauritius. They carry higher credit risk compared
                                                    to instruments rated in the two higher categories.</td>
                                                <td>Higher</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU A4</td>
                                                <td>Instruments with this rating are considered to have a minimal
                                                    degree of safety regarding timely payment of financial
                                                    obligations in Mauritius. They carry very high credit risk and
                                                    are susceptible to default.</td>
                                                <td>Very high</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU D</td>
                                                <td>Instruments with this rating are in default or expected to be in
                                                    default on maturity in Mauritius.</td>
                                                <td>Default or soon</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p>Modifiers {"+" (plus) / "-"(minus)} can be used with the rating symbols for
                                        the categories CARE MAU AA to CARE MAU C. The modifiers reflect the
                                        comparative standing within the category.</p>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header">
                <button id="btnId2" class="accordion-button collapsed" type="button" jf-ext-button-ct="3
            issuer rating - rating symbols and definition">
                    <span class="acc_srno">3</span>
                    <span>
                        <h1>Issuer Rating - Rating Symbols and Definition</h1>
                    </span>
                </button>
            </h2>
            <div id="collapse3" class="accordion-collapse collapse">
                <div class="accordion-body">


                    <div class="card-body">
                        <div>
                            <div>
                                <h3>A. Long / Medium-term Instruments (Bonds/NCD/FD/CD/SO/CPS/RPS/L)</h3>
                                <div>
                                    <table class="table table-bordered" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                            <tr>
                                                <th>Symbols</th>
                                                <th>Definition</th>
                                                <th>Credit Risk</th>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU AAA (Is)</td>
                                                <td>Issuers with this rating are considered to have the highest
                                                    degree of safety regarding timely servicing of financial
                                                    obligations in Mauritius.</td>
                                                <td>Lowest</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU AA (Is)</td>
                                                <td>Issuers with this rating are considered to have a high degree of
                                                    safety regarding timely servicing of financial obligations in
                                                    Mauritius.</td>
                                                <td>Very low</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU A (Is)</td>
                                                <td>Issuers with this rating are considered to have an adequate
                                                    degree of safety regarding timely servicing of financial
                                                    obligations in Mauritius.</td>
                                                <td>Low</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU BBB (Is)</td>
                                                <td>Issuers with this rating are considered to have a moderate
                                                    degree of safety regarding timely servicing of financial
                                                    obligations in Mauritius.</td>
                                                <td>Moderate</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU BB (Is)</td>
                                                <td>Issuers with this rating are considered to have a moderate risk
                                                    of default regarding timely servicing of financial obligations
                                                    in Mauritius.</td>
                                                <td>Moderate-high</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU B (Is)</td>
                                                <td>Issuers with this rating are considered to have a high risk of
                                                    default regarding timely servicing of financial obligations in
                                                    Mauritius.</td>
                                                <td>High</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU C (Is)</td>
                                                <td>Issuers with this rating are considered to have a very high risk
                                                    of default regarding timely servicing of financial obligations
                                                    in Mauritius.</td>
                                                <td>Very high</td>
                                            </tr>
                                            <tr>
                                                <td>CARE MAU D (Is)</td>
                                                <td>Issuers with this rating are in default or are expected to be in
                                                    default soon in Mauritius.</td>
                                                <td>Default or soon</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <p>CRAF’s Issuer Rating (CIR) reflects the overall credit risk of the issuer.
                                        The rating scale has been aligned with the long-term instrument rating scale
                                        ranging from AAA(Is) (Highest Safety) to D(Is) (Default). 'Is' suffix
                                        indicates 'Issuer Rating'</p>
                                    <p>Modifiers {"+" (plus) / "-"(minus)} can be used with the rating symbols for
                                        the categories CARE MAU A1 to CARE MAU A4. The modifiers reflect the
                                        comparative standing within the category</p>
                                </div>
                                <div>
                                    <h3>Rating Outlook</h3>
                                    <p>The rating outlook can be ‘Positive’, ‘Stable’ or ‘Negative’.</p>
                                    <p>A ‘Positive’ outlook indicates an expected upgrade in the credit ratings in
                                        the medium term on account of expected positive impact on the credit risk
                                        profile of the entity in the medium term.</p>
                                    <p>A ‘Negative’ outlook would indicate an expected downgrade in the credit
                                        ratings in the medium term on account of expected negative impact on the
                                        credit risk profile of the entity in the medium term.</p>
                                    <p>A ‘Stable’ outlook would indicate expected stability (or retention) of the
                                        credit ratings in the medium term on account of stable credit risk profile
                                        of the entity in the medium term.</p>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
        </div>
        <div class="accordion-item">
            <h2 class="accordion-header">
                <button id="btnId3" class="accordion-button collapsed" type="button">
                    <span class="acc_srno">4</span>
                    <span>
                        <h1>SME</h1>
                    </span>
                </button>
            </h2>
            <div id="collapse4" class="accordion-collapse collapse">
                <div class="accordion-body">


                    <div class="card-body">
                        <div>
                            <table class="table table-bordered" cellspacing="0" cellpadding="0" border="0">
                                <tbody>
                                    <tr>
                                        <th>SME Rating</th>
                                        <th>Definition</th>
                                    </tr>
                                    <tr>
                                        <td>CARE MAU SME 1</td>
                                        <td>The level of creditworthiness of an SME, adjudged in relation to other
                                            SMEs is the Highest.</td>
                                    </tr>
                                    <tr>
                                        <td>CARE MAU SME 2</td>
                                        <td>The level of creditworthiness of an SME, adjudged in relation to other
                                            SMEs is High.</td>
                                    </tr>
                                    <tr>
                                        <td>CARE MAU SME 3</td>
                                        <td>The level of creditworthiness of an SME, adjudged in relation to other
                                            SMEs is Above Average.</td>
                                    </tr>
                                    <tr>
                                        <td>CARE MAU SME 4</td>
                                        <td>The level of creditworthiness of an SME, adjudged in relation to other
                                            SMEs is Average.</td>
                                    </tr>
                                    <tr>
                                        <td>CARE MAU SME 5</td>
                                        <td>The level of creditworthiness of an SME, adjudged in relation to other
                                            SMEs is Below Average.</td>
                                    </tr>
                                    <tr>
                                        <td>CARE MAU SME 6</td>
                                        <td>The level of creditworthiness of an SME, adjudged in relation to other
                                            SMEs is Inadequate.</td>
                                    </tr>
                                    <tr>
                                        <td>CARE MAU SME 7</td>
                                        <td>The level of creditworthiness of an SME, adjudged in relation to other
                                            SMEs is Poor.</td>
                                    </tr>
                                    <tr>
                                        <td>CARE MAU SME 8</td>
                                        <td>The level of creditworthiness of an SME, adjudged in relation to other
                                            SMEs is Lowest. Such entities may also be in default.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>



                </div>
            </div>
        </div>

        <div class="accordion-item">
            <h2 class="accordion-header">
                <button id="btnId4" class="accordion-button collapsed" type="button">
                    <span class="acc_srno">5</span>
                    <span>
                        <h1>Collective Investment Scheme - Symbols and Definition</h1>
                    </span>
                </button>
            </h2>
            <div id="collapse5" class="accordion-collapse collapse">
                <div class="accordion-body">


                    <div>
                        <table class="table table-bordered" cellspacing="0" cellpadding="0" border="0">
                            <tbody>
                                <tr>
                                    <th>CIS Rating</th>
                                    <th>Definition</th>
                                </tr>
                                <tr>
                                    <td>CARE MAU 1 (CIS)</td>
                                    <td>Schemes carrying this rating are considered to be very strong, with high
                                        likelihood of achieving their objectives and meeting the obligations to
                                        investors.</td>
                                </tr>
                                <tr>
                                    <td>CARE MAU 2 (CIS)</td>
                                    <td>Schemes carrying this rating are considered to be strong, with adequate
                                        likelihood of achieving their objectives and meeting the obligations to
                                        investors. They are rated lower than CARE MAU 1 (CIS) rated schemes because
                                        of relatively higher risk.</td>
                                </tr>
                                <tr>
                                    <td>CARE MAU 3 (CIS)</td>
                                    <td>Such schemes are considered to have adequate strengths for achieving their
                                        objectives and meeting the obligations to investors. They are considered to
                                        be investment grade.</td>
                                </tr>
                                <tr>
                                    <td>CARE MAU 4 (CIS)</td>
                                    <td>Schemes carrying this rating are considered to have inadequate capability to
                                        achieve their objectives and meet the obligations to investors. They are
                                        considered to be of speculative grade.</td>
                                </tr>
                                <tr>
                                    <td>CARE MAU 5 (CIS)</td>
                                    <td>Such schemes are considered weak and are unlikely to achieve their
                                        objectives and meet their obligations to investors. They have either failed
                                        or are likely to do so in the near future.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>