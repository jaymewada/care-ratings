<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlInsightsBreadcrumSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">INSIGHTS</a></li>
                        <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">RATINGS</a></li> -->
                        <li class="breadcrumb-item active" aria-current="page">Annual Reports</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">Annual Reports</h1>
            </div>
        </div>
    </div>
</section>

<section class="AnnualReport-relase-sec padding-100 yearDropDown-main slick-dot-none">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-12 col-lg-12 mb-5">
                <div class="d-flex justify-content-md-between align-items-md-center flex-column flex-md-row">
                    <div>
                        <h2 class="heading-2 text-dark">Annual Reports</h2>
                    </div>
                    <div class="mt-xl-3 d-flex  mb-lg-0 ">
                        <div class="d-flex flex-column justify-content-between h-100 cardContainer me-2">
                            <div class="slider-arrows me-0">
                                <span class="prev-btn slick-arrow slick-hidden" aria-disabled="true" tabindex="-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-chevron-left" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z">
                                        </path>
                                    </svg>
                                </span>
                                <span class="next-btn slick-arrow slick-hidden" aria-disabled="true" tabindex="-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-chevron-right" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z">
                                        </path>
                                    </svg>
                                </span>
                            </div>
                        </div>
                        <div class="d-flex sasifb flex-row-reverse ">
                            <div class="yearToggle mb-0" style="margin-left:5px;" id="renderHtmlAnnualReportYeardata">

                                <select class="empInput form-control" name="Year_Id" id="Year_ann_Id"
                                    style="border: 1px solid #858796;" jf-ext-cache-id="10">
                                    <option value="2022-2023" id="2022-2023">2022-2023</option>
                                    <option value="2021-2022" id="2021-2022">2021-2022</option>
                                    <option value="2020-2021" id="2020-2021">2020-2021</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row gx-5">
            <div class="col-md-12  col-lg-12" id="renderHtmlAnnualReportSection">
                <div class="row AnnualReport-relase-slider sliderGap-30 slick-initialized slick-slider">
                    <div class="slick-list draggable">
                        <div class="slick-track"
                            style="opacity: 1; width: 342px; transform: translate3d(0px, 0px, 0px);">
                            <div class="item latest slick-slide slick-current slick-active" data-slick-index="0"
                                aria-hidden="false" tabindex="0" style="width: 297px;">
                                <div class="card card-style-1">
                                    <div class="card-body">
                                        <!--<span class="text-grey text-small">1970-01-01</span>-->
                                        <p class="heading-3 text-white mb-2 mt-2 font-light">Annual Report 2022-2023</p>
                                        <a content-type="application/pdf"
                                            href="https://www.careratingsafrica.com/Uploads/newsfiles/FinancialReports/1698993508_1688367339_CARE Ratings Africa Private Limited Annual Accounts FY2023.pdf"
                                            class="btn btn-link primary p-0 d-block text-start mt-4" target="_blank"
                                            tabindex="0">DOWNLOAD NOW</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>