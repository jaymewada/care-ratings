<?php include "components/header.php" ?>
<section class="CareEdgewebinars1-sec padding-100 yearDropDown-main slick-dot-none">

    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-11 mb-5">
                <div class="d-flex justify-content-md-between align-items-md-center flex-column flex-md-row">
                    <div>
                        <h3 class="heading-1 text-black">Media</h3>
                    </div>
                    <div class="d-flex mb-lg-0">
                        <div class="d-flex flex-column justify-content-between h-100 cardContainer me-2">
                            <div class="slider-arrows me-0">
                                <span class="prev-btn slick-arrow slick-hidden" aria-disabled="true" tabindex="-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-chevron-left" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z">
                                        </path>
                                    </svg>
                                </span>
                                <span class="next-btn slick-arrow slick-hidden" aria-disabled="true" tabindex="-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-chevron-right" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z">
                                        </path>
                                    </svg>
                                </span>
                            </div>
                        </div>

                        <div class="custom-dropdown filter-dropdown  dropdown-menu-outiline  dropdown mb-lg-0 ">
                            <div class="d-flex sasifb flex-row-reverse ">

                                <div class="yearToggle mb-0" style="margin-left:5px;"
                                    id="renderHtmlCareEdgeMediaYeardata">
                                    <select class="empInput form-control" name="Year_Id" id="media_Year_Id"
                                        style="border: 1px solid #858796;" jf-ext-cache-id="10">
                                        <option value="2023" id="2023">2023</option>
                                        <option value="1970" id="1970">1970</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid" id="renderHtmlCareEdgeMediaSectiondata">
        <div class="CareEdgewebinars1-slider left-space-sldier overflow-hidden mb-2 slick-initialized slick-slider">
            <div class="slick-list draggable">
                <div class="slick-track" style="opacity: 1; width: 1137px; transform: translate3d(0px, 0px, 0px);">
                    <div class="item slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false"
                        tabindex="0" style="width: 329px;">
                        <a href="null" tabindex="0">
                            <div class="card card-style-2 ">
                                <div
                                    class="card-image position-relative d-flex justify-content-center align-items-center">
                                    <img src="https://www.careratingsafrica.com/upload/video/Thumbnail_Image/1701439391_19 domain script.xml"
                                        class="card-img-top img-fluid" alt="...">
                                    <div class="small animayion-none">

                                    </div>
                                </div>
                                <div class="card-body border-0 p-0">
                                    <h3 class="heading-2 mt-4">test</h3>
                                    <span>null</span>
                                    <p>01 Dec 2023</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="item slick-slide slick-active" data-slick-index="1" aria-hidden="false" tabindex="0"
                        style="width: 329px;">
                        <a href="upload/video/pdf/1698669071_11. Media Coverage Saurav MIOD Event (2).pdf" tabindex="0">
                            <div class="card card-style-2 ">
                                <div
                                    class="card-image position-relative d-flex justify-content-center align-items-center">
                                    <img src="https://www.careratingsafrica.com/upload/video/Thumbnail_Image/1698669071_media2.png"
                                        class="card-img-top img-fluid" alt="...">
                                    <div class="small animayion-none">

                                    </div>
                                </div>
                                <div class="card-body border-0 p-0">

                                    <h3 class="heading-2 mt-4">Saurav MIOD Event</h3>
                                    <span>
                                        <p>"Corporate Governance plays a vital role in determining an organization's
                                            willingness to meet financial obligations"</p>
                                    </span>
                                    <p>30 Oct 2023</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="item slick-slide slick-active" data-slick-index="2" aria-hidden="false" tabindex="0"
                        style="width: 329px;">
                        <a href="upload/video/pdf/1698666949_11. Media Coverage - Saurav on ESG Framework.pdf"
                            tabindex="0">
                            <div class="card card-style-2 ">
                                <div
                                    class="card-image position-relative d-flex justify-content-center align-items-center">
                                    <img src="https://www.careratingsafrica.com/upload/video/Thumbnail_Image/1698666949_media1.png"
                                        class="card-img-top img-fluid" alt="...">
                                    <div class="small animayion-none">

                                    </div>
                                </div>
                                <div class="card-body border-0 p-0">

                                    <h3 class="heading-2 mt-4">Saurav on ESG Framework</h3>
                                    <span>
                                        <p>Saurav on ESG Framework</p>
                                    </span>
                                    <p>30 Oct 2023</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include "components/footer.php" ?>