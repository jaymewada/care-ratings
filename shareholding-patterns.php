<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlInsightsBreadcrumSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">INSIGHTS</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Shareholding Patterns</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">Shareholding Patterns</h1>
            </div>
        </div>
    </div>
</section>

<section class="padding-100 design-vec-bg">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-11" id="renderHtmlAboutUsOverViewSectiondata">
                <div class="row align-items-center">
                    <div class="col-md-12 col-xl-12">
                        <h3 class="heading-1 text-black ">Shareholders</h3>
                        <br class="d-none d-xl-block">
                        <hr class="style-1 mt-xl-5 mb-xl-5">
                        <div class="heading-6 para-line-height text-justify">
                            <p><strong>CARE Ratings Limited, India</strong></p>
                            <p>Since its inception in 1993, CareEdge Ratings (CARE Ratings Ltd) has established itself
                                as one of India’s leading credit rating agencies.</p>
                            <p>CareEdge Ratings provides ratings for a variety of industries, such as manufacturing,
                                infrastructure, and the financial sector, which includes banking, and non-financial
                                services. The company has played a pivotal role in the development of bank debt and
                                capital market instruments, such as commercial papers, corporate bonds and debentures,
                                and structured credit.
                                With three decades of experience in the analytics business and a deep understanding of
                                the operations of various industries and sectors, the company is in a sound position to
                                offer itself as a knowledge hub and publishes insightful reports on the Indian and
                                global economy, and industry research papers regularly.</p>
                            <p>The wholly-owned subsidiaries of CareEdge Ratings include CARE Advisory Research &amp;
                                Training Limited and CARE Risk Solutions Private Limited.</p>
                            <p>CARE Ratings (Africa) Private Limited and CARE Ratings Nepal Ltd are subsidiaries in
                                Mauritius and Nepal, respectively.
                                CareEdge Ratings is also a partner in ARC Ratings, an international credit rating
                                agency. The business has a strategic alliance with the Japan Credit Ratings Agency (JCR)
                                and a memorandum of understanding with the Russian credit rating agency ACRA.</p>
                            <p><strong>African Development Bank</strong> </p>
                            <p>The African Development Bank (AfDB) Group is a regional multilateral development finance
                                institution established to contribute to the economic development and social progress of
                                African countries that are the institution’s Regional Member Countries (RMCs). The AfDB
                                was founded following an agreement signed by member states on August 14, 1963, in
                                Khartoum, Sudan, which became effective on September 10, 1964. The AfDB comprises three
                                entities: the African Development Bank (ADB), the African Development Fund (ADF) and the
                                Nigeria Trust Fund (NTF). As the premier development finance institution on the
                                continent, the AfDB’s mission is to help reduce poverty, improve living conditions for
                                Africans and mobilize resources for the continent’s economic and social development. The
                                AfDB headquarters is officially in Abidjan, Côte d’Ivoire.</p>


                            <p><strong>MCB Equity Fund</strong></p>
                            <p>MCB Equity Fund is the private equity arm of MCB Group, the largest banking institution
                                in Mauritius and sole shareholder of the fund. The fund is managed by MCB Capital
                                Partners, a wholly owned subsidiary of MCB Capital Markets. It is an evergreen fund with
                                a committed capital of USD 100 million, MCB Equity Fund provides equity and quasi equity
                                to established and fast growing businesses across Africa. </p>

                            <p><strong>SBM (NFC) Holdings Limited (SNHL)</strong></p>
                            <p>SNHL is part of State Bank of Mauritius Group with more than a decade of existence.
                                Non-Banking Financial Cluster has been operating for more than 15 years and provides
                                services in Asset Management, Securities Brokerage, Private Equity, Factoring,
                                Investment Advisory, Registry, Fund Management and Insurance Agency. </p>
                            <a href="/uploads/newsfiles/CRAF's Shareholding Pattern.pdf" target="_blank">Share Holding
                                Pattern</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>