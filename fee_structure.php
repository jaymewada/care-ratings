<?php include 'components/header.php' ?>

<section class="capabilities-sec regulatory-disclosure bg-grey capabities-new-updated overflow-hidden"
    style="background-image:url(images/Capabilities.png);" id="renderHtmlRatingResourcesSectiondata">
    <div class="container-fluid left-space pe-0">
        <div class="row">
            <div class="col-md-12">
                <h3 class="heading-1 text-black">Fee Structure Africa</h3>
            </div>
        </div>
        <div class="accordian-style-1 mt-5 row g-0">
            <div class="col-md-5 col-xl-3">
                <div class="accordian-title-box">
                    <span class="acc-title text-center"></span>
                    <span class="acc-count">1</span>
                </div>
            </div>
            <div class="col-md-12 col-xl-9">
                <div class="accordion">





                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId0" class="accordion-button" type="button"
                                jf-ext-button-ct="fee structure africa">
                                Fee Structure Africa
                            </button>
                        </h2>
                        <div id="collapse0" class="accordion-collapse collapse show">
                            <div class="accordion-body">
                                <p>CARE Ratings Africa Private Limited (CRAF) follows a transparent pricing mechanism
                                    for undertaking rating of various products. The fee structure is usually computed as
                                    a percentage of debt amount to be rated.<br>
                                    The general nature of compensation arrangements that CRAF has with rated entities is
                                    as under:<br>
                                    1) Before crystallising any rating agreement, the applicable fees are finalised with
                                    the entity to be rated.<br>
                                    2) The credit rating fees generally have two components: Initial Rating Fee (IRF)
                                    and Annual Surveillance Fee (ASF). The IRF is charged at the time of assignment of
                                    initial rating and the ASF is charged during the time that the rating remains
                                    outstanding.<br>
                                    3) While the fees are generally correlated to the debt to be rated, it also depends
                                    upon the industry type, debt facility break-up and complexity of the assignment.<br>
                                    4) Only the CEO &amp; business development team is involved in the finalisation of
                                    the fees for the rating assignments.<br>
                                    5) Rating analysts are not part of the mandate origination and fee discussions.
                                    Thus, the fees charged for the rating assignments is not disclosed to the rating
                                    analysts.<br>
                                    6) The amount of rating fees is not a determinant of rating analysis or rating
                                    outcome by CARE Ratings Africa in any manner whatsoever.<br>
                                    7) CARE Ratings reserves the right to charge the rating fees within the general
                                    nature of compensation arrangements with rated clients.<br>
                                    8) In certain cases, CARE Ratings may be appointed and compensated for the rating
                                    assignments by the investors or parties other than the issuer for the same.</p>

                                <p>Initial Rating Fee<br>
                                    ⦁&nbsp;&nbsp; &nbsp;Bonds/Debentures -0.15% of the issue amount.<br>
                                    ⦁&nbsp;&nbsp; &nbsp;Commercial Paper - 0.15% of the issue amount.<br>
                                    ⦁&nbsp;&nbsp; &nbsp;Issuer Rating - 0.15% of all the outstanding debts as on last
                                    balance sheet date.<br>
                                    ⦁&nbsp;&nbsp; &nbsp;Bank facilities - 0.15% of the sanctioned amount.<br>
                                    ⦁&nbsp;&nbsp; &nbsp;Fixed Deposits - 0.15% of the amount of Fixed Deposits.<br>
                                    ⦁&nbsp;&nbsp; &nbsp;SME Gradings &nbsp;- MUR 75,000 (up to MUR 20 million turnover),
                                    MUR 100,000 (MUR 20 million to MUR 35 million turnover) and MUR 125,000 (greater
                                    than MUR 35 million turnover)</p>

                                <p><br>
                                    Annual Surveillance Fee:<br>
                                    ⦁&nbsp;&nbsp; &nbsp; FD/Debentures/CP/Bank Facilities: 0.075% of the amount
                                    outstanding under the rated instrument.</p>

                                <p>⦁&nbsp;&nbsp; &nbsp;Issuer Rating&nbsp;- 0.075% of the amount outstanding under the
                                    rated instrument.</p>

                                <p>⦁&nbsp;&nbsp; &nbsp;Credit Reports: Fees applicable will depend on the scope and
                                    coverage of each report and can be obtained on specific request.<br>
                                    Notes:<br>
                                    ⦁&nbsp;&nbsp; &nbsp;Rating fees are computed separately on each instrument
                                    issued.<br>
                                    ⦁&nbsp;&nbsp; &nbsp;Issuers are liable to pay rating fees, regardless of whether
                                    they accept CRAF’s rating or not.&nbsp;<br>
                                    ⦁&nbsp;&nbsp; &nbsp;Full rating fee is to be paid upfront.<br>
                                    ⦁&nbsp;&nbsp; &nbsp;CRAF sometimes offers discounts on the above Fee structure
                                    keeping in view various factors such as quantum of debt to be rated, group
                                    structures, existing relationships and market dynamics.<br>
                                    ⦁&nbsp;&nbsp; &nbsp;VAT will be charged extra as applicable.<br>
                                    ⦁&nbsp;&nbsp; &nbsp;CRAF reserves the right to make changes in the fee structure at
                                    any time.<br>
                                    ⦁&nbsp;&nbsp; &nbsp;CRAF does not charge any fee to its clients or to the investors
                                    for disseminating / publishing ratings and Press <strong>Releases on its website <a
                                            href="http://www.careratingsafrica.com/">www.careratingsafrica.com</a></strong>
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>