<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlInsightsBreadcrumSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">CAPABILITIES</a></li>
                        <li class="breadcrumb-item active" aria-current="page">CORPORATE SECTOR</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">CORPORATE SECTOR</h1>
            </div>
        </div>
    </div>
</section>

<section class="padding-100 design-vec-bg">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-11" id="renderHtmlInsightsOverViewSectiondata">



                <div class="row align-items-center">
                    <div class="col-md-6">
                        <h3 class="heading-1 text-black ">Overview</h3>
                        <br class="d-none d-xl-block">
                        <hr class="style-1 mt-xl-5 mb-xl-5">
                        <p class="heading-3 para-line-height"></p>
                        <p>CRAF’s credit ratings are its opinion on the relative ability and willingness of the issuer
                            to meet the debt service obligations as and when they arise. Rating determination is a
                            matter of experienced and holistic judgement, based on the relevant quantitative and
                            qualitative factors affecting the credit quality of the issuer. CRAF has based its
                            ratings/outlooks on information obtained from sources believed by it to be accurate and
                            reliable. Within the corporate sector, CRAF rates debt instruments and bank facilities of
                            manufacturing entities, service entities, trading entities, PSUs etc.</p>
                        <p>CRAF’s credit analysis of an entity begins with a review of the economy, followed by the
                            industry in which the entity operates, along with an assessment of the business risk factors
                            specific to the entity. This is followed by an assessment of the financial risk factors and
                            quality of management of the entity. For project stage entities/ entities undertaking large
                            projects, CRAF also analyses project risk for arriving at the entity’s rating.
                            Over-and-above this, CRAF has defined several sector-specific methodologies to capture
                            nuances of the respective sectors, which are also factored in the entity’s credit
                            assessment. CRAF’s offerings for the corporate sector include</p>
                        <ul>
                            <li>Bank Loan Ratings</li>
                            <li>Debt Instrument Ratings</li>
                            <li>Long-Term Instruments Like
                                <ul>
                                    <li>Bonds</li>
                                    <li>Non-Convertible Debentures (NCDs)</li>
                                    <li>Preference Shares</li>
                                </ul>
                            </li>
                            <li>Medium-Term Instruments Like
                                <ul>
                                    <li>Fixed Deposits (FDs)</li>
                                </ul>
                            </li>
                            <li>Short-Term Instruments Like
                                <ul>
                                    <li>Money Market Instruments (MMIs)</li>
                                    <li>Commercial Papers (CPs)</li>
                                    <li>Short-Term NCDs</li>
                                </ul>
                            </li>
                            <p></p>
                        </ul>
                    </div>
                    <div class="col-md-5 offset-xl-1">
                        <img src="https://www.careratingsafrica.com/storage/app/admin/images/about-profile_1663342035_1686918343.jpeg"
                            class="card-img-top img-fluid" alt="...">
                        <!--<img src="https://www.careratingsafrica.com/uploads/newsfiles/app/admin/images/about-profile_1663342035_1686918343.jpeg" class="card-img-top img-fluid" alt="...">-->
                    </div>














                </div>
            </div>
        </div>
    </div>
</section>

<section class="padding-100 bg-primary grey-patch mb-0 mt-lg-5">
    <div class="container-fluid">

        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="heading-1 text-white">Other Capabilities</h3>
                    </div>
                    <div class="col-md-4">
                        <div class="slider-arrows white arrow-2 justify-content-end">
                            <span class="prev-btn slick-arrow slick-disabled" style="" aria-disabled="true">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                    class="bi bi-chevron-left" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                        d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z">
                                    </path>
                                </svg>
                            </span>
                            <span class="next-btn slick-arrow" style="" aria-disabled="false">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                    class="bi bi-chevron-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                        d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z">
                                    </path>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr class="style-2">
                    </div>
                </div>
            </div>
        </div>

        <div class="other-capabilities-slider left-space-sldier">
            <?php for ($x = 0; $x <= 10; $x++) { ?>
            <div>
                <a href="./financial-sector" tabindex="0">
                    <div class="card card-style-1">
                        <div class="d-flex justify-content-between">
                            <span class="text-grey text-small">CareEdge Ratings</span>
                            <span class="arrow-link small">
                                <svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                    viewBox="0 0 15.28 15.256">
                                    <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                        <line id="Line_60" data-name="Line 60" y1="13.182"
                                            transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                            stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></line>
                                        <path id="Path_51" data-name="Path 51"
                                            d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                            transform="translate(-7.26 -3.929)" fill="none" stroke="#fff"
                                            stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></path>
                                    </g>
                                </svg>
                            </span>
                        </div>
                        <div class="card-body">
                            <p class="heading-3 text-white mb-3 font-light">Financial Sector</p>
                            <p class=" text-grey mb-3 font-light">CARE Ratings rates debt instruments/ bank
                                facilities of a large number of companies in the financial sector, including banks,
                                non-banking finance companies, housing finance companies, financial institutions
                                etc.</p>
                        </div>
                    </div>
                </a>
            </div>
            <?php } ?>
        </div>
</section>

<?php include "components/footer.php" ?>