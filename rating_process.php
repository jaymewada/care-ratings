<?php include 'components/header.php' ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlStartedSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item" aria-current="page">Get Rated</li>
                        <li class="breadcrumb-item active" aria-current="page">Rating Process</li>
                    </ol>
                </nav>
                <div
                    class="d-flex justify-content-md-between align-items-xl-center align-items-baseline flex-column flex-md-row">
                    <h1 class="heading-1 text-white">Rating Process</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="capabilities-sec regulatory-disclosure bg-grey capabities-new-updated overflow-hidden"
    style="background-image:url(images/Capabilities.png);" id="renderHtmlRatingResourcesSectiondata">

    <div class="container-fluid left-space pe-0">
        <div class="row">
            <div class="col-md-12">
                <h3 class="heading-1 text-black">Rating Process</h3>
            </div>
        </div>
        <div class="accordian-style-1 mt-5 row g-0">
            <div class="col-md-5 col-xl-3">
                <div class="accordian-title-box">
                    <span class="acc-title text-center"></span>
                    <span class="acc-count">1</span>
                </div>
            </div>
            <div class="col-md-12 col-xl-9">
                <div class="accordion">
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId0" class="accordion-button" type="button"
                                jf-ext-button-ct="rating process">
                                Rating Process
                            </button>
                        </h2>
                        <div id="collapse0" class="accordion-collapse collapse show">
                            <div class="accordion-body">
                                <p>The rating process takes about two to three weeks, depending on the complexity of
                                    the assignment and the flow of information from the client. Ratings are assigned
                                    by the Rating Committee.</p>

                                <p><img src="./public/frontend-assets/images/rating-process.png"><br>
                                    <br>
                                    For detailed write-up on Rating Process please &nbsp;<a
                                        href="./upload/pdf/get-rated/CareEdge - Rating Process.pdf"
                                        target="_blank">click here</a>
                                </p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>