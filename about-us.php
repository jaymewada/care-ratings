<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item active">About Us</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">About Us</h1>
            </div>
        </div>
    </div>
</section>

<section class="padding-100 design-vec-bg">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-11" id="renderHtmlAboutUsOverViewSectiondata">
                <div class="row align-items-center">
                    <div class="col-md-12 col-xl-12">
                        <h3 class="heading-1 text-black ">Company Profile</h3>
                        <br class="d-none d-xl-block">
                        <hr class="style-1 mt-xl-5 mb-xl-5">
                        <div class="heading-6 para-line-height text-justify content-scroll">
                            <p>CARE Ratings (Africa) Private Limited (CRAF) is incorporated in Mauritius to provide
                                credit ratings and related services in Mauritius and other geographies in Africa. </p>
                            <p>CRAF is the ﬁrst credit rating agency to be licensed by the Financial Services Commission
                                of Mauritius in May 2015. It is also recognised by the Bank of Mauritius as External
                                Credit Assessment Institution (ECAI) since May 2016. CRAF is also licensed by the
                                Capital Markets Authority of Kenya to operate as a Credit Rating Agency in Kenya. CRAF
                                intends to expand across other geographies in Africa with Mauritius as its hub of
                                operations.</p>
                            <p>CRAF's shareholders are CARE Ratings Limited (CareEdge Group), African Development Bank
                                (AfDB), MCB Equity Fund, (MEF) and SBM (NFC) Holdings Limited (SNHL). The mix enables
                                the entity to have stronger brand recognition in the African continent.</p>
                            <p>CRAF provides Ratings of various instruments such as Bonds, Money Market Instruments,
                                Bank Deposits, Structured Finance and Bank Facilities. CRAF also does Issuer Rating
                                (Banks, Corporates, NBDTIs &amp; Insurance companies), ESG Ratings &amp; Gradings,
                                Second Party Opinion Reports, channel partner evaluation and SME ratings. </p>
                            <p>CRAF gets its technical support in the areas such as rating systems and procedures,
                                methodologies, etc. from CARE Ratings Limited on an ongoing basis. Technical support
                                ensures that CRAF has adequate resources to provide high-quality credit opinions on an
                                ongoing basis. CRAF is committed to the establishment of the highest standards of
                                professional quality and integrity. In line with this, CRAF has set up its Rating
                                Committee with full-time members comprising officials from CARE and CRAF. The company
                                also has a panel of experts from Mauritius and African Development Bank group to assist
                                the Rating Committee by providing valuable inputs about the economy, industry and
                                corporate environment in Mauritius &amp; Africa.</p>
                            <p>Within a short span of seven years, CRAF has brought about a perceptible, qualitative
                                difference to the debt market and banking sector in Mauritius. CRAF works extensively
                                with the Regulators and Stakeholders to increase the awareness about the concept of
                                Credit Rating among lenders, investors and issuers and clear understanding of the
                                benefits from such Ratings. Through its top-quality research and inputs based on the
                                experience of its parent’s operations, CRAF has also helped in policy making. </p>
                            <p>Today, the rated segment of Mauritius debt market (capital market instruments &amp; bank
                                facilities) has grown from “ZERO” in 2016 to approx. “USD 3 billion” in 2023, an
                                indicator of strong investors’ faith and belief in CRAF’s Rating methodology and
                                approach in assessing risk. </p>
                            <p>CRAF is a member of COMESA Business Council and full-fledged associate member of
                                International Capital Markets Association, Zurich for providing gradings and Second
                                Party Opinion on green bonds / social bonds / SLB / Sustainability Bonds.</p>
                            <p><strong>Outstanding Ratings:</strong> - Following the grant of license by FSC in 2015,
                                CRAF has 56 outstanding ratings and rated debt of USD 3 billion (denominated in MUR, USD
                                &amp; Euro) as on June 30, 2023. </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="padding-100 bg-grey-100  vision-mission-sec">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <h3 class="heading-1 text-black ">Vision, Mission and Values</h3>
                        <hr class="style-1 mt-xl-5 mb-xl-5">
                    </div>
                </div>
                <div class="row gx-5 hang-boxes Vision-mission-sldier " id="renderHtmlMissionVissionSection">
                    <div class="col-md-4">
                        <a tabindex="0">
                            <div class="card card-style-1 px-3">
                                <div class="card-body ">
                                    <p class="heading-3 text-white mb-3">Our Vision</p>
                                    <span class="text-white heading-6 height-f">
                                        <p>A Global Research &amp; Analytics company that enables risk mitigation and
                                            superior decision making</p>
                                    </span>
                                </div>

                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a tabindex="0">
                            <div class="card card-style-1 px-3">
                                <div class="card-body ">
                                    <p class="heading-3 text-white mb-3">Our Mission</p>
                                    <span class="text-white heading-6 height-f">
                                        <p>To provide best-in-class tools, analyses and insights, enabling stakeholders
                                            in Mauritian and African financial markets to make informed decisions.</p>

                                        <ul>
                                            <li>To build a pre-eminent position for ourselves in Mauritius and Africa in
                                                Risk analysis, ESG and information services</li>
                                            <li>To earn customer satisfaction and investor confidence through fairness
                                                and professional excellence</li>
                                            <li>To remain deeply committed to our internal and external stakeholders
                                            </li>
                                            <li>To apply the best possible tools &amp; techniques for risk analysis
                                                aimed to ensure efficiency and top quality</li>
                                            <li>To ensure globally comparable quality standards in our rating, research
                                                and information service&nbsp;</li>
                                            <li>&nbsp;</li>
                                        </ul>
                                    </span>
                                </div>

                            </div>
                        </a>
                    </div>
                    <div class="col-md-4">
                        <a tabindex="0">
                            <div class="card card-style-1 px-3">
                                <div class="card-body ">
                                    <p class="heading-3 text-white mb-3">Our Values</p>
                                    <span class="text-white heading-6 height-f">
                                        <ul>
                                            <li>Integrity and Transparency: Commitment to be ethical, sincere and open
                                                in our dealings</li>
                                            <li>Pursuit of Excellence: Committed to strive relentlessly to constantly
                                                improve ourselves</li>
                                            <li>Fairness: Treat clients, employees and other stakeholders fairly</li>
                                            <li>Independence: Unbiased and fearless in expressing our opinion</li>
                                            <li>Thoroughness: Rigorous analysis and research on every assignment that we
                                                take</li>
                                        </ul>
                                    </span>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="boardof-deirector-sec padding-100 ">
    <div class="container-fluid">
        <div class="row justify-content-center">

            <div class="col-md-11">

                <div class="d-flex justify-content-md-between align-items-md-center flex-column flex-md-row">
                    <h3 class="heading-1 text-black">Board of Directors</h3>

                    <div class="slider-arrows me-0">
                        <span class="prev-btn">

                            <img
                                src="https://www.careratingsafrica.com/public/frontend-assets/images/icons/arrow-left.png">
                        </span>

                        <span class="next-btn">
                            <img
                                src="https://www.careratingsafrica.com/public/frontend-assets/images/icons/arrow-right.png">
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="boardof-director-slider left-space-sldier">
        <?php for ($x = 0; $x <= 10; $x++) { ?>
        <div>
            <div class="profile-card p-0">
                <img src="public/frontend-assets/images/bod.jpeg" class="img-fluid" alt="Mr. Najib Shah">
                <h4 class="pers-name">Mr. Najib Shah</h4>
                <span class="profile-name">Chairman</span>
                <a href="#" data-bs-toggle="modal" data-bs-target="#about_leaders_modal0"
                    class="btn btn-link primary d-block p-0 mt-3 text-start font-regular" tabindex="0">Read
                    more</a>
            </div>
        </div>
        <?php } ?>
    </div>
</section>

<section class="senior-managment1-sec padding-100 pt-0" id="renderHtmlseniormanagmentSectiondata">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="d-flex justify-content-md-between align-items-md-center flex-column flex-md-row">
                    <h3 class="heading-1 text-black">Senior Management</h3>

                    <div class="slider-arrows me-0">

                        <span class="prev-btn">
                            <img
                                src="https://www.careratingsafrica.com/public/frontend-assets//images/icons/arrow-left.png">
                        </span>
                        <span class="next-btn">
                            <img
                                src="https://www.careratingsafrica.com/public/frontend-assets//images/icons/arrow-right.png">
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="senior-management-slider left-space-sldier">
        <?php for ($x = 0; $x <= 10; $x++) { ?>
        <div>
            <div class="profile-card p-0">
                <img src="public/frontend-assets/images/bod.jpeg" class="img-fluid" alt="Mr. Najib Shah">
                <h4 class="pers-name">Mr. Najib Shah</h4>
                <span class="profile-name">Chairman</span>
                <a href="#" data-bs-toggle="modal" data-bs-target="#about_leaders_modal0"
                    class="btn btn-link primary d-block p-0 mt-3 text-start font-regular" tabindex="0">Read
                    more</a>
            </div>
        </div>
        <?php } ?>
    </div>

    <div class="modal fade leadership_modal" id="exampleModal0" tabindex="-1" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close" jf-ext-button-ct="×">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                            <img src="https://www.careratingsafrica.com/storage/app/admin/images/image 1_1698759975.png"
                                class="img-fluid" alt="Mr. Saurav Chatterjee">
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 leadership_modal_content_div">
                            <h4 class="pers-name ">Mr. Saurav Chatterjee</h4>
                            <span class="profile-name">Director &amp; CEO</span>
                            <div class="leadership_modal_p_div">
                                <p></p>
                                <p>Mr. Saurav Chatterjee is the Director &amp; CEO of CARE Ratings (Africa) Private
                                    Limited since March 2016. He has<br>
                                    been associated with CARE Ratings Limited, India. (a premier Rating Agency in
                                    India)
                                    in senior roles for major<br>
                                    part of his career with a stint in corporate banking operations of Standard
                                    Chartered Bank. He is a seasoned risk<br>
                                    professional with vast experience in Indian and African markets. His core
                                    competencies include financial<br>
                                    engineering, risk assessment and technical analysis. Mr. Chatterjee has been
                                    instrumental in setting up and<br>
                                    scaling operations for the Rating agency in Mauritius. He is well versed with
                                    statutory and legal framework for<br>
                                    various jurisdictions of African Union. A versatile and dynamic personality, he
                                    deep
                                    dives into various aspects of<br>
                                    governance and risk whilst assessing corporates. He has served as member of
                                    Rating
                                    Committee of CARE<br>
                                    Ratings for more than a decade. He is well known in the regulatory and
                                    institutional
                                    circles for having popularized<br>
                                    the concept of credit rating in the Mauritian markets. His vision is to make
                                    capital
                                    markets accessible to<br>
                                    stakeholders in African Union through disclosures, transparency and price
                                    discovery
                                    by adopting global best<br>
                                    practices. Mr. Chatterjee takes personal interest in mentoring young analysts to
                                    be
                                    risk specialists<br>
                                    and future leaders. He is also committed and passionate about making education
                                    affordable to<br>
                                    children hailing from vulnerable society. He holds a Master’s in Financial &amp;
                                    Business Administration and a fellow<br>
                                    charter from the CFA Society, India. He is keen golfer and an ambassador for
                                    Indian
                                    cultural and historical<br>
                                    aspects of art.&nbsp;</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="code-of-conduct padding-100 bg-primary grey-patch  mb-0">
    <div class="container-fluid">
        <div class="row justify-content-center">

            <div class="col-md-11">
                <div class="row">
                    <div class="col-md-8">
                        <h3 class="heading-1 text-white">Code of Conduct</h3>
                    </div>
                    <div
                        class="mt-xl-3 custom-dropdown filter-dropdown  dropdown-menu-outiline  dropdown mb-5  mb-lg-0 ">
                        <div class="d-flex sasifb flex-row-reverse ">

                            <div class="" style="margin-left:5px;" id="renderHtmlPoliciesYeardata">

                                <select class="empInput form-control" name="Year_Id" id="Year_Id"
                                    style="border: 1px solid #858796;" jf-ext-cache-id="10">

                                    <option value="2023-2024" id="2023-2024">2023-2024</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="slider-arrows white arrow-2 justify-content-end mt-4">

                            <span class="prev-btn slick-arrow slick-hidden" aria-disabled="true" tabindex="-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                    class="bi bi-chevron-left" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                        d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z">
                                    </path>
                                </svg>
                            </span>
                            <span class="next-btn slick-arrow slick-hidden" aria-disabled="true" tabindex="-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                    class="bi bi-chevron-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                        d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z">
                                    </path>
                                </svg>
                            </span>
                        </div>
                    </div>
                    <hr class="style-2">
                </div>
            </div>

        </div>

        <div>
            <div class="commons-slider-codeofconduct left-space-sldier overflow-hidden">
                <div class="slick-list draggable">
                    <div class="slick-track" style="opacity: 1; width: 379px; transform: translate3d(0px, 0px, 0px);">
                        <div class="item slick-slide slick-current slick-active" tabindex="0" style="width: 329px;"
                            role="tabpanel" id="slick-slide20" aria-describedby="slick-slide-control20"
                            data-slick-index="0" aria-hidden="false">
                            <div class="card card-style-1">

                                <div class="d-flex justify-content-between">
                                    <span class="text-grey text-small text-uppercase">BEST PRACTICES</span>


                                </div>

                                <div class="card-body">
                                    <p class="heading-3 text-white mb-3 font-light">Code of Conduct</p>
                                    <a href="https://www.careratingsafrica.com/uploads/newsfiles/FinancialReports/1699004094_IOSCO - CRAF.pdf"
                                        class="btn btn-link primary text-start p-0 text-uppercase font-regular"
                                        tabindex="0">DOWNLOAD
                                        NOW</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>