<?php include 'components/header.php' ?>
<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlStartedSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Regulatory Disclosures</li>
                    </ol>
                </nav>
                <div
                    class="d-flex justify-content-md-between align-items-xl-center align-items-baseline flex-column flex-md-row">
                    <h1 class="heading-1 text-white">Regulatory Disclosures</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="capabilities-sec regulatory-disclosure bg-grey capabities-new-updated overflow-hidden">
    <div class="container-fluid left-space pe-0">
        <div class="accordian-style-1 row g-0">
            <div class="col-md-5 col-xl-3">
                <div class="accordian-title-box">
                    <span class="acc-title text-center"></span>
                    <span class="acc-count">1</span>
                </div>
            </div>
            <div class="col-md-12 col-xl-9">
                <div class="accordion">
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId0" class="accordion-button" type="button"
                                jf-ext-button-ct="disclosures as per sebi operational circular sebi/ho/ddhs/ddhs-racpod2/p/cir/2023/6 dated january 6, 2023">
                                Disclosures as per SEBI Operational Circular SEBI/HO/DDHS/DDHS-RACPOD2/P/CIR/2023/6
                                dated January 6, 2023
                            </button>
                        </h2>
                        <div id="collapse0" class="accordion-collapse collapse show">
                            <div class="accordion-body">
                                <p><a href="./upload/pdf/PD_Benchmarks_31-12-2019.pdf" target="_self">25.2.10. PD
                                        Benchmarks </a></p>

                                <p>26.3.1. Disclosures on Credit Rating History, defaults and movement</p>

                                <p>26.3.1.1. Half-Yearly Rating Summary Sheet</p>

                                <p><a href="./upload/pdf/Annexure 19 (Half Yearly Rating Summary Sheet) (1).xlsx"
                                        target="_self">October 2023- March 2024</a></p>

                                <p><a href="./upload/pdf/Annexure 19 (Half Yearly Rating Summary Sheet).xlsx"
                                        target="_self">April 2023- September 2023</a></p>

                                <p><a href="./upload/pdf/Half Yearly Rating Summary Sheet.xlsx" target="_self">October
                                        2022- March 2023</a></p>

                                <p><a href="./upload/pdf/Half Yearly Rating Summary Sheet Apr22 - Sep22.pdf"
                                        target="_self">April 2022- September 2022</a></p>

                                <p><a href="./upload/pdf/Half Yearly Rating Summary Sheet Oct21-Mar22.pdf"
                                        target="_self">October 2021- March 2022</a></p>

                                <p><a href="./upload/pdf/Half Yearly Rating Summary Sheet Apr21-Sep21.pdf"
                                        target="_self">April 2021 - September 2021</a></p>

                                <p><a href="./upload/pdf/Half Yearly Rating Summary Sheet Oct20-Mar21.pdf"
                                        target="_self">October 2020 - March 2021</a></p>

                                <p><a href="./upload/pdf/Half Yearly Rating Summary Sheet Apr20-Sept20.pdf"
                                        target="_self">April 2020 - September 2020</a></p>

                                <p><a href="./upload/pdf/Half Yearly Rating Summary Sheet Oct19-Mar20.pdf"
                                        target="_self">October 2019 - March 2020</a></p>

                                <p><a href="./upload/pdf/Half Yearly Rating Summary Sheet Apr19-Sept19.pdf"
                                        target="_self">April 2019 - September 2019</a></p>

                                <p><a href="./upload/pdf/Half Yearly Rating Summary Sheet Oct18-Mar19.pdf"
                                        target="_self">October 2018 - March 2019</a></p>

                                <p><a href="./upload/pdf/Half Yearly Rating Summary Sheet Apr18-Sept18.pdf"
                                        target="_self">April 2018 - September 2018</a></p>

                                <p>26.3.1.2. Details Of New Credit Ratings Assigned During Last 6 Months</p>

                                <p><a href="./upload/pdf/Annexure 20 (New Ratings) (1).xlsx" target="_self">October
                                        2023- March 2024</a></p>

                                <p><a href="./upload/pdf/Annexure 20 (New Ratings).xlsx" target="_self">April 2023-
                                        September 2023</a></p>

                                <p><a href="./upload/pdf/Annexure I.xlsx" target="_self">October 2022- March 2023</a>
                                </p>

                                <p><a href="./upload/pdf/Annexure I (14-10-22).pdf" target="_self">April 2022- September
                                        2022</a></p>
                                <p>26.3.1.3. (a) Movement Of Each Credit Rating</p>

                                <p><a href="./upload/pdf/Annexure 21 (Movement of Ratings) (1).xlsx"
                                        target="_self">October 2023-March 2024</a></p>

                                <p><a href="./upload/pdf/Annexure 21 (Movement of Ratings).xlsx" target="_self">April
                                        2023- September 2023</a></p>

                                <p><a href="./upload/pdf/Annexure II.xlsx" target="_self">October 2022- March 2023</a>
                                </p>

                                <p><a href="./upload/pdf/Annexure II (14-10-22).pdf" target="_self">April 2022-
                                        September 2022</a></p>

                                <p>26.3.1.3. (b) Movement Of Each Credit Rating From Investment Grade To Non Investment
                                    Grade And Vice Versa</p>

                                <p><a href="./upload/pdf/Annexure 22 (Movement of Ratings from IG to BIG &amp; Vice versa) (1).xlsx"
                                        target="_self">October 2023-March 2024</a></p>

                                <p><a href="./upload/pdf/Annexure 22 (Movement of Ratings from IG to BIG &amp; Vice versa).xlsx"
                                        target="_self">April 2023- September 2023</a></p>

                                <p><a href="./upload/pdf/Annexure III.xlsx" target="_self">October 2022- March 2023</a>
                                </p>

                                <p><a href="./upload/pdf/Annexure III (14-10-22).pdf" target="_self">April 2022-
                                        September 2022</a></p>
                                <!--<p><a href="./upload/pdf/Annexure III (15-4-2022).pdf" target="_self">October 2021- March 2022</a></p>
<div><a href="https://www.careratings.com/pdf/resources/Annexure III (14-10-2021).pdf" target="_self">April 2021 - September 2021</a></div>-->
                                <!--<div><a href="https://www.careratings.com/pdf/resources/Annexure III (15-4-2021).pdf" target="_self">October 2020 - March 2021</a></div></div>-->
                                <!--<div><a href="https://www.careratings.com/pdf/resources/Annexure%20III%20-15-10-2020-.pdf" target="_self">April 2020 – September 2020</a></div>-->
                                <!--<div><a href="https://www.careratings.com/pdf/resources/Annexure%20III%2013-5-2020.pdf" target="_self">October 2019- March 2020</a></div>-->

                                <p>26.3.1.4. History Of Credit Ratings Of All Outstanding Securities</p>

                                <p><a href="./upload/pdf/Annexure V.xlsx" target="_blank">For the Period FY 2023-2024
                                    </a></p>

                                <p><a href="./upload/pdf/Annexure V (1).xls" target="_blank">For the Period FY
                                        2022-2023</a></p>

                                <p>26.3.1.5. List Of Defaults Separately For Each Rating Category</p>

                                <p><a href="./upload/pdf/Annexure 24 (Details of Defaults) (2).xlsx"
                                        target="_self">October 2023- March 2024</a></p>

                                <p><a href="./upload/pdf/Annexure 24 (Details of Defaults).xlsx" target="_self">April
                                        2023- September 2023</a></p>

                                <p><a href="./upload/pdf/Annexure VI.xlsx" target="_self">October 2022- March 2023</a>
                                </p>

                                <p><a href="./upload/pdf/Annexure VI (14-10-22).pdf" target="_blank">April 2022-
                                        September 2022</a></p>

                                <p>26.3.2. Structured Finance Product</p>

                                <p><a href="./structured-finance" target="_blank">Performance of rated pools</a></p>

                                <p>26.4.1. Long-run and Short-run Average Default Rates</p>

                                <p><a href="./upload/pdf/Long-run and Short-run Average Default Rates FY24.xlsx">For the
                                        Period FY 2014 – FY2024</a></p>

                                <p><a href="./upload/pdf/Long-run and Short-run Average Default Rates.xlsx"
                                        target="_blank">For the Period FY 2013 – FY2023</a></p>

                                <p><a href="./upload/pdf/Long-run and Short-run Average Default Rates 29-4-22.pdf"
                                        target="_blank">For the Period FY 2012 – FY2022</a></p>

                                <p><a href="./upload/pdf/Long-run and Short-run Average Default Rates 30-4-2021.pdf"
                                        target="_blank">For the Period FY 2011 – FY2021</a></p>

                                <p><a href="./upload/pdf/Long-run and Short-run Average Default Rates 05-08-2020.pdf"
                                        target="_blank">For the Period FY 2010 – FY2020</a></p>

                                <p>Average Default Rates For The Last Five Financial Year Periods</p>

                                <p>For the period FY2015-2019</p>

                                <ul>
                                    <li><a href="./upload/pdf/Annexure VII (Overall).pdf" target="_blank">Overall</a>
                                    </li>
                                    <li><a href="./upload/pdf/Annexure VII (with Bifurcation).pdf" target="_blank">With
                                            bifurcation of Securities and Other financial Instruments </a></li>
                                </ul>

                                <p><a href="./upload/pdf/For%20the%20period%20FY2014-2018%20-%20Annexure%20VII.pdf"
                                        target="_self">For the period FY2014-2018</a></p>

                                <p><a href="./upload/pdf/For%20the%20period%20FY2013-2017%20-%20Annexure%20VII.pdf"
                                        target="_self">For the period FY2013-2017</a></p>

                                <p><a href="./upload/pdf/For%20the%20period%20FY2012-2016%20-%20Annexure%20VII.pdf"
                                        target="_self">For the period FY2012-2016</a></p>

                                <p><a href="./upload/pdf/For%20the%20period%20FY2011-2015%20-%20Annexure%20VII.pdf"
                                        target="_self">For the period FY2011-2015</a></p>

                                <p><a href="./upload/pdf/For%20the%20period%20FY2010-2014%20-%20Annexure%20VII.pdf"
                                        target="_self">For the period FY2010-2014</a></p>

                                <p><a href="./upload/pdf/For%20the%20period%20FY2009-2013%20-%20Annexure%20VII.pdf"
                                        target="_self">For the period FY2009-2013</a></p>

                                <p><a href="./upload/pdf/For%20the%20period%20FY2008-2012%20-%20Annexure%20VII.pdf"
                                        target="_self">For the period FY2008-2012</a></p>

                                <p><a href="./upload/pdf/For%20the%20period%20FY2007-2011%20-%20Annexure%20VII.pdf"
                                        target="_self">For the period FY2007-2011</a></p>

                                <p><a href="./upload/pdf/For%20the%20period%20FY2006-2010%20-%20Annexure%20VII.pdf"
                                        target="_self">For the period FY2006-2010</a></p>

                                <p>26.4.2. Average One Year transition rates for long-term ratings for the last
                                    5-Financial Years Period</p>

                                <p><a href="./upload/pdf/Average One Year Transition Rates FY24.xlsx">For the Period FY
                                        2020 – FY2024</a></p>

                                <p><a href="./upload/pdf/Average One Year Transition Rates FY24.xlsx>26.4.2. Average One Year transition rates for long-term ratings for the last 5-Financial Years Period</a></p>

<p><a  data-cke-saved-href=">For the Period FY 2019 – FY2023</a></p>

                                <p><a href="./upload/pdf/Average One Year Transition Rates 29-4-22.pdf"
                                        target="_self">For the Period FY 2018 – FY2022</a></p>

                                <p><a href="./upload/pdf/Average One Year Transition Rates (30-4-2021).pdf"
                                        target="_self">For the Period FY 2017 – FY2021</a></p>

                                <p><a href="./upload/pdf/AverageOneYearTransitionRates.pdf" target="_self">For the
                                        Period FY 2016 – FY2020</a></p>

                                <p>26.4.4. Income</p>

                                <p><a href="./upload/pdf/26.4.4.1. Income Disclosure FY24.pdf" target="_self">26.4.4.1.
                                        Total receipt from rating and non rating services(FY24)</a></p>

                                <p><a href="./upload/pdf/26.4.4.2. Share of non rating  income FY24.pdf"
                                        target="_self">26.4.4.2. Issuer wise percentage share of non-rating income of
                                        CARE and its Subsidiaries to the total revenue of CARE and its subsidiaries from
                                        that issuer.(FY24)</a></p>

                                <p>26.4.4.3. Name of the rated issuers who along with their associates contribute 10% or
                                    more to total revenue of CARE and its subsidiaries - NIL(FY24)</p>

                                <p><a href="./upload/pdf/26.4.4.1. Income Disclosure FY23.pdf" target="_self">26.4.4.1.
                                        Total receipt from rating and non rating services(FY23)</a></p>

                                <p><a href="./upload/pdf/26.4.4.2. Share of non rating  income FY23.pdf"
                                        target="_self">26.4.4.2. Issuer wise percentage share of non-rating income of
                                        CARE and its Subsidiaries to the total revenue of CARE and its subsidiaries from
                                        that issuer.(FY23)</a></p>

                                <p>26.4.4.3. Name of the rated issuers who along with their associates contribute 10% or
                                    more to total revenue of CARE and its subsidiaries - NIL(FY23)</p>

                                <p>26.4.5. Unsolicited Credit Ratings</p>

                                <p>CARE Ratings Does Not Carry Out Any Unsolicited Credit Ratings</p>

                                <p><a href="./search?Id=qE87DhcU7Rt++IC+4OhhKw==" target="_blank">27.2 List of
                                        instruments with no confirmation on payment status</a></p>

                                <p><a href="./unaccepted-ratings" target="_self">27.5. Disclosures in case of rating not
                                        accepted by an issuer</a></p>

                                <p><a href="./DelayinPeriodicReview" target="_self">27.6. Disclosures in case of delay
                                        in periodic review</a></p>

                                <p>29.1 Rating Procedure</p>

                                <ul>
                                    <li>
                                        <p><a href="./criteria_methodologies" target="_self">Methodology</a></p>
                                    </li>
                                    <li>
                                        <p><a href="./rating_process" target="_self">Policies And Procedures</a></p>
                                    </li>
                                </ul>

                                <p><a href="./upload/pdf/General Nature of Compensation Arrangements with rated entities- final 10 March 2021.pdf"
                                        target="_self">29.2.1. General Nature Of Its Compensation Arrangements With The
                                        Issuers</a></p>

                                <p><a href="./shareholding-patterns" target="_self">29.4. Share Holding Of CARE
                                        Ratings</a></p>

                                <p>29.5. Compliance Status Of IOSCO Code Of Conduct</p>

                                <p><a href="./upload/pdf/IOSCO%20Code-%20final%206-4-2021.pdf" target="_self">IOSCO Code
                                        Of Conduct</a></p>

                                <p>CARE Ratings is Compliant With IOSCO Code Of Conduct.</p>

                                <p><a href="https://www.careratings.com/regulatory-report" target="_self">Disclosure of
                                        Information on Issuers Not Cooperating (INC) with CRAs</a></p>

                            </div>
                        </div>
                    </div>



                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId1" class="accordion-button collapsed" type="button"
                                jf-ext-button-ct="other disclosures">
                                Other Disclosures
                            </button>
                        </h2>
                        <div id="collapse1" class="accordion-collapse collapse">
                            <div class="accordion-body">
                                <p><a
                                        href="https://www.careratings.com/upload/pdf/Termination-Letter-Hard-copy_anddetails-of-Registered-Post.pdf">Termination
                                        of Monitoring Agency Agreement</a></p>

                                <p><a href="./upload/pdf/SO CE Press Release-Revised 15-09-2019.pdf"
                                        target="_self">Revision in suffix to ratings pursuant to SEBI Circular</a></p>

                                <p><a href="./upload/pdf/Policy for dealing with Conflict of Interest(1-11-22).pdf">Guidelines
                                        for dealing with Conflict of Interest for Investment/ Trading</a></p>

                                <p><a href="./issuers_not_submitted_nds" target="_blank">List of issuers who have not
                                        yet submitted the ‘No Default Statement’ as mandated by SEBI</a></p>

                                <p><a href="./upload/pdf/Discontinuation%20Of%20Non-Rating%20Services%20By%20Care%20Ratings.pdf"
                                        target="_self">Discontinuation Of Non-Rating Services By CARE Ratings</a></p>

                                <p>&nbsp;</p>

                                <ul>
                                    <li>
                                        <p><a href="./upload/pdf/get-rated/Policy on Default Recognition_May 2023.pdf"
                                                target="_self">CARE’s Policy on Default Recognition</a>
                                        </p>
                                    </li>
                                    <li>
                                        <p><a href="./upload/pdf/get-rated/Rating outlook and rating watch _Dec 2022(21-12-22).pdf"
                                                target="_self">CARE’s Criteria on assigning Outlook to Credit
                                                Ratings</a>
                                        </p>
                                    </li>
                                    <li>
                                        <p><a href="./rating_process" target="_self">CARE’s Credit Rating Process</a>
                                        </p>
                                    </li>
                                    <li>
                                        <p><a href="./rating_faqs" target="_self">FAQs on rating</a></p>
                                    </li>
                                    <li>
                                        <p><a href="./criteria_methodologies" target="_self">CARE’s Rating Criteria
                                                &amp; Methodologies</a></p>
                                    </li>
                                    <li>
                                        <p>Financial Ratios</p>
                                    </li>
                                    <li>
                                        <p><a href="./upload/pdf/get-rated/Rating Methodology  Financial Ratios  Financial Sector December 2022(6-12-22).pdf"
                                                target="_self">Financial Sector</a>
                                            <!--<a href="/upload/pdf/Financial Ratios – Financial Sector (7-7-22).pdf" target="_self">Financial Sector</a>-->
                                        </p>
                                    </li>
                                    <li>
                                        <p><a href="./upload/pdf/Financial Ratios – Insurance Sector (18-8-22).pdf"
                                                target="_self">Insurance Sector</a></p>
                                    </li>
                                    <li>
                                        <p><a href="./pdf/resources/CARE%20Ratings’%20criteria%20on%20Financial%20Ratios%20-%20Non%20Financial%20Sector%20-%20March%202023.pdf"
                                                target="_blank">Non Financial Sector</a>
                                            <!--<a href="./upload/pdf/Financial ratios - Non Financial Sector-May 22(6-10-22).pdf" target="_self">Non Financial Sector</a>-->
                                        </p>
                                    </li>
                                    <li>
                                        <p><a href="./upload/pdf/Functioning%20of%20Rating%20Committees_Apr2020.pdf"
                                                target="_self">Functioning of CARE’s Rating Committees/
                                                Sub-Committees</a></p>
                                    </li>
                                    <li>
                                        <p><a href="./upload/pdf/General Nature of Compensation Arrangements with rated entities- final 10 March 2021.pdf"
                                                target="_self">General Nature of Compensation Arrangements with rated
                                                entities</a></p>
                                    </li>
                                    <li>
                                        <p><a href="./upload/pdf/Policy on Outsourcing_CARE Ratings Limited.pdf"
                                                target="_self">Policy on outsourcing of activities</a></p>
                                    </li>
                                    <li>CARE does not outsource any part of its rating activities.</li>
                                    <!--<li>
	<p><a href="./upload/pdf/Managing Conflict of Interest Final 5March2021.pdf" target="_self">Managing Conflict of Interest</a></p>
	</li>-->
                                    <li>
                                        <p><a href="./upload/pdf/No-Gift-Policy.pdf" target="_self">CARE’s Gift
                                                Policy</a></p>
                                    </li>
                                    <li>
                                        <p><a href="./upload/pdf/CARE's Confidentiality Policy-finalc 5March2021.pdf"
                                                target="_self">CARE’s Confidentiality Policy</a></p>
                                    </li>
                                </ul>

                                <p>&nbsp;</p>

                            </div>
                        </div>
                    </div>



                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId2" class="accordion-button collapsed" type="button"
                                jf-ext-button-ct="disclosures as per rbi directive dbod.bp.no./5382/21.06.007/2012-13 dated april 26, 2013">
                                Disclosures as per RBI directive DBOD.BP.No./5382/21.06.007/2012-13 dated April 26, 2013
                            </button>
                        </h2>
                        <div id="collapse2" class="accordion-collapse collapse">
                            <div class="accordion-body">
                                <p><a href="./upload/pdf/CompensationarrangementBankFacilities-28May2018.pdf"
                                        target="_blank">Compensation arrangement for bank loan ratings</a></p>

                            </div>
                        </div>
                    </div>



                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId3" class="accordion-button collapsed" type="button"
                                jf-ext-button-ct="disclosures as per rbi directive dbod.bp.3526/21.06.007/2014-15 dated september 5, 2014">
                                Disclosures as per RBI directive DBOD.BP.3526/21.06.007/2014-15 dated September 5, 2014
                            </button>
                        </h2>
                        <div id="collapse3" class="accordion-collapse collapse">
                            <div class="accordion-body">
                                <a href="./upload/pdf/Suspensionpolicyforbankloanratings.pdf" target="_blank">CARE’s
                                    suspension policy for Bank Loan Ratings</a>

                            </div>
                        </div>
                    </div>



                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId4" class="accordion-button collapsed" type="button"
                                jf-ext-button-ct="disclosure as per sebi master circular dated july 31, 2023 on online resolution of disputes in the indian securities market">
                                Disclosure as per SEBI Master Circular dated July 31, 2023 on Online Resolution of
                                Disputes in the Indian Securities Market
                            </button>
                        </h2>
                        <div id="collapse4" class="accordion-collapse collapse">
                            <div class="accordion-body">
                                <p><a href="https://www.careratings.com/smart-odr">SMART ODR</a></p>

                            </div>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>










</section>
<?php include 'components/footer.php' ?>