

$(document).ready(function () {
 



  var swiper1 = new Swiper(".swiper-1", {
    slidesPerView: 1,
    spaceBetween: 140,
    speed: 1000,
    navigation: {
      nextEl: ".upcoming-events .next-btn",
      prevEl: ".upcoming-events .prev-btn",
    },
    breakpoints: {
      640: {
        slidesPerView: 2,
      },
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 2,
      },
      1280: {
        slidesPerView: 2.8,
      },
    },
  });

  var swiper2 = new Swiper(".swiper-2", {
    slidesPerView: 1,
    spaceBetween: 45,
    speed: 1000,
    navigation: {
      nextEl: ".upcoming-events .next-btn",
      prevEl: ".upcoming-events .prev-btn",
    },
    breakpoints: {
      640: {
        slidesPerView: 2,
      },
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 2,
      },
      1280: {
        slidesPerView: 4.5,
      },
    },
  });
  
/*12-01-2023 start*/
  var swiper2 = new Swiper(".swiper-publication", {
    slidesPerView: 1,
    spaceBetween: 45,
    speed: 1000,
    navigation: {
      nextEl: ".care-edge-slider-new .next-btn",
      prevEl: ".care-edge-slider-new .prev-btn",
    },
    breakpoints: {
      640: {
        slidesPerView: 2,
      },
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 2,
      },
      1280: {
        slidesPerView: 4.5,
      },
    },
  });
/*12-01-2023 end*/

  var swiper22 = new Swiper(".swiper-22", {
    slidesPerView: 1,
    spaceBetween: 45,
    speed: 1000,
    navigation: {
      nextEl: ".upcoming-events .next-btn",
      prevEl: ".upcoming-events .prev-btn",
    },
    breakpoints: {
      640: {
        slidesPerView: 2,
      },
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 2,
      },
      1280: {
        slidesPerView: 3.5,
      },
    },
  });

  const commonSlider = (
    sliderName,
    dots,
    fade,
    infinite,
    slidesToShow,
    slidesToScroll,
    prevArrow,
    nextArrow,
    variableWidth
  ) => {
    $(sliderName).slick({
      dots: dots,
      fade: fade,
      infinite: infinite,
      speed: 1000,
      slidesToShow: slidesToShow,
      slidesToScroll: slidesToScroll,
      prevArrow: prevArrow,
      nextArrow: nextArrow,
      variableWidth: variableWidth,
      responsive: [
        {
          breakpoint: 1100,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: true,
          },
        },
        {
          breakpoint: 1099,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            dots: true,
          },
        },

        {
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
          },
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
          },
        },
      ],
    });
  };

  commonSlider(
    ".board-of-directors-slider",
    false,
    false,
    false,
    3,
    1,
    ".board-of-directors-sec .prev-btn",
    ".board-of-directors-sec .next-btn",
    false
  );

  commonSlider(
    ".senior-managment-slider",
    false,
    false,
    false,
    3,
    1,
    ".senior-managment-sec .prev-btn",
    ".senior-managment-sec .next-btn",
    false
  );

  commonSlider(
    ".similar-sector-sldier",
    false,
    false,
    false,
    3,
    1,
    ".similar-sector-sec .prev-btn",
    ".similar-sector-sec .next-btn",
    false
  );

  commonSlider(
    ".reports-slider",
    false,
    false,
    false,
    3,
    1,
    ".reports-sec .prev-btn",
    ".reports-sec .next-btn",
    false
  );

  commonSlider(
    ".corp-gover-sldier",
    false,
    false,
    false,
    3,
    1,
    ".corp-gover-sec .prev-btn",
    ".corp-gover-sec .next-btn",
    false
  );

  commonSlider(
    ".webinars-slider",
    false,
    false,
    false,
    3,
    1,
    ".webinars-sec .prev-btn",
    ".webinars-sec .next-btn",
    false
  );

  commonSlider(
    ".event-gallery-slider",
    false,
    false,
    false,
    2,
    1,
    ".event-gallery-sec .prev-btn",
    ".event-gallery-sec .next-btn",
    false
  );

  commonSlider(
    ".podcasts-slider",
    false,
    false,
    false,
    2,
    1,
    ".podcasts-sec .prev-btn",
    ".podcasts-sec .next-btn",
    false
  );

  commonSlider(
    ".solution_product-tab-sldier",
    false,
    false,
    false,
    3,
    1,
    ".financial-report-sec .prev-btn",
    ".financial-report-sec .next-btn",
    false
  );

  $(".commons-slider").slick({
    infinite: false,
    speed: 1000,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: ".arrow-1 .prev-btn",
    nextArrow: ".arrow-1 .next-btn",
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 1099,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },

      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          dots: true,
          centerMode: true,
        },
      },
    
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true,
          // centerMode: true,
          // centerPadding: "50px",
        },
      },
    ],
  });
  $(".commons-slider-2").slick({
    infinite: false,
    speed: 1000,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: ".arrow-2 .prev-btn",
    nextArrow: ".arrow-2 .next-btn",
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 1099,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },

      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          dots: true,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true,
        },
      },
    ],
  });
  $(".commons-slider-3").slick({
    infinite: false,
    speed: 1000,
    slidesToShow: 4,
    slidesToScroll: 1,
    prevArrow: ".arrow-2 .prev-btn",
    nextArrow: ".arrow-2 .next-btn",
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 1099,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },

      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          dots: true,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true,
        },
      },
    ],
  });
  $(".commons-slider-4").slick({
    infinite: false,
    speed: 1000,
    slidesToShow: 3,
    slidesToScroll: 1,
    prevArrow: ".arrow-2 .prev-btn",
    nextArrow: ".arrow-2 .next-btn",
    responsive: [
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 1099,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },

      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          dots: true,
        },
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true,
        },
      },
    ],
  });


  if ($(window).width() > 1024) {
    $(".row").each(function () {
      var highestBox = 0;

      $(".equal-height", this).each(function () {
        if ($(this).height() > highestBox) {
          highestBox = $(this).height();
        }
      });

      $(".equal-height", this).height(highestBox);
    });
  }

  $(".play__btn").yu2fvl();

  $(".play__btn").click(function () {
    $("header").removeClass("fixed-header");
  });

  $("#message-input").keyup(function () {
    var characterCount = $(this).val().length,
      current = $("#current"),
      maximum = $("#maximum"),
      theCount = $("#the-count");

    current.text(characterCount);
  });


  $("body").on('click', '.accordion-button', function(){

  // $(".accordion-button").click(function () {
    $(this)
      .parent(".accordion-header")
      .next(".accordion-collapse")
      .slideToggle(300);
    $(this)
      .parents(".accordion-item")
      .siblings()
      .find(".accordion-collapse")
      .slideUp(300);

    $(this).toggleClass("collapsed");
    $(this)
      .parents(".accordion-item")
      .siblings()
      .find(".accordion-button")
      .addClass("collapsed");

    let accTxt = $(this).text();
    let accNum = $(this).parents(".accordion-item").index() + 1;
    $(".acc-count").text(accNum);
    $(".acc-title").text(accTxt);
    anime({
      targets: ".acc-count",
      translateY: [-100, 0],
      opacity: [0, 0.1],
      easing: "easeOutExpo",
    });
    anime({
      targets: ".acc-title",
      translateY: [50, 0],
      opacity: [0, 1],
      easing: "easeOutExpo",
    });
  });

  

  var lastScrollTop = 0;

  $(window).scroll(function () {
    var st = $(this).scrollTop();
    if (st > lastScrollTop) {
      $("header").removeClass("fixed-header");
    } else if (st == 0) {
      $("header").removeClass("fixed-header");
    } else {
      $("header").addClass("fixed-header");
      $(".find-ratings").removeClass("fixed");
    }
    lastScrollTop = st;

    // if (scroll >= 80) {
    //   $("header").addClass("fixed-header");
    // } else {
    //   $("header").removeClass("fixed-header");
    // }

    if ($(".find-ratings").length > 0) {
      let findRatingScrollTop = $(".find-ratings").position();
      if (st >= findRatingScrollTop.top) {
        $(".find-ratings").addClass("fixed");
      } else {
        $(".find-ratings").removeClass("fixed");
      }
    }

    if (st > 300) {
      $(".scrolltoTop").addClass("show");
    } else {
      $(".scrolltoTop").removeClass("show");
    }
  });

  $(".scrolltoTop").on("click", function () {
    $("html, body").animate({
      scrollTop: $("html, body").offset().top,
    });
  });

  // $(".main-slider").slick({
  //   dots: true,
  //   infinite: false,
  //   speed: 1000,
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  // });

  // });
  // $('.main-slider').on('beforeChange', function (event, slick, currentSlide, nextSlide) {

  //     gsap.fromTo(".main-slider .banner-captions .heading-1, .banner-captions p, .projects-count span, .projects-count h2", {opacity: 0,  y: 100,}, {opacity: 1, y: 0, duration: 1});

  // });
  /*Slider banner animation*/

  // dropdown JS
  $(".custom-dropdown .dropdown-toggle").click(function () {
    $(".dropdown-menu").not($(this).next(".dropdown-menu")).removeClass("show");
    $(this).next(".dropdown-menu").toggleClass("show");

    anime({
      targets: ".custom-dropdown .dropdown-menu li",
      translateY: [30, 0],
      opacity: [0, 1],
      delay: function (el, i, l) {
        return i * 0;
      },
      duration: 800,
      easing: "easeOutExpo",
    });
  });

  $(".custom-dropdown .dropdown-menu li a").click(function () {
    let slectedVal = $(this).text();
    $(this).parents(".dropdown").find(".selected-value").text(slectedVal);
    $(this).parents(".dropdown-menu").removeClass("show");
  });

  // dropdown JS
  // $("header .dropdown").hover(
  //   function () {
  //     $(".dropdown-menu")
  //       .not($(this).find(".dropdown-menu"))
  //       .removeClass("show");
  //     $(this).find(".dropdown-menu").addClass("show");
  //     $(".secondary-menu > ul").slideUp();
  //     $(".menu-icon").removeClass("opened");
  //   },
  //   function () {
  //     $(".dropdown-menu").removeClass("show");
  //   }
  // );

  // Click on outside of the element (document)
  $(document).on("click", function (event) {
    var $trigger = $(".dropdown, .secondary-menu");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
      $(".dropdown-menu").removeClass("show");
      $(".secondary-menu > ul").slideUp();
      $(".menu-icon").removeClass("opened");
    }
  });

  // Click on outside of the element end

  $(".menu-items li").hover(function () {
    $(this).find(".sub-inner-menu").addClass("show");
    $(this).siblings("li").find(".sub-inner-menu").removeClass("show");
  });
  $(".menu-items li").mouseleave(function () {
    $(".sub-inner-menu").removeClass("show");
  });
});





$('.digit-group').find('input').each(function() {
	$(this).attr('maxlength', 1);
	$(this).on('keyup', function(e) {
		var parent = $($(this).parent());
		
		if(e.keyCode === 8 || e.keyCode === 37) {
			var prev = parent.find('input#' + $(this).data('previous'));
			
			if(prev.length) {
				$(prev).select();
			}
		} else if((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
			var next = parent.find('input#' + $(this).data('next'));
			
			if(next.length) {
				$(next).select();
			} else {
				if(parent.data('autosubmit')) {
					parent.submit();
				}
			}
		}
	});
});

//onclick active

$('.btn-group .btn').on('click', function(){
  $('.btn-group .btn.active').removeClass('active');
  $(this).addClass('active');
});




$(document).ready(function(){

var current_fs, next_fs, previous_fs; //fieldsets
var opacity;
var current = 1;
var steps = $("fieldset").length;

setProgressBar(current);

$(".next").click(function(){

current_fs = $(this).parent();
next_fs = $(this).parent().next();

//Add Class Active
$("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

//show the next fieldset
next_fs.show();
//hide the current fieldset with style
current_fs.animate({opacity: 0}, {
step: function(now) {
// for making fielset appear animation
opacity = 1 - now;

current_fs.css({
'display': 'none',
'position': 'relative'
});
next_fs.css({'opacity': opacity});
},
duration: 500
});
setProgressBar(++current);
});

$(".previous").click(function(){

current_fs = $(this).parent();
previous_fs = $(this).parent().prev();

//Remove class active
$("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

//show the previous fieldset
previous_fs.show();

//hide the current fieldset with style
current_fs.animate({opacity: 0}, {
step: function(now) {
// for making fielset appear animation
opacity = 1 - now;

current_fs.css({
'display': 'none',
'position': 'relative'
});
previous_fs.css({'opacity': opacity});
},
duration: 500
});
setProgressBar(--current);
});

function setProgressBar(curStep){
var percent = parseFloat(100 / steps) * curStep;
percent = percent.toFixed();
$(".progress-bar")
.css("width",percent+"%")
}

$(".submit").click(function(){
return false;
})

});



//password

$(".toggle-password").click(function() {

  $(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
});

//text count

$('textarea').keyup(function() {
    
  var characterCount = $(this).val().length,
      current = $('#current'),
      maximum = $('#maximum'),
      theCount = $('#the-count');
    
  current.text(characterCount);
 
  
  /*This isn't entirely necessary, just playin around*/
  if (characterCount < 70) {
    current.css('color', '#666');
  }
  if (characterCount > 70 && characterCount < 90) {
    current.css('color', '#6d5555');
  }
  if (characterCount > 90 && characterCount < 100) {
    current.css('color', '#793535');
  }
  if (characterCount > 100 && characterCount < 120) {
    current.css('color', '#841c1c');
  }
  if (characterCount > 120 && characterCount < 139) {
    current.css('color', '#8f0001');
  }
  
  if (characterCount >= 140) {
    maximum.css('color', '#8f0001');
    current.css('color', '#8f0001');
    theCount.css('font-weight','bold');
  } else {
    maximum.css('color','#666');
    theCount.css('font-weight','normal');
  }
  
      
});


/*Find-rating page Filter Js Start*/ 
$(function() {

  $("#filter-rate li a").click(function() {

    var category = $(this).html();

    category = category.toLowerCase();

    $("#filter-rate li a").removeClass("active");

    $(this).addClass("active");

    $("#portfolio-rate li").fadeOut();

    $("#portfolio-rate li").each(function() {

      if ($(this).hasClass(category)) {
        $(this).fadeIn();
      }

    });

  });

});
/*Find-rating page Filter Js End*/

//search bar start

let results2 = document.getElementById("res2");


function myFunc2(value){
  if(value !== ""){
    results2.classList.remove("hideResults2");
  }else{
    results2.classList.add("hideResults2");
  }
  
}

var swiper22 = new Swiper(".swiper-22", {
  slidesPerView: 1,
  spaceBetween: 45,
  speed: 1000,
  navigation: {
    nextEl: ".upcoming-events .next-btn",
    prevEl: ".upcoming-events .prev-btn",
  },
  breakpoints: {
    640: {
      slidesPerView: 2,
    },
    768: {
      slidesPerView: 2,
    },
    1024: {
      slidesPerView: 2,
    },
    1280: {
      slidesPerView: 3.5,
    },
  },
});

//search bar end



//career page js
function myFunction() {
  document.getElementById("myDiv").style.display = "block";
  document.getElementById("myDiv1").style.display = "none";
  document.getElementById("myDiv2").style.display = "none";
  document.getElementById("myDiv3").style.display = "none";

}
function myFunction1() {
  document.getElementById("myDiv1").style.display = "block";
  document.getElementById("myDiv").style.display = "none";
  document.getElementById("myDiv2").style.display = "none";
  document.getElementById("myDiv3").style.display = "none";

}
function myFunction2() {
  document.getElementById("myDiv1").style.display = "none";
  document.getElementById("myDiv").style.display = "none";
  document.getElementById("myDiv2").style.display = "block";
  document.getElementById("myDiv3").style.display = "none";

}
function myFunction3() {
  document.getElementById("myDiv1").style.display = "none";
  document.getElementById("myDiv").style.display = "none";
  document.getElementById("myDiv2").style.display = "none";
  document.getElementById("myDiv3").style.display = "block";

}

// Select dropdown arrow

$(document).ready(function () {
  $('#renderHtmlCompanyPRDocumentYearSection').on('click', function () {
      $(this).toggleClass('aroowRotate');
  });
});




var swiper = new Swiper('.speak-slider', {
  slidesPerView: 1,
  slidesPerColumn: 2,
  spaceBetween: 30,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  breakpoints: {
    640: {
      slidesPerView: 1.5,
    },
    768: {
      slidesPerView: 1.5,
    },
    1024: {
      slidesPerView: 2.5,
    },
    1280: {
      slidesPerView: 3.5,
    },
  },
});
var swiper = new Swiper('.csr-slider', {
  slidesPerView: 1,
  slidesPerColumn: 2,
  spaceBetween: 30,
  pagination: {
    el: '.swiper-pagination',
    clickable: true,
  },
  breakpoints: {
    640: {
      slidesPerView: 1.5,
    },
    768: {
      slidesPerView: 2.5,
    },
    1024: {
      slidesPerView: 2.5,
    },
    1280: {
      slidesPerView: 4.5,
    },
  },
});


const players = Array.from(document.querySelectorAll("#player")).map((p) => new Plyr(p));

$(document).ready(function () {
	$("#loader").css("display", "none");

	$(".close-button").on("click", function () {
		$(".results").addClass("hideResults");
		$("#CompanyInput").val("");
	});

	$("#CompanyInputmobile").keyup(function () {
		var cinput = $("#CompanyInputmobile").val();
		if (cinput.length >= 4) {
			console.log(cinput);
			headerSearchSectiondata(cinput);
			// return true;
		}
	});

	$(".select-all-input").one("click", function () {
		$(".swiper-wrapper.second2").append(
			'<div class="swiper-slide"><div class="card card-style-1"><div class="d-flex justify-content-between"><span class="text-grey text-small text-uppercase">24TH OCTOBER 2021</span></div><div class="card-body align-items-center"><p class="heading-3 text-white mb-1">Baramati Agro</p><span class="text-grey">₹ 1800</span><a href="#" class="btn btn-link primary p-0 d-block text-start mt-4">DOWNLOAD NOW</a></div></div></div>'
		);
		//   $( this ).toggleClass( ".swiper-slide" );
	});

	function printErrorMsg(msg) {
		$(".print-footer-error-msg").find("ul").html("");
		$(".print-footer-error-msg").css("display", "block");
		$.each(msg, function (key, value) {
			$(".print-footer-error-msg")
				.find("ul")
				.append("<li>" + value + "</li>");
		});
	}
	var $selector = $("#newsletter_form");
	form_get_otp = $selector.parsley();
	validation = true;
	$("#newsletter_btn").click(function (event) {
		event.preventDefault();
		form_get_otp.validate();
	});
	form_get_otp.subscribe("parsley:form:success", function (event) {
		data = $("#newsletter_form").serialize();
		$.ajax({
			url: "https://www.careratingsafrica.com/newsletter",
			method: "post",
			data: data,
			success: function (response) {
				if ($.isEmptyObject(response.error)) {
					$(".print-footer-error-msg").css("display", "none");
					$(".print-footer-success-msg").find("ul").html("");
					$(".print-footer-success-msg").css("display", "block");
					$(".print-footer-success-msg")
						.find("ul")
						.append("<li>" + response.success + "</li>");
					//pass to dashboard
					// location.reload();
					// window.location.href = "https://www.careratingsafrica.com/user/dashboard";
					//end
				} else {
					printErrorMsg(response.error);
				}
			},
			error: function (response) {
				printErrorMsg(response.error);
			},
		});
	});
});

let results = document.getElementById("res");

function myFunc(value) {
	if (value !== "") {
		results.classList.remove("hideResults");
	} else {
		results.classList.add("hideResults");
	}
}

function renderEco() {
	const commonpressSlider = (
		sliderName,
		dots,
		fade,
		infinite,
		slidesToShow,
		slidesToScroll,
		prevArrow,
		nextArrow,
		variableWidth
	) => {
		$(sliderName).slick({
			dots: dots,
			fade: fade,
			infinite: infinite,
			speed: 1000,
			slidesToShow: slidesToShow,
			slidesToScroll: slidesToScroll,
			prevArrow: prevArrow,
			nextArrow: nextArrow,
			variableWidth: variableWidth,
			responsive: [
				{
					breakpoint: 1100,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
						infinite: true,
						dots: true,
					},
				},
				{
					breakpoint: 1099,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						infinite: true,
						dots: true,
					},
				},

				{
					breakpoint: 600,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						dots: true,
					},
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						dots: true,
					},
				},
			],
		});
	};

	commonpressSlider(
		".reports-slider",
		false,
		false,
		false,
		4,
		3,
		".reports-sec .prev-btn",
		".reports-sec .next-btn",
		false
	);
}

renderEco();

let results5 = document.getElementById("res5");

function myFunc5(value) {
	if (value !== "") {
		results5.classList.remove("hideResults5");
	} else {
		results5.classList.add("hideResults5");
	}
}

$(document).ready(function () {
	$(".webinars1-slider").slick({
		dots: false,
		fade: false,
		infinite: false,
		speed: 1000,
		slidesToShow: 3,
		slidesToScroll: 1,
		prevArrow: ".webinars1-sec .prev-btn",
		nextArrow: ".webinars1-sec .next-btn",
		variableWidth: false,
		responsive: [
			{
				breakpoint: 1100,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
				},
			},
			{
				breakpoint: 1099,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
				},
			},

			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				},
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				},
			},
		],
	});

	$(".eventgallery-slider").slick({
		dots: false,
		fade: false,
		infinite: false,
		speed: 1000,
		slidesToShow: 2,
		slidesToScroll: 1,
		prevArrow: ".CareEdgeeventgallery-sec .prev-btn",
		nextArrow: ".CareEdgeeventgallery-sec .next-btn",
		variableWidth: false,
		responsive: [
			{
				breakpoint: 1100,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
				},
			},
			{
				breakpoint: 1099,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
				},
			},

			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				},
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				},
			},
		],
	});

	$(".boardof-director-slider").slick({
		dots: false,
		fade: false,
		infinite: false,
		speed: 1000,
		slidesToShow: 3,
		slidesToScroll: 1,
		prevArrow: ".boardof-deirector-sec .prev-btn",
		nextArrow: ".boardof-deirector-sec .next-btn",
		variableWidth: false,
		responsive: [
			{
				breakpoint: 1100,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
				},
			},
			{
				breakpoint: 1099,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
				},
			},

			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				},
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				},
			},
		],
	});

	$(".senior-management-slider").slick({
		dots: false,
		fade: false,
		infinite: false,
		speed: 1000,
		slidesToShow: 3,
		slidesToScroll: 1,
		prevArrow: ".senior-managment1-sec .prev-btn",
		nextArrow: ".senior-managment1-sec .next-btn",
		variableWidth: false,
		responsive: [
			{
				breakpoint: 1100,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
				},
			},
			{
				breakpoint: 1099,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
				},
			},

			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
				},
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				},
			},
		],
	});

	$(".other-capabilities-slider").slick({
		infinite: false,
		speed: 1000,
		slidesToShow: 3,
		slidesToScroll: 1,
		prevArrow: ".arrow-2 .prev-btn",
		nextArrow: ".arrow-2 .next-btn",
		responsive: [
			{
				breakpoint: 1100,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: true,
				},
			},
			{
				breakpoint: 1099,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: true,
				},
			},

			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					dots: true,
				},
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					dots: true,
				},
			},
		],
	});

	$(".reports-slider-new").slick({
		infinite: false,
		speed: 1000,
		slidesToShow: 2.1,
		slidesToScroll: 1,
		prevArrow: ".arrow-1 .prev-btn",
		nextArrow: ".arrow-1 .next-btn",
		responsive: [
			{
				breakpoint: 1100,
				settings: {
					slidesToShow: 2.1,
					slidesToScroll: 1,
					infinite: true,
					dots: true,
				},
			},
			{
				breakpoint: 1099,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: true,
				},
			},

			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2.1,
					slidesToScroll: 1,
					dots: true,
					centerMode: true,
				},
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1.1,
					slidesToScroll: 1,
					dots: true,
				},
			},
		],
	});

	$(".small-cards-slider").slick({
		dots: false,
		fade: false,
		infinite: false,
		speed: 1000,
		slidesToShow: 3,
		slidesToScroll: 1,
		prevArrow: ".small-card-slider-buttons .btn-prev",
		nextArrow: ".small-card-slider-buttons .btn-next",
		variableWidth: false,
		responsive: [
			{
				breakpoint: 1100,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					infinite: true,
					dots: true,
				},
			},
			{
				breakpoint: 1099,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					infinite: true,
					dots: true,
				},
			},

			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1,
					dots: true,
				},
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1.5,
					slidesToScroll: 1,
					dots: true,
				},
			},
		],
	});

	$(".resultPan .close-button").on("click", function () {
		//$(".resultPan").addClass("hideResults");

		$(".results").addClass("hideResults");
		$("#CompanyInput").val("");

		$(".results1").addClass("hideResults1");
		$("#CompanybannerInputs").val("");

		$(".results5").addClass("hideResults5");
		$("#CompanyFindRatingInput").val("");
	});
});


