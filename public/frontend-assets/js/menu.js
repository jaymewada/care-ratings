// ---------------------------------------------------------
// Bootstrap 4 : Responsive Dropdown Multi Submenu
// ---------------------------------------------------------
$(function(){
  $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
    if (!$(this).next().hasClass('show')) {
      $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
    }
    var $subMenu = $(this).next(".dropdown-menu");
    $subMenu.toggleClass('show'); 			// appliqué au ul
    $(this).parent().toggleClass('show'); 	// appliqué au li parent

    $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
      $('.dropdown-submenu .show').removeClass('show'); 	// appliqué au ul
      $('.dropdown-submenu.show').removeClass('show'); 		// appliqué au li parent
    });
    return false;
  });
});


if ($(window).width() <= 992) {
    $(".navbar-nav .dropdown .dropdown-toggle").click(function () {
      $(this).next(".dropdown-menu").slideToggle();
      $(this)
        .parent(".nav-item")
        .siblings("li")
        .find(".dropdown-menu")
        .slideUp();
    });
  }
  $(".sub-links > a").click(function () {
    $(this).toggleClass("open");
    $(this).next("ul").slideToggle();
    $(this)
      .parents(".col-md-6")
      .siblings(".col-md-6")
      .find(".sub-links ul")
      .slideUp();
    $(this).parent(".sub-links").siblings(".sub-links").find("ul").slideUp();

    $(this)
      .parents(".col-md-6")
      .siblings(".col-md-6")
      .find(".sub-links a")
      .removeClass("open");
    $(this)
      .parents(".sub-links")
      .siblings(".sub-links")
      .children("a")
      .removeClass("open");
  });

  $(".secondary-menu .menu-icon").click(function () {
    $(this).next("ul").slideToggle();
  });
  $(".secondary-menu ul li .dropdown-toggle").click(function () {
    $(this).parent().siblings().find(".secondary-menu-list ").slideUp();
    $(this).next(".secondary-menu-list ").slideToggle();
  });
  


  jQuery("#faq-tab .btn").on("click",function(){
    jQuery("#faq-tab .btn").removeClass("active");
    jQuery(this).addClass("active");
  });

 /* if (localStorage.getItem('cookieSeen') != 'shown') {
    $('.cookies').delay(2000).fadeIn();
    localStorage.setItem('cookieSeen','shown')
  };
  $('.cookie-close').click(function() {
    $('.cookies').fadeOut();
  });*/

  // Create cookie
function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + (exdays*24*60*60*1000));
  let expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

// Delete cookie
function deleteCookie(cname) {
  const d = new Date();
  d.setTime(d.getTime() + (24*60*60*1000));
  let expires = "expires="+ d.toUTCString();
  document.cookie = cname + "=;" + expires + ";path=/";
}

// Read cookie
function getCookie(cname) {
  let name = cname + "=";
  let decodedCookie = decodeURIComponent(document.cookie);
  let ca = decodedCookie.split(';');
  for(let i = 0; i <ca.length; i++) {
      let c = ca[i];
      while (c.charAt(0) == ' ') {
          c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
      }
  }
  return "";
}

// Set cookie consent
function acceptCookieConsent(){
  deleteCookie('user_cookie_consent');
  setCookie('user_cookie_consent', 1, 30);
  document.getElementById("cookieNotice").style.display = "none";
}

let cookie_consent = getCookie("user_cookie_consent");
console.log(cookie_consent);
if(cookie_consent != ""){
  document.getElementById("cookieNotice").style.display = "none";
}else{
  document.getElementById("cookieNotice").style.display = "block";
}