<?php include 'components/header.php' ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlStartedSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Privacy Policy</li>
                    </ol>
                </nav>
                <div
                    class="d-flex justify-content-md-between align-items-xl-center align-items-baseline flex-column flex-md-row">
                    <h1 class="heading-1 text-white">Privacy Policy</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="capabilities-sec regulatory-disclosure bg-grey capabities-new-updated overflow-hidden">

    <div class="container-fluid left-space pe-0">
        <div class="accordian-style-1 row g-0">
            <div class="col-md-5 col-xl-3">
                <div class="accordian-title-box">
                    <span class="acc-title text-center"></span>
                    <span class="acc-count">1</span>
                </div>
            </div>
            <div class="col-md-12 col-xl-9">
                <div class="accordion">

                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId0" class="accordion-button" type="button"
                                jf-ext-button-ct="privacy policy">
                                Privacy Policy
                            </button>
                        </h2>
                        <div id="collapse0" class="accordion-collapse collapse show">
                            <div class="accordion-body">
                                <p>This Privacy Policy governs the website&nbsp;<a
                                        href="https://www.careratings.com/index.aspx">http://www.careratings.com/</a>&nbsp;(“Web
                                    Site”), operated by CARE Ratings Limited (“CareEdge”). This Policy explains how we
                                    collect and protect your personal information on our Web Site. Except as indicated
                                    herein, we will not sell, share, license, trade, or rent your personal information
                                    to others. For purposes of this Policy, .we., .us., and .our. means CareEdge.</p>

                                <p>This Policy represents our current data collection and usage practices. CareEdge will
                                    from time to time review and revise these practices. We will post any changes to
                                    this Policy here. Please refer to this Policy on a regular basis. Use of our Web
                                    Site or any of the products or services provided on our Web Site, indicates your
                                    agreement to the terms of this Policy.</p>

                                <p>This Policy applies only to information collected by our Web Site. We are not
                                    responsible for the privacy practices of websites that are operated or owned by
                                    third parties.</p>

                                <p><strong>Collection of personal data</strong></p>

                                <p>Personally identifiable information (PII) is information that identifies you or can
                                    be used to identify or contact you, (e.g., your name, email address, business
                                    address, or phone number. We request PII from you when you register on any of our
                                    Website. The registration process requests that you provide your name, business
                                    email address, business address, business phone number, and company affiliation, and
                                    agree to the terms of use agreement presented at the time of registration. In
                                    addition to information collected during registration, we may collect PII knowingly
                                    provided by you in emails to our Web Site, online forms, order and registration
                                    forms and surveys. In each of these cases, we collect PII from you only if you
                                    voluntarily submit such information to us. You do not have to provide us with any
                                    PII to visit our Web Site. If you choose to withhold requested information, however,
                                    you may not be able to use all of the features of our Web Site.</p>

                                <p><strong>Why does CareEdge collect PII?</strong></p>

                                <p>To process your transactions, maintain your account, ensure your adherence to our
                                    Terms of Use, and respond to your inquiries.</p>

                                <p>To provide you with information about services and products that we believe may be of
                                    interest to you offered by CareEdge.</p>

                                <p><strong>Use of PII</strong></p>

                                <p>We will use Personal Data that we collect solely for our internal use, including for
                                    statistical analysis of users. behavior, for product development, for content
                                    improvement, or to customize the content and layout of our Website. Nonpersonal Data
                                    may be used for internal or external purposes such as researching and identifying
                                    market segments and needs. We will not disclose to third parties any of your
                                    Personal Data, except to the extent necessary to comply with Legal or regulatory
                                    requirements,<br>
                                    with your express permission, to preserve our legal rights in the Website content
                                    and<br>
                                    in certain marketing situations, the names of the institutions that use any of our
                                    Website.<br>
                                    Please note that we may, in our sole discretion, transfer such information among our
                                    affiliated companies, wherever such affiliates are located, to successors, or
                                    potential successors, to the relevant business or<br>
                                    to independent contractors we engage to assist with or perform any of the foregoing
                                    activities.<br>
                                    In addition, we may, in our sole discretion, use Personal Data to contact you
                                    regarding your account status and changes to the subscriber agreement, this Policy,
                                    any other policies, agreements or transactions relevant to your use of the Website
                                    and marketing to you products and services which we believe may be of interest to
                                    you.</p>

                                <p><br>
                                    <strong>Collection of non-personal data</strong>
                                </p>

                                <p>In addition to PII, we may collect and/or track information that by itself cannot be
                                    used to identify or contact you, (e.g., demographic information (like age,
                                    profession or gender), domain names, computer type, browser types, and other
                                    anonymous statistical data involving the use of our Web Site (Non personal Data). We
                                    use Non personal Data to help us understand who uses our Web Site and to improve and
                                    market our Web Site.</p>

                                <p><strong>PII security measures</strong></p>

                                <p>We have established reasonable precautions to protect collected Personal Data from
                                    loss, misuse, unauthorized access, disclosure, alteration or destruction, which
                                    include contractual, administrative, physical, and technical steps. Technical
                                    measures include use of firewall protection and encryption technology where
                                    appropriate. Please note that while we have endeavoured to create a secure and
                                    reliable site for users, the confidentiality of any communication or material
                                    transmitted to/from us via the Website or e-mail cannot be guaranteed.</p>

                                <p><strong>Jurisdiction</strong></p>

                                <p>This policy shall be governed by the laws of India, without regard to conflicts of
                                    law rules, and the exclusive jurisdiction and venue for any dispute shall be Mumbai.
                                </p>

                                <p>Contact Information for CareEdge</p>

                                <p>The Data Controller for this website is Credit Analysis &amp; Research.</p>

                                <p>Questions or concerns regarding our privacy policy and data protection practices
                                    should be addressed to&nbsp;<a
                                        href="mailto:navin.jain@careratings.com">Nehal.Shah@careedge.in</a></p>

                                <p>BY USING OUR WEBSITE, YOU SIGNIFY YOUR ACCEPTANCE OF THIS PRIVACY POLICY AND OF US
                                    PROCESSING YOUR PII IN ACCORDANCE WITH THE TERMS OF THIS PRIVACY POLICY. IF YOU DO
                                    NOT AGREE WITH THE TERMS OF THIS PRIVACY POLICY, YOU SHOULD NOT USE OUR WEBSITE.
                                    YOUR CONTINUED USE OF OUR WEBSITE FOLLOWING THE POSTING OF CHANGES TO THIS PRIVACY
                                    POLICY WILL MEAN THAT YOU ALSO ACCEPT THOSE CHANGES.</p>

                            </div>
                        </div>
                    </div>




                </div>
            </div>
        </div>
    </div>










</section>
<?php include 'components/footer.php' ?>