<?php include 'components/header.php' ?>

<section class="faq-acc faq_acc_php padding-100 pt-0" id="renderHtmlRatingFaqsSectiondata">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <h2 class="heading-1 text-dark">FAQ’s – Credit Rating</h2>
            </div>
        </div>
    </div>

    <div class="accordian-style-1 style-2 accordion">
        <div class="accordion-item">
            <h2 class="accordion-header">
                <button id="btnId0" class="accordion-button collapsed" type="button"
                    jf-ext-button-ct="1 what is credit rating?">
                    <span class="acc_srno">1</span>
                    <span> What is Credit Rating?</span>
                </button>
            </h2>
            <div id="collapse1" class="accordion-collapse collapse show" style="display: none;">
                <div class="accordion-body">
                    <p>Credit rating is essentially the opinion of the rating agency on the relative ability and
                        willingness of the issuer of a debt instrument to meet the debt service obligations as and when
                        they arise.
                    </p>
                </div>
            </div>
        </div>

        <div class="accordion-item">
            <h2 class="accordion-header">
                <button id="btnId1" class="accordion-button collapsed" type="button" jf-ext-button-ct="2
how are craf's ratings in mauritius different from the ratings provided by global rating agencies?">
                    <span class="acc_srno">2</span>
                    <span> How are CRAF's ratings in Mauritius different from the ratings provided by global rating
                        agencies?</span>
                </button>
            </h2>
            <div id="collapse2" class="accordion-collapse collapse" style="display: none;">
                <div class="accordion-body">
                    <p>CRAF is a domestic credit rating agency in Mauritius and it is licensed by the FSC.</p>
                    <p>CRAF provides ratings on a 'Domestic Scale'. That is to say that credit rating for an entity
                        domiciled in Mauritius would be assigned based on the credit quality of the company relative to
                        the sovereign credit risk (of Government of Mauritius). As against this a rating on 'Global
                        Scale' would compare a Mauritian entity across the globe. The domestic scale ratings of CRAF
                        would lead to finer risk differentiation amongst entities in Mauritius.
                    </p>
                </div>
            </div>
        </div>

        <div class="accordion-item">
            <h2 class="accordion-header">
                <button id="btnId2" class="accordion-button collapsed" type="button" jf-ext-button-ct="3
what are the benefits of getting rated?">
                    <span class="acc_srno">3</span>
                    <span> What are the benefits of getting rated?</span>
                </button>
            </h2>
            <div id="collapse3" class="accordion-collapse collapse" style="display: none;">
                <div class="accordion-body">
                    <p>
                        A rating provides an independent opinion on the entity getting rated. CRAF will provide high
                        quality rating based on the expertise built by its parent CARE Ratings with over 30 years of
                        rating experience.
                    </p>

                    <p>
                        Rating can be used by lenders / deposit holders / bond investors of the company prior to taking
                        exposure in the rated entity.
                    </p>

                    <p>
                        Rating is supported by a Rating Rationale that provides a detailed analysis on the rated entity
                        and can be used by the company with its stakeholders
                    </p>
                </div>
            </div>
        </div>

        <div class="accordion-item">
            <h2 class="accordion-header">
                <button id="btnId3" class="accordion-button collapsed" type="button" jf-ext-button-ct="4
why do rating agencies use symbols like aaa, aa, rather than give marks or descriptive credit opinion?">
                    <span class="acc_srno">4</span>
                    <span> Why do rating agencies use symbols like AAA, AA, rather than give marks or descriptive credit
                        opinion?</span>
                </button>
            </h2>
            <div id="collapse4" class="accordion-collapse collapse">
                <div class="accordion-body">
                    <p>The great advantage of rating symbols is their simplicity, which facilitates universal
                        understanding. Rating companies also publish explanations for their symbols used as well as the
                        rationale for the ratings assigned by them, to facilitate deeper understanding.
                    </p>
                </div>
            </div>
        </div>

        <div class="accordion-item">
            <h2 class="accordion-header">
                <button id="btnId4" class="accordion-button collapsed" type="button" jf-ext-button-ct="5
does credit rating constitute an advice to the investors to buy?">
                    <span class="acc_srno">5</span>
                    <span> Does credit rating constitute an advice to the investors to buy?</span>
                </button>
            </h2>
            <div id="collapse5" class="accordion-collapse collapse">
                <div class="accordion-body">
                    <p>It does not. The reason is that some factors, which are of significance to an investor in
                        arriving at an investment decision, are not taken into account by rating agencies. These include
                        reasonableness of the issue price or the coupon rate, secondary market liquidity and pre-payment
                        risk. Further, different investors have different views regarding the level of risk to be taken
                        and rating agencies can only express their views on the relative credit risk.
                    </p>
                </div>
            </div>
        </div>

        <div class="accordion-item">
            <h2 class="accordion-header">
                <button id="btnId5" class="accordion-button collapsed" type="button"
                    jf-ext-button-ct="6
what kind of responsibility or accountability will attach to a rating agency if an investor, who makes his investment decision on the basis of its rating, incurs a loss on the investment?">
                    <span class="acc_srno">6</span>
                    <span> What kind of responsibility or accountability will attach to a rating agency if an investor,
                        who makes his investment decision on the basis of its rating, incurs a loss on the
                        investment?</span>
                </button>
            </h2>
            <div id="collapse6" class="accordion-collapse collapse">
                <div class="accordion-body">
                    <p>
                        A credit rating is a professional opinion given after studying all available information at a
                        particular point of time. Nevertheless, such opinions may prove wrong in the context of
                        subsequent events. Further, there is no privity of contract between an investor and a rating
                        agency and the investor is free to accept or reject the opinion of the agency. Nevertheless,
                        rating is essentially an investor service and a rating agency is expected to maintain the
                        highest possible level of analytical competence and integrity. In the long run, the credibility
                        of a rating agency has to be built, brick by brick, on the quality of its services.
                    </p>
                </div>
            </div>
        </div>

        <div class="accordion-item">
            <h2 class="accordion-header">
                <button id="btnId6" class="accordion-button collapsed" type="button" jf-ext-button-ct="7
how reliable and consistent is the rating process? how do rating agencies eliminate the subjective element in rating?">
                    <span class="acc_srno">7</span>
                    <span> How reliable and consistent is the rating process? How do rating agencies eliminate the
                        subjective element in rating?</span>
                </button>
            </h2>
            <div id="collapse7" class="accordion-collapse collapse">
                <div class="accordion-body">
                    <p>
                        To answer the second question first, it is neither possible nor even desirable, to totally
                        eliminate
                        the subjective element. Ratings do not come out of a pre-determined mathematical formula, which
                        fixes the relevant variables as well as the weights attached to each one of them. Rating
                        agencies do
                        a great amount of number crunching, but the final outcome also takes into account factors like
                        quality of management, corporate strategy, economic outlook and international environment. To
                        ensure
                        consistency and reliability, a number of qualified professionals are involved in the rating
                        process.
                        The Rating Committee, which assigns the final rating, consists of professionals with impeccable
                        credentials. Rating agencies also ensure that the rating process is insulated from any possible
                        conflicts of interest.
                    </p>
                </div>
            </div>
        </div>

        <div class="accordion-item">
            <h2 class="accordion-header">
                <button id="btnId7" class="accordion-button collapsed" type="button" jf-ext-button-ct="8
why do rating agencies monitor the issues already rated?">
                    <span class="acc_srno">8</span>
                    <span> Why do rating agencies monitor the issues already rated?</span>
                </button>
            </h2>
            <div id="collapse8" class="accordion-collapse collapse">
                <div class="accordion-body">
                    <p>A rating is an opinion given on the basis of information available at a particular point of time.
                        As time goes by, many things change, affecting the debt servicing capabilities of the issuer,
                        one way or the other. It is, therefore, essential that as a part of their investor service,
                        rating agencies monitor all outstanding debt issues rated by them. In the context of emerging
                        developments, the rating agencies often put issues under credit watch and upgrade or downgrade
                        the ratings as and when necessary. Normally, such action is taken after intensive interaction
                        with the issuers.
                    </p>
                </div>
            </div>
        </div>

        <div class="accordion-item">
            <h2 class="accordion-header">
                <button id="btnId8" class="accordion-button collapsed" type="button"
                    jf-ext-button-ct="9 do issuers have a right of appeal against a rating assigned?">
                    <span class="acc_srno">9</span>
                    <span> Do issuers have a right of appeal against a rating assigned?</span>
                </button>
            </h2>
            <div id="collapse9" class="accordion-collapse collapse">
                <div class="accordion-body">
                    <p>Yes. In a situation where an issuer is unhappy with the rating assigned, he may request for a
                        review, furnishing additional information, if any, considered relevant. The rating agency will,
                        then, undertake a review and thereafter indicate its final decision. Unless the rating agency
                        had overlooked critical information at the first stage, (which is unlikely), chances of the
                        rating being changed on appeal are rare.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>