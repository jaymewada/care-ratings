<?php include 'components/header.php' ?>
<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlStartedSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Disclaimer</li>
                    </ol>
                </nav>
                <div
                    class="d-flex justify-content-md-between align-items-xl-center align-items-baseline flex-column flex-md-row">
                    <h1 class="heading-1 text-white">Disclaimer</h1>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="capabilities-sec regulatory-disclosure bg-grey capabities-new-updated overflow-hidden"
    style="background-image:url(images/Capabilities.png);" id="renderHtmlRatingResourcesSectiondata">





    <div class="container-fluid left-space pe-0">
        <div class="accordian-style-1 row g-0">
            <div class="col-md-5 col-xl-3">
                <div class="accordian-title-box">
                    <span class="acc-title text-center"></span>
                    <span class="acc-count">1</span>
                </div>
            </div>
            <div class="col-md-12 col-xl-9">
                <div class="accordion">
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId0" class="accordion-button" type="button" jf-ext-button-ct="disclaimer">
                                Disclaimer
                            </button>
                        </h2>
                        <div id="collapse0" class="accordion-collapse collapse show">
                            <div class="accordion-body">
                                <p>CareEdge obtains all information furnished on the Web Site from sources believed by
                                    it to be accurate and reliable. You expressly agree that</p>

                                <p>1. The credit ratings and other opinions provided via the Web Site are, and will be
                                    construed solely as, statements of opinion of the relative future credit risk (as
                                    defined below) of entities, credit commitments, or debt or debt-like securities and
                                    not statements of current or historical fact as to credit worthiness, investment or
                                    financial advice, recommendations regarding credit decisions or decisions to
                                    purchase, hold or sell any securities, endorsements of the accuracy of any of the
                                    data or conclusions, or attempts to independently assess or vouch for the financial
                                    condition of any company;</p>

                                <p>2 .The credit ratings and other credit opinions provided via the Web Site do not
                                    address any other risk, including but not limited to liquidity risk, market value
                                    risk or price volatility;</p>

                                <p>3. The credit ratings and other opinions provided via the Web Site do not take into
                                    account your personal objectives, financial situations or needs;</p>

                                <p>4. Each rating or other opinion will be weighed, if at all, solely as one factor in
                                    any investment or credit decision made by or on behalf of you; and</p>

                                <p>5. You will accordingly make your own study and evaluation of each credit decision or
                                    security, and of each issuer and guarantor of, and each provider of credit support
                                    for, each security or credit that you may consider purchasing, holding, selling, or
                                    providing. Further, you expressly agree that any tools or information made available
                                    on the Web Site are not a substitute for the exercise of independent judgment and
                                    expertise. You should always seek the assistance of a professional for advice on
                                    investments, tax, the law, or other professional matters. For purposes of this
                                    paragraph, CareEdge defines “credit risk” as the risk that an entity may not meet
                                    its contractual, financial obligations as they come due and any estimated financial
                                    loss in the event of default.</p>

                                <p>CareEdge adopts all necessary measures so that the information it uses in assigning a
                                    credit rating is of sufficient quality and from sources CareEdge considers to be
                                    reliable, including, when appropriate, independent third-party sources. However,
                                    CareEdge is not an auditor and cannot in every instance independently verify or
                                    validate information received in the rating process. Because of the possibility of
                                    human and mechanical error as well as other factors, the Web Site and all related
                                    materials, products, services and information are provided on an "AS IS" and “AS
                                    AVAILABLE” basis without representation or warranty of any kind, and CareEdge AND
                                    ITS LICENSORS AND SUPPLIERS MAKE NO REPRESENTATION OR WARRANTY, EXPRESS OR IMPLIED,
                                    TO YOU OR ANY OTHER PERSON OR ENTITY AS TO THE ACCURACY, RESULTS, TIMELINESS,
                                    COMPLETENESS, MERCHANTABILITY, FITNESS FOR ANY PARTICULAR PURPOSE WITH RESPECT TO
                                    THE WEB SITE OR ANY RELATED MATERIALS, TOOLS, PRODUCTS, SERVICES OR INFORMATION.
                                    CareEdge makes no representation or warranty that any materials, tools, products,
                                    services or information on the Web Site are appropriate or available for use in any
                                    particular locations, and access to them from territories where any of the contents
                                    of this Web Site are illegal is prohibited. If you choose to access this Web Site
                                    from such locations, you do so on your own volition and are responsible for
                                    compliance with any applicable local laws, rules and regulations. CareEdge may limit
                                    the Web Site’s availability, in whole or in part, to any person, geographic area or
                                    jurisdiction we choose, at any time and in our sole discretion. You agree and
                                    acknowledge that no oral or written information or advice given by CareEdge or any
                                    of its employees or agents in respect to the Web Site shall constitute a
                                    representation or a warranty unless such information or advice is incorporated into
                                    these Terms of Use by a written agreement. FURTHER, THE INFORMATION, MATERIALS,
                                    TOOLS, PRODUCTS, AND SERVICES MADE AVAILABLE ON THIS WEB SITE MAY INCLUDE
                                    INACCURACIES OR TYPOGRAPHICAL ERRORS, AND THERE MAY BE TIMES WHEN THIS WEB SITE OR
                                    ITS CONTENTS ARE UNAVAILABLE. MOREOVER, CareEdge MAY MAKE MODIFICATIONS AND/OR
                                    CHANGES TO THE WEB SITE OR TO THE INFORMATION, MATERIALS, TOOLS, PRODUCTS, AND
                                    SERVICES DESCRIBED OR MADE AVAILABLE ON THE WEB SITE AT ANY TIME, FOR ANY REASON.
                                    YOU ASSUME THE SOLE RISK OF MAKING USE AND/OR RELYING ON THE INFORMATION, MATERIALS,
                                    TOOLS, PRODUCTS, AND SERVICES MADE AVAILABLE ON THE WEB SITE.</p>

                                <p>UNDER NO CIRCUMSTANCES WILL CareEdge OR ITS LICENSORS OR SUPPLIERS BE LIABLE TO YOU
                                    FOR ANY SPECIAL, INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES OF ANY KIND,
                                    INCLUDING, BUT NOT LIMITED TO, COMPENSATION, REIMBURSEMENT OR DAMAGES ON ACCOUNT OF
                                    THE LOSS OF PRESENT OR PROSPECTIVE PROFITS, EXPENDITURES, INVESTMENTS OR
                                    COMMITMENTS, WHETHER MADE IN THE ESTABLISHMENT, DEVELOPMENT OR MAINTENANCE OF
                                    BUSINESS REPUTATION OR GOODWILL, FOR LOSS OF DATA, COST OF SUBSTITUTE MATERIALS,
                                    PRODUCTS, SERVICES OR INFORMATION, COST OF CAPITAL, AND THE CLAIMS OF ANY THIRD
                                    PARTY, OR FOR ANY OTHER REASON WHATSOEVER, EVEN IF CARE OR ITS LICENSORS OR
                                    SUPPLIERS HAVE BEEN ADVISED OF THE POSSIBILITY OF DAMAGES.</p>

                                <p>Indemnity.</p>

                                <p>You agree to indemnify and hold harmless CareEdge, its licensors and suppliers, all
                                    of their affiliates, and all of their respective officers, directors, employees,
                                    shareholders, legal representatives, agents, successors and assigns, from and
                                    against any damages, liabilities, costs and expenses (including reasonable
                                    attorneys’ and professionals' fees and court costs) arising out of any third party
                                    claims based on or related to your use of the Web Site or any breach by you of these
                                    Terms of Use.</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>



<?php include 'components/footer.php' ?>