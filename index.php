<?php include "components/header.php" ?>

<section class="home-slider-sec position-relative">
    <!-- Swiper -->
    <div class="swiper main-slider">
        <div class="swiper-wrapper" id="renderHtmlHomeBannerSectiondata">
            <div class="swiper-slide">
                <img src="public/frontend-assets/images/banner-slider.png" class="img-fluid">
                <div class="banner-caption">
                    <h2 class="heading-1 mt-5 pt-5">CARE Ratings (Africa) </h2>
                    <h3 class="heading-3 mb-3 pb-3 pt-0">CARE Ratings Africa is proud to be collaborative partner with
                        Economic Development Board for Africa Partnership Conference held in October 2023. </h3>
                    <a href="#" class="btn btn-primary w-auto know-more-btn">Read More</a>
                </div>
                <div class="swiper-slide-shadow"></div>
            </div>
            <div class="swiper-slide">
                <img src="public/frontend-assets/images/banner-slider.png" class="img-fluid">
                <div class="banner-caption">
                    <h2 class="heading-1 mt-5 pt-5">CARE Ratings (Africa) </h2>
                    <h3 class="heading-3 mb-3 pb-3 pt-0">Assigned Issuer rating of CARE MAU AAA (Is) to The Mauritius
                        Commercial Bank Ltd </h3>
                </div>
            </div>
        </div>
        <div
            class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets swiper-pagination-horizontal">
            <span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 1"></span><span
                class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button"
                aria-label="Go to slide 2" aria-current="true"></span>
        </div>
        <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
    </div>
    <div class="recent-ratings-list">
        <h3 class="heading-3">Recent Ratings</h3>
        <div class="scrollbar">
            <ul class="p-0 m-0" data-mcs-theme="light" id="renderHtmlCompanyRecentReportsHomeSection">
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=PZ86TN0+jCiYLvRzlR03lw==">Attitude
                            Property Ltd</a> <span class="text-small text-light">29TH MARCH 2024</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=ZRr1eOreaL97AAuksfeFvQ==">CIM
                            Financial Services Ltd</a> <span class="text-small text-light">20TH FEBRUARY 2023</span>
                    </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=mXncS7RjGthCPRnggwxcPA==">Compagnie
                            des Villages de Vacances de l'Isle de France Limitée</a> <span
                            class="text-small text-light">5TH MARCH 2024</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=VlD1hRqgKi5khNSyQAij2A==">KASA
                            Investments Ltd</a> <span class="text-small text-light">22ND SEPTEMBER 2022</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=Dre0BNbjrYJEWXo+KrUFHA==">ENL
                            Limited</a> <span class="text-small text-light">23RD FEBRUARY 2024</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=qptZ+zQxMqwj++ZRmgm/HA==">SRL
                            Touessrok Hotel Ltd</a> <span class="text-small text-light">27TH FEBRUARY 2024</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=Ndf6D35i0E9eSCFDYlQ6NQ==">Anahita
                            Hotel Limited</a> <span class="text-small text-light">27TH FEBRUARY 2024</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=9MILcXwWR7KlwO5Oxbm/fA==">Lottotech
                            Ltd</a> <span class="text-small text-light">21ST FEBRUARY 2024</span> </div>
                </li>
                <li class="d-flex">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>
                    </div>
                    <div class="flex-grow-1"> <a class="m-0 d-block" href="search?Id=qBQObQ4Lf2vJSJYJxENWvg==">Evaco
                            Ltd</a> <span class="text-small text-light">14TH FEBRUARY 2024</span> </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="find-ratings">
        <div class="container-fluid ">
            <h3 class="heading-2 text-white m-0 font-regular pe-5 desktop-show1">Find Ratings</h3>
            <form>
                <h3 class="heading-2 text-white m-0 font-regular pe-5 desktop-hide1">Find Ratings</h3>
                <input type="text" class="form-control company-name" id="CompanybannerInputs"
                    oninput="myFunc1(this.value)" value="" placeholder="Search Company to Find Ratings" />
                <div id="res3" class="resultPan results1 hideResults1">
                    <div class="callout" data-closable>
                        <button class="close-button" aria-label="Close alert" type="button" data-close> <span
                                aria-hidden="true">&times;</span> </button>
                    </div>
                    <div class="row" id="CompanybannerRowInput">
                        <div class="col-lg-6">
                            <div class="organization-box">
                                <h3 class="heading-2 text-black mb-4 font-regular">Organization</h3>
                                <ul>
                                    <li><a href="search?Id=${data.CompanyID}">Company Name</a></li>
                                    <li><a href="search?Id=${data.CompanyID}">Company Name</a></li>
                                    <li><a href="search?Id=${data.CompanyID}">Company Name</a></li>
                                    <li><a href="search?Id=${data.CompanyID}">Company Name</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="reports-search-box">
                                <h3 class="heading-2 text-black mb-4 font-regular">Press Release</h3>
                                <ul>
                                    <li><a target="_blank" href="./upload/CompanyFiles/PR/${reportdata.pdf}">
                                            <p>Reportdata Title</p><span class="date-report">7 October ,2022</span>
                                        </a></li>
                                    <li><a target="_blank" href="./upload/CompanyFiles/PR/${reportdata.pdf}">
                                            <p>Reportdata Title</p><span class="date-report">7 October ,2022</span>
                                        </a></li>
                                    <li><a target="_blank" href="./upload/CompanyFiles/PR/${reportdata.pdf}">
                                            <p>Reportdata Title</p><span class="date-report">7 October ,2022</span>
                                        </a></li>
                                    <li><a target="_blank" href="./upload/CompanyFiles/PR/${reportdata.pdf}">
                                            <p>Reportdata Title</p><span class="date-report">7 October ,2022</span>
                                        </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-white ms-auto desktop-hide1">
                    <a href="find-ratings">FIND RATINGS</a>
                </button>
                <button class="btn desktop-show1 arrow-btn ms-auto">
                    <svg xmlns="http://www.w3.org/2000/svg" width="44" height="44" viewBox="0 0 44 44">
                        <g id="Group_209" data-name="Group 209" transform="translate(-1728 -1881)">
                            <circle id="Ellipse_11" data-name="Ellipse 11" cx="22" cy="22" r="22"
                                transform="translate(1728 1881)" fill="#fff"></circle>
                            <g id="Group_135" data-name="Group 135" transform="translate(1213 1811)">
                                <rect id="Rectangle_64" data-name="Rectangle 64" width="24" height="24"
                                    transform="translate(525 80)" fill="#00d3c3" opacity="0"></rect>
                                <path id="Union_11" data-name="Union 11"
                                    d="M.293,13.021a1,1,0,0,1,0-1.415l4.95-4.95L.293,1.707A1,1,0,0,1,1.707.293L7.364,5.95a1,1,0,0,1,0,1.414L1.707,13.021a1,1,0,0,1-1.415,0Z"
                                    transform="translate(530.515 95.828) rotate(-90)" fill="#00d3c3"></path>
                            </g>
                        </g>
                    </svg>
                </button>
                <div class="pms-cons-copm d-none">
                    <ul class="p-0 m-0">
                        <li class="d-flex border-0">
                            <div class="flex-shrink-0">
                                <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                                    viewBox="0 0 12.167 14.833">
                                    <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                        <path id="Path_18" data-name="Path 18"
                                            d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                            transform="translate(-4 -2)" fill="none" stroke="#fff"
                                            stroke-linecap="round" stroke-linejoin="bevel" stroke-width="1.5"
                                            fill-rule="evenodd" />
                                    </g>
                                </svg>
                            </div>
                            <div class="flex-grow-1"> <a class="m-0 d-block" href="javascript:void(0)">PMS Construction
                                    Company</a> <span class="text-small text-light">24TH MAY 2021</span> </div>
                        </li>
                    </ul>
                </div>
            </form>
        </div>
    </div>
</section>

<section class="capabilities-sec bg-grey overflow-hidden"
    style="background-image:url(public/frontend-assets/images/Capabilities.png);">
    <div class="container-fluid left-space pe-0" id="renderHtmlHomeCapabilitySectorSectiondata">
        <div class="row">
            <div class="col-md-12">
                <h3 class="heading-1 text-black">Capabilities</h3>
            </div>
        </div>
        <div class="accordian-style-1 mt-5 row g-0">
            <div class="col-md-5 col-xl-3">
                <div class="accordian-title-box"> <span class="acc-title text-center">Corporate Sector</span> <span
                        class="acc-count">1</span> </div>
            </div>
            <div class="col-md-12 col-xl-9">
                <div class="accordion">
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId0" class="accordion-button" type="button"> Corporate Sector </button>
                        </h2>
                        <div id="collapse0" class="accordion-collapse collapse show">
                            <div class="accordion-body">
                                <p>CARE Ratings’ credit ratings are its opinion on the relative ability and willingness
                                    of the issuer to meet the debt service obligations as and when they arise. Rating
                                    determination is a matter of experienced and holistic judgement, based on the
                                    relevant quantitative and qualitative factors affecting the credit quality of the
                                    issuer. CARE Ratings has based its ratings/outlooks on information obtained from
                                    sources believed by it to be accurate and reliable.Within the corporate sector, CARE
                                    Ratings rates debt instruments and bank facilities of manufacturing entities,
                                    service entities, trading entities, PSUs etc.</p>
                                <!-- <a href="corporate-sector" class="btn btn-link primary p-0 w-auto">Read
                           More</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId1" class="accordion-button collapsed" type="button"> Financial Sector
                            </button>
                        </h2>
                        <div id="collapse1" class="accordion-collapse collapse">
                            <div class="accordion-body">
                                <p>CARE Ratings rating determination is a matter of experienced and holistic judgment,
                                    based on the relevant quantitative and qualitative factors affecting the credit
                                    quality of the issuer. The analytical framework of CARE Ratings' rating methodology
                                    is divided into two interdependent segments - The first deals with the operational
                                    characteristics and the second with the financial characteristics. Besides
                                    quantitative factors, qualitative aspects like an assessment of management
                                    capabilities play a very important role in arriving at the rating for an instrument.
                                </p>
                                <!-- <a href="financial-sector" class="btn btn-link primary p-0 w-auto">Read
                           More</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId2" class="accordion-button collapsed" type="button"> Structured Finance
                            </button>
                        </h2>
                        <div id="collapse2" class="accordion-collapse collapse">
                            <div class="accordion-body">
                                <p>Structured Finance rating encompasses both retail assets and corporate assets in the
                                    Indian context. The space covers both off-balance sheet and on-balance sheet
                                    structures. It focuses on opining on specific customised solutions and largely
                                    covers all forms of facilities whether extended in loan form or tradable instrument
                                    form.In the corporate assets space, partial/full guarantee structures, pooled
                                    issuances have recently become prevalent. In retail assets space, securitization and
                                    direct assignment are prevalent in India. CARE Ratings uses the suffix ‘(SO)’ for
                                    securitization transactions indicating that these are structured obligations.</p>
                                <!-- <a href="structured-finance" class="btn btn-link primary p-0 w-auto">Read
                           More</a>-->
                            </div>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId3" class="accordion-button collapsed" type="button"> Infrastructure Sector
                            </button>
                        </h2>
                        <div id="collapse3" class="accordion-collapse collapse">
                            <div class="accordion-body">
                                <p>CARE Ratings’ Infrastructure Sector Ratings (ISR) encompass ratings assigned to debt
                                    programs of issuers in the thermal power, solar energy, wind energy, hydro energy,
                                    transmission, power distribution companies, roads (toll, annuity, and hybrid
                                    annuity), telecommunications, airports, ports, and other such infrastructure-related
                                    sectors.CARE Ratings adopts a separate methodology for evaluation and assignment of
                                    Infrastructure Sector Ratings, distinct from corporate sector debt ratings as rating
                                    drivers for Infrastructure Sector Ratings are quite different from that of the
                                    corporate sector.</p>
                                <!-- <a href="infrastructure-sector" class="btn btn-link primary p-0 w-auto">Read
                           More</a>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="reports-sec padding-100">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-12 d-flex justify-content-between col-lg-12  report-sec-head">
                <h2 class="heading-1 text-dark mb-5">Reports</h2>
                <div class="mt-xl-3 d-flex">
                    <div>
                        <div class="slider-arrows arrow-1 justify-content-end"> <span
                                class="prev-btn slick-arrow slick-hidden" aria-disabled="true" tabindex="-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                    class="bi bi-chevron-left" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                        d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z">
                                    </path>
                                </svg>
                            </span> <span class="next-btn slick-arrow slick-hidden" aria-disabled="true" tabindex="-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                    class="bi bi-chevron-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                        d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z">
                                    </path>
                                </svg>
                            </span> </div>
                    </div>
                    <div class="d-flex report-custom-btn1 flex-row-reverse ">
                        <div class="dropdown-toggle" id="renderHtmlRecentInsightsYeardata">
                            <select class="empInput form-control" name="InsightsYear_Id" id="InsightsYear_Id">
                                <option value="2023" id="2023">2023</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12  col-lg-4">
                <hr class="style-1">
                <h3 class="heading-2 text-dark"> Latest Industry<br> &amp; Economic<br> Research </h3>
            </div>
            <div class="col-md-12 col-lg-8">
                <div class="row commons-sliderInsights sliderGap-30">
                    <div class="item all 2022">
                        <a href="#" target="_blank">
                            <div class="card card-style-1">
                                <span class="arrow-link">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                        viewBox="0 0 15.28 15.256">
                                        <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                            <line id="Line_60" data-name="Line 60" y1="13.182"
                                                transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2" />
                                            <path id="Path_51" data-name="Path 51"
                                                d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                                transform="translate(-7.26 -3.929)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2" />
                                        </g>
                                    </svg>
                                </span>
                                <div class="card-body">
                                    <span class="text-grey text-small">2 April 2024</span>
                                    <p class="heading-3 text-white mt-3">Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit. In faucibus arcu nec dignissim iaculis Nun.</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="item all 2022">
                        <a href="#" target="_blank">
                            <div class="card card-style-1">
                                <span class="arrow-link">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                        viewBox="0 0 15.28 15.256">
                                        <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                            <line id="Line_60" data-name="Line 60" y1="13.182"
                                                transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2" />
                                            <path id="Path_51" data-name="Path 51"
                                                d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                                transform="translate(-7.26 -3.929)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2" />
                                        </g>
                                    </svg>
                                </span>
                                <div class="card-body">
                                    <span class="text-grey text-small">2 April 2024</span>
                                    <p class="heading-3 text-white mt-3">Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit. In faucibus arcu nec dignissim iaculis Nun.</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="item all 2022">
                        <a href="#" target="_blank">
                            <div class="card card-style-1">
                                <span class="arrow-link">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                        viewBox="0 0 15.28 15.256">
                                        <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                            <line id="Line_60" data-name="Line 60" y1="13.182"
                                                transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2" />
                                            <path id="Path_51" data-name="Path 51"
                                                d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                                transform="translate(-7.26 -3.929)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2" />
                                        </g>
                                    </svg>
                                </span>
                                <div class="card-body">
                                    <span class="text-grey text-small">2 April 2024</span>
                                    <p class="heading-3 text-white mt-3">Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit. In faucibus arcu nec dignissim iaculis Nun.</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="item all 2022">
                        <a href="#" target="_blank">
                            <div class="card card-style-1">
                                <span class="arrow-link">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                        viewBox="0 0 15.28 15.256">
                                        <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                            <line id="Line_60" data-name="Line 60" y1="13.182"
                                                transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2" />
                                            <path id="Path_51" data-name="Path 51"
                                                d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                                transform="translate(-7.26 -3.929)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2" />
                                        </g>
                                    </svg>
                                </span>
                                <div class="card-body">
                                    <span class="text-grey text-small">2 April 2024</span>
                                    <p class="heading-3 text-white mt-3">Lorem ipsum dolor sit amet, consectetur
                                        adipiscing elit. In faucibus arcu nec dignissim iaculis Nun.</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="budget-sec padding-100">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 col-lg-4">
                <hr class="style-1">
                <h3 class="heading-2 text-dark">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In faucibus
                    arcu nec dignissim iaculis Nun.</h3>
                <p class="heading-5 text-grey mb-5">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In faucibus
                    arcu nec dignissim iaculis Nun.</p>
                <div class="d-flex align-items-center"> <a data-bs-toggle="modal"
                        data-url="https://www.youtube.com/embed/E0Jwro7fp40" href="exampleModalToggle-v1"
                        class="play__btn play_php btn btn-primary me-3">WATCH VIDEO</a> <a href="media"
                        class="btn btn-link primary p-0 w-auto">View MORE</a> </div>
            </div>
            <div class="col-md-12  col-lg-8">
                <div class="video-box">
                    <img src="public/frontend-assets/images/video-thumbnail.jpg" class="img-fluid">
                    <div class="vide_playbtn"> <a target="_blank" class="play__btn play_php" data-bs-toggle="modal"
                            data-url="https://www.youtube.com/embed/E0Jwro7fp40" href="exampleModalToggle-v1">
                            <span></span> </a> </div>
                </div>
            </div>
        </div>

        <!-- Modal popup for desktop-->
        <div class="modal fade login-popup budgetVid-popup" id="exampleModalToggle-v1" aria-hidden="true"
            aria-labelledby="exampleModalToggleLabel-v1" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                    </div>
                    <div class="modal-body">
                        <div class="eventGal_modal-content">
                            <iframe width="960" height="560" id="video_url_php"
                                src="https://www.youtube.com/embed/E0Jwro7fp40" title="YouTube video player"
                                frameborder="0"
                                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                                allowfullscreen=""></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="care-rating-rec overview_php">
    <div class="container-fluid" id="renderHtmlHomeOverviewSectiondata">
        <div class="row justify-content-center">
            <div class="col-md-12 col-xxl-9"> <span class="e-logo">
                    <svg xmlns="http://www.w3.org/2000/svg" width="434.982" height="447.471"
                        viewBox="0 0 434.982 447.471">
                        <g id="Group_8587" data-name="Group 8587" transform="translate(86.022 -176.042)" opacity="0.2">
                            <path id="Path_11954" data-name="Path 11954"
                                d="M-51.506,176.542l-28.088,97.1H218.945l31.3-97.1Z"
                                transform="translate(98.031 -0.001)" fill="#fff" stroke="#a1a1a1" stroke-width="1">
                            </path>
                            <path id="Path_11955" data-name="Path 11955"
                                d="M-54.476,186.239l-28.088,97.106H215.974l31.3-97.106Z"
                                transform="translate(47.5 164.985)" fill="#fff" stroke="#a1a1a1" stroke-width="1">
                            </path>
                            <path id="Path_11956" data-name="Path 11956"
                                d="M-57.268,195.937l-28.089,97.106H213.182l31.3-97.106Z"
                                transform="translate(0 329.969)" fill="#fff" stroke="#a1a1a1" stroke-width="1"></path>
                        </g>
                    </svg>
                </span>
                <p class="heading-2 font-regular">CARE Ratings (Africa) Private Limited (CRAF) is incorporated in
                    Mauritius and is the first credit rating agency to be licensed by the Financial Services Commission
                    of Mauritius from May 7, 2015. It is also recognized by Bank of Mauritius as External Credit
                    Assessment Institution (ECAI) from May 9, 2016. CRAF provides credit ratings and related services in
                    Mauritius and has plans to venture into other geographies of Africa. </p>
            </div>
        </div>
    </div>
</section>

<section class="CareEdgeeventgallery-sec padding-100 eventGallery-modal yearDropDown-main">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="d-flex justify-content-md-between flex-column flex-md-row">
                    <div>
                        <h3 class="heading-1 text-black">Events Gallery</h3>
                    </div>
                    <div
                        class="mt-xl-3 custom-dropdown d-flex filter-dropdown  dropdown-menu-outiline  dropdown mb-5  mb-lg-0 ">
                        <div class="row justify-content-center">
                            <div class="slider-arrows"> <span class="prev-btn slick-arrow slick-hidden"
                                    aria-disabled="true" tabindex="-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-chevron-left" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z">
                                        </path>
                                    </svg>
                                </span> <span class="next-btn slick-arrow slick-hidden" aria-disabled="true"
                                    tabindex="-1">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                        class="bi bi-chevron-right" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z">
                                        </path>
                                    </svg>
                                </span> </div>
                        </div>
                        <div class="d-flex sasifb flex-row-reverse ">
                            <div class="yearToggle" id="renderHtmlHomeEventsGalleryYeardata">
                                <select class="empInput form-control" name="Year_Id" id="Year_Id">
                                    <option value="2023" id="2023">2023</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid" id="renderHtmlHomeEventsGallerySectiondata">
        <div class="CareEdgeeventgallery-slider left-space-sldier overflow-hidden mb-2 pt-0">
            <div class="item latest">
                <div class="card card-style-2">
                    <a class="btn trigger openModal_php" data-bs-toggle="modal" href="#exampleModalToggle-e1"
                        data-url="public/frontend-assets/images/thumbnail.jpg"
                        data-desc="Lorem ipsum dolor sit amet, consectetur adipiscing elit. In faucibus arcu nec dignissim iaculis Nun."
                        role="button">
                        <div class="card-image position-relative">
                            <img src="public/frontend-assets/images/thumbnail.jpg" class="card-img-top img-fluid"
                                alt="...">
                            <label class="date-lable">2 April 2024</label>
                        </div>
                    </a>
                    <div class="card-body border-0 p-0">
                        <h3 class="heading-2 mt-4 mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
                            faucibus arcu nec dignissim iaculis Nun.</h3>
                    </div>
                </div>
            </div>
            <div class="item latest">
                <div class="card card-style-2">
                    <a class="btn trigger openModal_php" data-bs-toggle="modal" href="#exampleModalToggle-e1"
                        data-url="public/frontend-assets/images/thumbnail.jpg"
                        data-desc="Lorem ipsum dolor sit amet, consectetur adipiscing elit. In faucibus arcu nec dignissim iaculis Nun."
                        role="button">
                        <div class="card-image position-relative">
                            <img src="public/frontend-assets/images/thumbnail.jpg" class="card-img-top img-fluid"
                                alt="...">
                            <label class="date-lable">2 April 2024</label>
                        </div>
                    </a>
                    <div class="card-body border-0 p-0">
                        <h3 class="heading-2 mt-4 mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
                            faucibus arcu nec dignissim iaculis Nun.</h3>
                    </div>
                </div>
            </div>
            <div class="item latest">
                <div class="card card-style-2">
                    <a class="btn trigger openModal_php" data-bs-toggle="modal" href="#exampleModalToggle-e1"
                        data-url="public/frontend-assets/images/thumbnail.jpg"
                        data-desc="Lorem ipsum dolor sit amet, consectetur adipiscing elit. In faucibus arcu nec dignissim iaculis Nun."
                        role="button">
                        <div class="card-image position-relative">
                            <img src="public/frontend-assets/images/thumbnail.jpg" class="card-img-top img-fluid"
                                alt="...">
                            <label class="date-lable">2 April 2024</label>
                        </div>
                    </a>
                    <div class="card-body border-0 p-0">
                        <h3 class="heading-2 mt-4 mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
                            faucibus arcu nec dignissim iaculis Nun.</h3>
                    </div>
                </div>
            </div>
            <div class="item latest">
                <div class="card card-style-2">
                    <a class="btn trigger openModal_php" data-bs-toggle="modal" href="#exampleModalToggle-e1"
                        data-url="public/frontend-assets/images/thumbnail.jpg"
                        data-desc="Lorem ipsum dolor sit amet, consectetur adipiscing elit. In faucibus arcu nec dignissim iaculis Nun."
                        role="button">
                        <div class="card-image position-relative">
                            <img src="public/frontend-assets/images/thumbnail.jpg" class="card-img-top img-fluid"
                                alt="...">
                            <label class="date-lable">2 April 2024</label>
                        </div>
                    </a>
                    <div class="card-body border-0 p-0">
                        <h3 class="heading-2 mt-4 mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
                            faucibus arcu nec dignissim iaculis Nun.</h3>
                    </div>
                </div>
            </div>
            <div class="item latest">
                <div class="card card-style-2">
                    <a class="btn trigger openModal_php" data-bs-toggle="modal" href="#exampleModalToggle-e1"
                        data-url="public/frontend-assets/images/thumbnail.jpg"
                        data-desc="Lorem ipsum dolor sit amet, consectetur adipiscing elit. In faucibus arcu nec dignissim iaculis Nun."
                        role="button">
                        <div class="card-image position-relative">
                            <img src="public/frontend-assets/images/thumbnail.jpg" class="card-img-top img-fluid"
                                alt="...">
                            <label class="date-lable">2 April 2024</label>
                        </div>
                    </a>
                    <div class="card-body border-0 p-0">
                        <h3 class="heading-2 mt-4 mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
                            faucibus arcu nec dignissim iaculis Nun.</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal popup for desktop-->
    <div class="modal fade login-popup eventGal-popup" id="exampleModalToggle-e1" aria-hidden="true"
        aria-labelledby="exampleModalToggleLabel-e1" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                </div>
                <div class="modal-body">
                    <div class="eventGal_modal-content"> <img class="eventGal-popup-img" src="" alt="" width="100%">
                        <div class="eventGal_modal_text">
                            <p class="eventGal_modal_text-para"> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal popup for desktop-->
</section>

<section class="get-rated-sec" style="background-image: url(public/frontend-assets/images/get-rated.png);">
    <div class="container-fluid" id="renderHtmlHomeGetRatedSectiondata">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="heading-1 text-white ">Get Rated</h2>
                        <hr class="style-2">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xl-3 col-xs-3 col-sm-3">
                        <div class="card card-style-1 green"> <a href="rating-process" class="arrow-link">
                                <svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                    viewBox="0 0 15.28 15.256">
                                    <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                        <line id="Line_60" data-name="Line 60" y1="13.182"
                                            transform="translate(3.871 0.001)" fill="none" stroke="#5dd4c4"
                                            stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></line>
                                        <path id="Path_51" data-name="Path 51"
                                            d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                            transform="translate(-7.26 -3.929)" fill="none" stroke="#5dd4c4"
                                            stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></path>
                                    </g>
                                </svg>
                            </a>
                            <div class="card-body">
                                <p class="heading-3 text-primary mt-5 font-light">Get Rated</p>
                                <a href="rating-process" class="btn btn-primary mt-2">KNOW MORE</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-8 offset-xl-1">
                        <p class="heading-2 text-white font-regular">The rating process takes about two to three weeks,
                            depending on the complexity of the assignment and the flow of information from the client.
                            Ratings are assigned by the Rating Committee.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>


<script>
var companyData = [];
var resultObject = [];
let results1 = document.getElementById("res3");

function myFunc1(value) {
    if (value !== "") {
        results1.classList.remove("hideResults1");
    } else {
        results1.classList.add("hideResults1");
    }
}

$(document).ready(function() {
    // CommentrySectiondata();
    $("#CompanybannerInputs").keyup(function() {
        var cinput = $("#CompanybannerInputs").val();
        if (cinput.length >= 3) {
            //console.log(cinput);
            //bannerSearchSectiondata(cinput);
            // return true;
        }
    });

    var mainslider = new Swiper(".main-slider", {
        grabCursor: true,
        effect: "creative",
        speed: 1500,
        autoplay: true,
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        pagination: {
            el: ".swiper-pagination",
            clickable: !0,
        },
        creativeEffect: {
            prev: {
                shadow: true,
                translate: ["-50%", 0, -1],
            },
            next: {
                translate: ["100%", 0, 0],
            },
        },
    });

    if ($(window).width() > 1024) {
        mainslider.on("slideChangeTransitionStart", function() {
            gsap.fromTo(
                ".banner-caption .heading-3", {
                    opacity: 0,
                    y: 100
                }, {
                    opacity: 1,
                    y: 0,
                    duration: 1,
                    delay: 1
                }
            );
            gsap.fromTo(
                ".banner-caption .heading-1", {
                    opacity: 0,
                    y: 100
                }, {
                    opacity: 1,
                    y: 0,
                    duration: 1,
                    delay: 1
                }
            );
            gsap.fromTo(
                ".banner-caption .know-more-btn", {
                    opacity: 0,
                    y: 50
                }, {
                    opacity: 1,
                    y: 0,
                    duration: 1,
                    delay: 1
                }
            );

            // setTimeout(() =>{
            //     //$('.swiper-slide-active .color_slide').css('transform', 'translateX(-200px)');
            //    //  $('.swiper-slide-active img').css('clip-path', 'polygon(50% 0%, 100% 1%, 100% 100%, 0 100%, 0 0)');
            // },500)
        });
    }

    function InsightsSliderdata() {
        $(".commons-sliderInsights").slick({
            infinite: false,
            speed: 1000,
            slidesToShow: 3,
            slidesToScroll: 3,
            prevArrow: ".arrow-1 .prev-btn",
            nextArrow: ".arrow-1 .next-btn",
            responsive: [{
                    breakpoint: 1100,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true,
                    },
                },
                {
                    breakpoint: 1099,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true,
                    },
                },

                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2.1,
                        slidesToScroll: 1,
                        dots: true,
                        centerMode: true,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1.1,
                        slidesToScroll: 1,
                        dots: true,
                        // centerMode: true,
                        // centerPadding: "50px",
                    },
                },
            ],
        });
    }
    InsightsSliderdata();

    const EventsGallerySlider = (
        sliderName,
        dots,
        fade,
        infinite,
        slidesToShow,
        slidesToScroll,
        prevArrow,
        nextArrow,
        variableWidth
    ) => {
        $(sliderName).slick({
            dots: dots,
            fade: fade,
            infinite: infinite,
            speed: 1000,
            slidesToShow: slidesToShow,
            slidesToScroll: slidesToScroll,
            prevArrow: prevArrow,
            nextArrow: nextArrow,
            variableWidth: variableWidth,
            responsive: [{
                    breakpoint: 1100,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true,
                    },
                },
                {
                    breakpoint: 1099,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true,
                    },
                },

                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        dots: true,
                    },
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true,
                    },
                },
            ],
        });
    };

    EventsGallerySlider(".CareEdgeeventgallery-slider", false, false, false, 2, 1,
        ".CareEdgeeventgallery-sec .prev-btn", ".CareEdgeeventgallery-sec .next-btn", false);
});

$(document).on("click", ".openModal_php", function() {
    var ImgUrl = $(this).data('url');
    var desc = $(this).data('desc');
    $(".modal-body .eventGal-popup-img").attr("src", ImgUrl);
    $(".modal-body .eventGal_modal_text").html(desc);
});
</script>