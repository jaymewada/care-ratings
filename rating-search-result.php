<?php include 'components/header.php' ?>
<section class="inner-banner bg-secondary jumbotron">
	<div class="container-fluid py-5">
		<div class="row justify-content-center" id="renderHtmlInvestorBreadcrumSection">
			<div class="col-md-11">
				<nav aria-label="breadcrumb">
					<ol class="breadcrumb mb-5">
						<li class="breadcrumb-item"><a href="./">Home</a></li>
						<li class="breadcrumb-item" aria-current="page">Find Ratings</li>
						<li class="breadcrumb-item active" aria-current="page">JSW Cement Limited</li>
					</ol>
				</nav>
				<h1 class="heading-1 text-white">JSW Cement Limited</h1>
			</div>
		</div>
	</div>
</section>

<section class="padding-100">
	<div class="container-fluid">
		<div class="row">
				<div class="col-md-10 mx-auto">
					<h2 class="heading-1 mb-3 mb-md-4 text-dark">JSW Cement Limited</h2>

					<div class="table-responsive">
						<table class="table table-style-1 table-borderless">
							<thead>
								<tr>
									<th scope="col">Instruments</th>
									<th scope="col">Rated Amount ( ₹ in Million )</th>
									<th scope="col">Rating</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>BG/LC</td>
									<td>0.00</td>
									<td>Withdrawn</td>
								</tr>
								<tr>
									<td>Cash Credit</td>
									<td>0.00</td>
									<td>Withdrawn</td>
								</tr>
								<tr>
									<td>Long Term</td>
									<td>0.00</td>
									<td>Withdrawn</td>
								</tr>
								<tr>
									<td>Term Loan</td>
									<td>0.00</td>
									<td>Withdrawn</td>
								</tr>
							</tbody>
						</table>
					</div>

					<p class="print-details">
						# - Rating Withdrawn, @ - Rating Watch, $ - Rating/Grading not in use, % - Instrument redeemed, ^ On Notice
						of withdrawal
						<br />
						&amp; - Rating suspended due to non-submission of information by the Company
						<br />
						* - Issuer Not Cooperating, Based on Best Available Information
					</p>
					<div class="d-flex flex-column flex-lg-row align-items-start align-items-lg-center mt-5">
						<a
							type="button"
							href="javascript:void(0)"
							class="btn btn-primary me-3"
							id="printableArea"
							value="print a div!"
							>PRINT RATING</a
						>
						<a
							href="https://www.careratings.com/rating-symbols-and-definition"
							class="btn btn-link primary p-0 w-auto me-3 mt-lg-0 mt-2"
							target="_blank"
							>RATING SYMBOL</a
						>
					</div>
				</div>
			</div>
	</div>
</section>

<section class="press-relase-sec yearDropDown-main">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4"></div>
			<div class="col-lg-8">
				<div class="d-md-flex justify-content-between align-items-center mb-lg-5">
					<h2 class="heading-1 text-dark">Press Releases</h2>
					<div class="d-flex justify-content-between justify-content-lg-end align-items-center">
						<div class="yearToggle m-0">
							<select class="empInput form-control" name="RRYear_Id" id="RRYear_Id" jf-ext-cache-id="10">
								<option value="2012" id="2012">2012</option>
								<option value="2011" id="2011">2011</option>
								<option value="2010" id="2010">2010</option>
							</select>
						</div>

						<span class="mx-2"></span>

						<div class="slider-arrows small-card-slider-buttons m-0">
							<span class="btn-prev">
								<svg
									xmlns="http://www.w3.org/2000/svg"
									width="16"
									height="16"
									fill="currentColor"
									class="bi bi-chevron-left"
									viewBox="0 0 16 16"
								>
									<path
										fill-rule="evenodd"
										d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"
									></path>
								</svg>
							</span>
							<span class="btn-next">
								<svg
									xmlns="http://www.w3.org/2000/svg"
									width="16"
									height="16"
									fill="currentColor"
									class="bi bi-chevron-right"
									viewBox="0 0 16 16"
								>
									<path
										fill-rule="evenodd"
										d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"
									></path>
								</svg>
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-4 align-self-center">
				<hr class="style-1" />
				<h3 class="heading-2 text-dark mb-5">
					Press releases<br />
					of the Company
				</h3>
			</div>

			<div class="col-lg-8">
				<div class="small-cards-slider sliderGap-30">
					<?php for ($x = 0; $x <= 10; $x++) { ?>
					<div class="item">
						<a href="">
							<div class="card card-style-1">
								<span class="arrow-link">
									<svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256" viewBox="0 0 15.28 15.256">
										<g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
											<line
												id="Line_60"
												data-name="Line 60"
												y1="13.182"
												transform="translate(3.871 0.001)"
												fill="none"
												stroke="#fff"
												stroke-linecap="round"
												stroke-linejoin="bevel"
												stroke-width="2"
											></line>
											<path
												id="Path_51"
												data-name="Path 51"
												d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
												transform="translate(-7.26 -3.929)"
												fill="none"
												stroke="#fff"
												stroke-linecap="round"
												stroke-linejoin="bevel"
												stroke-width="2"
											></path>
										</g>
									</svg>
								</span>

								<div class="card-body">
									<span class="text-grey text-small">8TH APRIL 2021</span>
									<p class="heading-3 text-white">JSW Cement Limited</p>
								</div>
							</div>
						</a>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="padding-100">
	<div class="">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-md-10">
					<h2 class="heading-1 mb-3 mb-md-4 text-dark">Bank Facilities Rated</h2>
					<div class="table-responsive">
						<table class="table table-style-1 table-borderless">
							<thead>
								<tr>
									<th scope="col">Type of Facility</th>
									<th scope="col">Instrument</th>
									<th scope="col">Bank Name</th>
									<th scope="col">Rated Amount ( ₹ Cr )</th>
								</tr>
							</thead>
							<tbody class="bank_body">
								<tr>
									<td>Fund-based</td>
									<td>Long Term</td>
									<td>HDFC Bank Ltd.</td>
									<td>13.00</td>
								</tr>

								<tr>
									<td>Non-fund-based - ST</td>
									<td>BG/LC</td>
									<td>HDFC Bank Ltd.</td>
									<td>10.00</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="padding-100">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4"></div>
			<div class="col-lg-8">
				<div class="d-md-flex justify-content-between align-items-center mb-lg-5">
					<h2 class="heading-1 text-dark">Similar Sector Companies</h2>
					<div class="d-flex justify-content-between justify-content-lg-end align-items-center">
						<div class="slider-arrows small-card-slider-buttons m-0">
							<span class="btn-prev">
								<svg
									xmlns="http://www.w3.org/2000/svg"
									width="16"
									height="16"
									fill="currentColor"
									class="bi bi-chevron-left"
									viewBox="0 0 16 16"
								>
									<path
										fill-rule="evenodd"
										d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"
									></path>
								</svg>
							</span>
							<span class="btn-next">
								<svg
									xmlns="http://www.w3.org/2000/svg"
									width="16"
									height="16"
									fill="currentColor"
									class="bi bi-chevron-right"
									viewBox="0 0 16 16"
								>
									<path
										fill-rule="evenodd"
										d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"
									></path>
								</svg>
							</span>
						</div>
					</div>
				</div>
			</div>

			<div class="col-lg-4 align-self-center">
				<hr class="style-1" />
				<h3 class="heading-2 text-dark mb-5">
					Ratings of the <br />
					Companies
				</h3>
			</div>

			<div class="col-lg-8">
				<div class="small-cards-slider sliderGap-30">
					<?php for ($x = 0; $x <= 10; $x++) { ?>
					<div class="item">
						<a href="">
							<div class="card card-style-1">
								<span class="arrow-link">
									<svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256" viewBox="0 0 15.28 15.256">
										<g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
											<line
												id="Line_60"
												data-name="Line 60"
												y1="13.182"
												transform="translate(3.871 0.001)"
												fill="none"
												stroke="#fff"
												stroke-linecap="round"
												stroke-linejoin="bevel"
												stroke-width="2"
											></line>
											<path
												id="Path_51"
												data-name="Path 51"
												d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
												transform="translate(-7.26 -3.929)"
												fill="none"
												stroke="#fff"
												stroke-linecap="round"
												stroke-linejoin="bevel"
												stroke-width="2"
											></path>
										</g>
									</svg>
								</span>

								<div class="card-body">
									<span class="text-grey text-small">8TH APRIL 2021</span>
									<p class="heading-3 text-white">Bhavya Cements Private Limited</p>
								</div>
							</div>
						</a>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php include "components/footer.php" ?>
