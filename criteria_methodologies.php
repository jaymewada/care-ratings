<?php include 'components/header.php' ?>

<section class="capabilities-sec regulatory-disclosure bg-grey capabities-new-updated overflow-hidden"
    style="background-image:url(images/Capabilities.png);">
    <div class="container-fluid left-space pe-0">
        <div class="row">
            <div class="col-md-12">
                <h3 class="heading-1 text-black">Criteria / Methodologies</h3>
            </div>
        </div>
        <div class="accordian-style-1 mt-5 row g-0">
            <div class="col-md-5 col-xl-3">
                <div class="accordian-title-box">
                    <span class="acc-title text-center"></span>
                    <span class="acc-count">1</span>
                </div>
            </div>
            <div class="col-md-12 col-xl-9">
                <div class="accordion">





                    <div class="accordion-item">
                        <h2 class="accordion-header">
                            <button id="btnId0" class="accordion-button" type="button"
                                jf-ext-button-ct="criteria/methodologies">
                                Criteria/Methodologies
                            </button>
                        </h2>
                        <div id="collapse0" class="accordion-collapse collapse show">
                            <div class="accordion-body">
                                <p>CARE Ratings (Africa) Private Limited (CRAF) undertakes a rating exercise based on
                                    information provided by the company, in-house databases and data from other sources
                                    that CRAF considers reliable. CRAF does not undertake unsolicited ratings. The
                                    primary focus of the rating exercise is to assess future cash generation capability
                                    of the company and its adequacy to meet debt obligations, even in adverse
                                    conditions. The analysis attempts to determine the long-term fundamentals and the
                                    probabilities of change in these fundamentals. The analytical framework of CRAF’s
                                    methodology is divided into two interdependent segments. The first deals with the
                                    operational characteristics and the second with the financial characteristics.
                                    Besides quantitative factors, qualitative aspects like assessment of management
                                    capabilities play a very important role in arriving at the rating for an instrument.
                                    The relative importance of qualitative and quantitative components of the analysis
                                    varies with the type of issuer. Rating determination is a matter of experienced and
                                    holistic judgment, based on the relevant quantitative and qualitative factors
                                    affecting the credit quality of the issuer. CRAF has developed various general and
                                    sector specific Rating Methodologies which are available on our website. CRAF
                                    reviews the Rating Criteria/Methodologies at least once in two financial years.</p>

                                <p><strong>Definition of Default</strong></p>

                                <p>Ratings are an opinion on the relative ability and willingness of an issuer to repay
                                    debt in a timely manner. CRAF considers one day one rupee missed payment as default,
                                    whenever CRAF is aware of such delay occurring in respect of any of CRAF rated debt
                                    instrument/facility/issuer. This definition is uniformly applied both for capital
                                    market instruments and bank facilities. Needless to say that while assigning rating
                                    to various types of bank facilities like Bill Discounting, Letter of Credit, Bank
                                    Guarantees, etc, CRAF takes into account nuances of such facilities while
                                    determining missed payment which is consistently applied across all the issuers. For
                                    further details on treatment of default on bank facilities and other debt
                                    instruments, please&nbsp;<a
                                        href="http://103.58.165.4/upload/pdf/Policy%20on%20Default%20Recognition_Dec%202022(2-12-22).pdf"
                                        target="blank">click here</a></p>

                                <p><strong>What Ratings Do Not Measure</strong></p>

                                <p>It is important to emphasize the limitations of credit ratings. They are not
                                    recommendations to renew, disburse or recall the concerned bank facilities or to
                                    buy, sell or hold any security. They do not take into account many aspects which
                                    influence an investment or lending decision. They do not, for example, evaluate the
                                    reasonableness of the issue price, possibilities for capital gains or take into
                                    account the liquidity in the secondary market. Ratings also do not take into account
                                    the risk of prepayment by issuer. Ratings neither take into account investors’ risk
                                    appetite nor the suitability of a particular instrument to a particular class of
                                    investors.</p>

                                <p><strong>Rating Outlook</strong></p>

                                <p>CRAF’s rating outlook is an opinion on the likely direction of movement of the rating
                                    in the medium term. A rating outlook shall be assigned to all credit rating
                                    assignments undertaken by CRAF. For further details, please&nbsp;<a
                                        href="http://103.58.165.4/upload/pdf/get-rated/Rating%20outlook%20and%20rating%20watch%20_Dec%202022(21-12-22).pdf"
                                        target="_blank">click here.</a></p>

                                <p>CARE Ratings’ Criteria/ Methodologies</p>

                                <p>General</p>

                                <p><a
                                        href="./upload/pdf/get-rated/Analytical treatment of restructuring - COVID-Revised (15-7-22).pdf">Analytical
                                        Treatment of Restructuring - COVID</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Policy on Assignment of Provisional Ratings_July2021 (21-6-22).pdf">Assignment
                                        of Provisional Rating</a></p>

                                <p><a href="./upload/pdf/get-rated/Consolidation and Combined appraoch - February 2023.pdf"
                                        target="_blank">Consolidation</a></p>

                                <p><a href="./upload/pdf/get-rated/Policy on Default Recognition_May 2023.pdf">Definition
                                        of Default</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Government Support - Feb 2023.pdf">Factoring
                                        Linkages Government Support</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Parent Sub JV Group - Feb 2023.pdf">Factoring
                                        Linkages Parent Sub JV Group</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology  Financial Ratios  Financial Sector December 2022(6-12-22).pdf">Financial
                                        Ratios - Financial Sector</a></p>

                                <p><a href="./upload/pdf/get-rated/Financial Ratios – Insurance Sector (18-8-22).pdf">Financial
                                        Ratios - Insurance Sector</a></p>

                                <p><a href="./pdf/resources/CARE%20Ratings’%20criteria%20on%20Financial%20Ratios%20-%20Non%20Financial%20Sector%20-%20March%202023.pdf"
                                        target="_blank">Financial Ratios – Non financial Sector</a></p>

                                <p><a href="./upload/pdf/get-rated/Investment Holding Companies - Feb 2023.pdf">Investment
                                        Holding Companies</a></p>

                                <p><a href="./upload/pdf/get-rated/CARE Ratings Criteria Issuer Rating.pdf">Issuer
                                        Rating</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Liquidity Analysis of Non-Financial Sector entities_May2022 Final.pdf">Liquidity
                                        Analysis of Non-financial sector entities</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/CARE Ratings’ criteria on information adequacy risk and issuer non-cooperation - March 2023.pdf">Policy
                                        in respect of non-cooperation by issuers </a></p>

                                <p><a href="./upload/pdf/get-rated/Policy on curing period-June 22 Final (30-6-22).pdf">Policy
                                        On Curing Period </a></p>

                                <p><a href="./upload/pdf/get-rated/Rating Credit Enhanced Debt-Dec 22(5-12-22).pdf">Rating
                                        Credit Enhanced Debt </a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating outlook and rating watch _Dec 2022(21-12-22).pdf">Rating
                                        Outlook and Rating Watch</a></p>

                                <p><a href="./upload/pdf/get-rated/Criteria for Short-Term Instruments -Feb 2023.pdf">Short
                                        Term Instruments</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/CARE Rating's Policy on Withdrawal of Ratings May 2023.pdf">Withdrawal
                                        Policy </a></p>

                                <p>Manufacturing/ Trading/Services</p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology- Auto Components &amp; Equipments (14-12-22).pdf">Auto
                                        Components &amp; Equipments</a></p>

                                <p><a href="./upload/pdf/get-rated/Rating Methodology- Auto Dealer (14-12-22).pdf">Auto
                                        Dealer</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Cement Industry (11-7-22).pdf">Cement</a>
                                </p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Commercial Vehicle Industry - December 2022(27-12-22).pdf">Commercial
                                        Vehicles</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology for Cotton Textile Manufacturing November 2022(30-11-22).pdf">Cotton
                                        Textile</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Cut &amp; Polished Diamonds (CPD) Industry - November 2022(25-11-22).pdf">Cut
                                        and Polished Diamonds</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology- Education_Sep2022 (14-12-22).pdf">Education</a>
                                </p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology_Fertilizers_May 2022 - Final (11-7-22).pdf">Fertilizer</a>
                                </p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology _Hospitals - September 2022..pdf">Hospital</a>
                                </p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology- Hotels &amp; Resorts (14-12-22).pdf">Hotels
                                        &amp; Resorts</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Manmade Yarn Manufacturing December 2022 Final(2-1-23).pdf">Manmade
                                        Yarn-Methodology</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Manufacturing Companies December 2022(14-12-22).pdf">Manufacturing
                                        Companies</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Mobile Service Providers - Feb 2023.pdf">Mobile
                                        Service Provider</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Non Ferrous Metals Aug-2022 (30-8-22).pdf">Non
                                        Ferrous Metal</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology  Paper &amp; Paper Products(25-1-23).pdf">Paper
                                        &amp; Paper Products</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology Agrochemicals December 2022(27-12-22).pdf">Pesticides
                                        &amp; Agrochemicals</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology- Pharmaceuticals (14-12-22).pdf ">Pharmaceuticals</a>
                                </p>

                                <p><a
                                        href="./upload/pdf/get-rated/CARE Ratings’ criteria – Project Stage Entities -  March 2023.pdf">Project
                                        stage companies</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Debt Backed by Lease Rentals Discounting March 2023.pdf">Rating
                                        methodology for Debt backed by lease rentals</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Real Estate Sector March 2023.pdf">Rating
                                        methodology for Real estate sector</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Organized Retail Companies December 2022(26-12-22).pdf">Retail</a>
                                </p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Service Sector Companies December 2022(14-12-22).pdf">Service
                                        Sector Companies</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology- Iron &amp; Steel Sept 2022 (14-12-22).pdf">Iron
                                        &amp; Steel</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Sugar Sector February 2023.pdf">Sugar</a>
                                </p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Wholesale Trading - Feb 2023.pdf">Wholesale
                                        Trading</a></p>

                                <p>Infrastructure</p>

                                <p><a href="./upload/pdf/get-rated/Rating Methodology - Airport Companies March 2023.pdf"
                                        target="_blank">Airports</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology – Road Assets Annuity December 2022(27-12-22).pdf">Road
                                        Assets-Annuity</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - City Gas Distribution Companies - Feb 2023.pdf">City
                                        Gas Distribution</a></p>

                                <p><a href="./upload/pdf/get-rated/Rating Methodology - Construction Sector March 2023.pdf"
                                        target="_blank">Construction</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology- Road Assets- Hybrid Annuity_October 2022 (14-12-22).pdf">Road
                                        Assets-Hybrid Annuity</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology  Infrastructure Investment Trusts (InvITs)(5-12-22).pdf">Infrastructure
                                        Investment Trusts (InvITs)</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Infrastructure Sector Ratings Feb 2023.pdf">Infrastructure
                                        Sector Ratings</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Port &amp; Port Services(25-1-23).pdf">Port
                                        &amp; Port services</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Power Distribution Companies - Feb 2023.pdf">Power
                                        Distribution</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Power Generation Projects_Sept2020.pdf">Power
                                        Generation Projects</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology- Power- Transmission December 2022 (14-12-22).pdf">Power-
                                        Transmission</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology  Real estate Investment Trusts (REITs) December 2022(01-12-22).pdf">Real
                                        Estate Investment Trusts (REITs)</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodoloy Shipping companies September 2022..pdf">Shipping</a>
                                </p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Solar Power Projects October 2022(10-10-22).pdf">Solar
                                        Power Projects</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology  Thermal Power Producers (16-8-22).pdf">Thermal
                                        Power</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology- Road Assets- Toll_October 2022 (14-12-22).pdf">Road
                                        Assets-Toll</a></p>

                                <p><a
                                        href="./upload/pdf/get-rated/Rating Methodology - Wind Power Projects October 2022(10-10-22).pdf">Wind
                                        Power Projects</a></p>

                                <p>BFSI</p>

                                <p><a href="./upload/pdf/get-rated/Rating Methodology - Banks March 2023.pdf"
                                        target="_blank">Bank</a></p>

                                <p><a href="./upload/pdf/get-rated/Criteria for Mutual Fund Ratings Capital Protection-Oriented Schemes - March 2023.pdf"
                                        target="_blank">Capital Protection Oriented Scheme</a></p>

                                <p><a href="./upload/pdf/get-rated/Rating Methodology - Housing Finance Companies_Jan23 Final(5-1-23).pdf"
                                        target="_blank">Housing Finance Companies</a></p>

                                <p><a href="./upload/pdf/get-rated/Rating Methodology - Insurance Sector (18-8-22).pdf"
                                        target="_blank">Insurance Sector</a></p>

                                <p><a href="./upload/pdf/get-rated/Rating Methodology Market Linked Notes - March 2023.pdf"
                                        target="_blank">Market Linked Notes</a></p>

                                <p><a href="./upload/NewsFiles/GetRated/Ratings%20Methodology%20-%20Fund%20Credit%20Quality%20February%202023.pdf"
                                        target="_blank">Mutual Fund Credit Quality</a></p>

                                <p><a href="./upload/pdf/get-rated/NBFC Criteria November-2022(24-11-22).pdf"
                                        target="_blank">Non Banking Financial Companies</a></p>

                                <p><a href="./upload/pdf/get-rated/Rating Criteria for Rating Basel III hybrid Instruments issued by Banks December 2022(5-12-22).pdf"
                                        target="_blank">Rating Basel III - Hybrid Capital Instruments issued by
                                        Banks</a></p>

                                <p><a href="./upload/NewsFiles/GetRated/Rating%20Methodology%20for%20ABS%20MBS%20-%20Feb%202023.pdf"
                                        target="_blank">Securitisation ABS and MBS</a></p>

                                <p><a href="./upload/NewsFiles/GetRated/Pools Backed by Wholesale Assets March 2023.pdf"
                                        target="_blank">Pools backed by Wholesale Assets</a></p>

                                <p>Public Finance</p>

                                <p><a href="./upload/pdf/get-rated/Rating Methodology - State Governments October 2022(10-10-22).pdf"
                                        target="_blank">State Governments</a></p>

                                <p><a href="./upload/pdf/get-rated/Rating Methodology - Urban Infrastructure October 2022(10-10-22).pdf"
                                        target="_blank">Urban Infrastructure Projects</a></p>

                                <p>Other Products</p>

                                <p><a href="./upload/pdf/get-rated/Independent Credit Evaluation of Residual debt_Sep 2022 Final(30-9-22).pdf"
                                        target="_blank">Independent Credit Evaluation of Residual Debt </a></p>

                                <p><a href="./upload/pdf/get-rated/Infrastructure EL Ratings Methodology 21-2-2021.pdf"
                                        target="_blank">Infrastructure Expected Loss Ratings</a></p>

                                <p><a href="./upload/pdf/get-rated/Rating Methodology - Recovery Ratings March 2023.pdf"
                                        target="_blank">Recovery Rating</a></p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>