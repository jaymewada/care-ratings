<?php include "components/header.php" ?>
<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlInsightsBreadcrumSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item">
                            <a href="index.html">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="javascript:void(0)">GET RATED</a>
                        </li>
                        <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">RATINGS</a></li> -->
                        <li class="breadcrumb-item active" aria-current="page">Rating Committee</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">Rating Committee</h1>
            </div>
        </div>
    </div>
</section>

<section class="care-edge-slider-new" id="renderHtmlRatingReportsSectiondata">
    <div class="container-fluid p-0">
        <div class="row justify-content-center m-0">
            <div class="col-md-12 p-0">
                <div class="d-flex justify-content-md-between flex-column flex-md-row">
                    <div>
                        <h3 class="heading-1 text-dark Title">Rating Committee</h3>
                        <p class="Text-para">CARE Ratings (Africa) Pvt. Ltd. is committed to the establishment of the
                            highest standards of professional quality and integrity. In line with this, CRAF has
                            constituted its Rating Committee with members comprising officials from CARE Ratings
                            Limited, India (CARE Ratings) and panel of experts from Mauritius  to ensure valuable inputs
                            about the economy, industry and corporate environment in Mauritius are available to the
                            Rating Committee. The Rating Committee members of CRAF are as under: </p>
                        <ul>
                            <li>Mr. Sachin Gupta – Executive Director and Chief Rating Officer of CARE Ratings Limited
                            </li>
                            <li>Mr. Milind Gadkari, Chief Compliance officer and Senior Director of CARE Ratings Limited
                            </li>
                            <li>Mr. Ranjan Sharma – Senior Director of CARE Ratings Limited</li>
                            <li>Ms.Rajashree Murkute– Senior Director of CARE Ratings Limited</li>

                            <li>Ms.Smita Rajpurkar– Director of CARE Ratings Limited</li>
                            <li>Mr. Mahendra Vikramdass Punchoo – Former Second Deputy Governor of Bank of Mauritius
                            </li>
                            <li>Ms. Aruna Radhakeesoon - Chairman of National Committee of Corporate Governance</li>
                            <li>Mr. Vipin Mahabirsingh-Managing Director in Central Depository & Settlement Co Ltd.</li>
                            <li>Dr. Soumendra K. Dash-Vice President, Risk Management, The African Private Capital
                                Association </li>
                            <li>Mr. Saurav Chatterjee – CEO, CRAF</li>

                        </ul>
                        <p>In case any of the panel of experts from Mauritius is associated with the rated entity,
                            leading to conflict of interest, the member will not participate in the rating process and
                            in the Rating Committee meeting.</p>
                    </div>

                </div>

            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>