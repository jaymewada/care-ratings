<?php include "components/header.php" ?>

<section class="home-slider-sec home-slider-sec-v1 position-relative">
    <div class="inner-banner  breadcrumb-career-sec">
        <img src="public/frontend-assets/images/career-bg-top.webp" class="img-fluid">
        <div class="banner-content">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="https://www.careratingsafrica.com">Home</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Careers</li>
                </ol>
            </nav>
            <div class="banner_captions">
                <h1 class="heading-1 text-white">Start your Career with CareEdge</h1>
                <h3 class="heading-2 text-white ">Search for the Company and the position you want to work</h3>
            </div>
            <form id="job-listing" action="" method="post" enctype="multipart/form-data" novalidate=""> <span
                    class="search-icon">
                    <div data-parsley-trigger="focusout" required=""
                        data-parsley-required-message="Enter Search Keyword"
                        class=" custom-dropdown filter-dropdown  dropdown-menu-outiline dropdown mb-5 careers-dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" jf-ext-button-ct="select">
                            <span id="type_selected" class="selected-value">Select</span>
                        </button>

                        <ul class="dropdown-menu" id="dtopdown_gp">
                            <li data-val="CareEdge Ratings"><a class="dropdown-item" href="javascript:void(0)"
                                    data-slide="CareEdgeRatings">CareEdge
                                    Ratings</a></li>
                            <li data-val="CareEdge Group"><a class="dropdown-item" href="javascript:void(0)"
                                    data-slide="CareEdgeGroup">CareEdge
                                    Group</a></li>
                            <li data-val="CareEdge Africa"><a class="dropdown-item" href="javascript:void(0)"
                                    data-slide="CareEdgeAfrica">CareEdge
                                    Africa</a></li>
                            <li data-val="CareEdge Nepal"><a class="dropdown-item" href="javascript:void(0)"
                                    data-slide="CareEdgeNepal">CareEdge
                                    Nepal</a></li>
                        </ul>
                    </div>
                    <input type="hidden" id="group_type" name="group_type" value="group_type">
                    <div class="filter-ratings d-xl-block">
                        <div class="career_searchBar">
                            <div class="form-career-page">
                                <input type="hidden" name="_token" value="48o4F2VKAWLAsDh91E2wEqw4IE5EqzJ5W7xzFTXK">
                                <span class="search-icon">
                                    <svg id="Icons_Actions_ic-actions-search"
                                        data-name="Icons / Actions / ic-actions-search"
                                        xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                        <rect id="Rectangle_157" data-name="Rectangle 157" width="24" height="24"
                                            fill="none"></rect>
                                        <g id="ic-actions-search" transform="translate(3.62 3.55)">
                                            <circle id="Ellipse_12" data-name="Ellipse 12" cx="7" cy="7" r="7"
                                                transform="translate(0)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="1.5">
                                            </circle>
                                            <line id="Line_50" data-name="Line 50" x2="4.88" y2="4.88"
                                                transform="translate(11.88 12.02)" fill="none" stroke="#fff"
                                                stroke-linecap="round" stroke-linejoin="bevel" stroke-width="1.5">
                                            </line>
                                        </g>
                                    </svg>
                                </span>
                                <input type="text" data-parsley-trigger="focusout" required=""
                                    data-parsley-required-message="Enter Search Keyword" name="search_keyword"
                                    placeholder="Search Position by Location , Branch , Sub Function , Role"
                                    class="form-control search-input typeahead" jf-ext-cache-id="7">
                                <a href="job-listing" class="btn  view-opening">VIEW
                                    OPENINGS</a>
                            </div>

                        </div>
                    </div>
                </span></form>
        </div>
    </div>
</section>

<section class="reports-sec job-open-sec padding-100">
    <div class="container-fluid">
        <div class="row ">
            <div class="col-md-12 col-lg-8 offset-lg-4 mb-5	">
                <div class="d-flex justify-content-md-between align-items-md-start flex-column flex-md-row">
                    <div>
                        <h2 class="heading-1 text-dark">Recent Job Openings</h2>
                    </div>

                    <div
                        class="mt-xl-3 custom-dropdown filter-dropdown  dropdown-menu-outiline dropdown mb-5  mb-lg-0 ">
                        <button class="btn btn-secondary dropdown-toggle" type="button"
                            jf-ext-button-ct="latest to oldest">
                            <span class="selected-value">LATEST TO OLDEST</span>
                        </button>
                        <ul class="dropdown-menu" id="latest_jobs_dp">
                            <li data-val="LATEST TO OLDEST"><a class="dropdown-item" href="javascript:void(0)"
                                    data-slide="latest">LATEST TO OLDEST</a>
                            </li>
                            <li data-val="2 Weeks Ago"><a class="dropdown-item" href="javascript:void(0)"
                                    data-slide="2weeksago">2 Weeks Ago</a></li>
                            <li data-val="2 Months Ago"><a class="dropdown-item" href="javascript:void(0)"
                                    data-slide="2monthsago">2 Months Ago</a></li>
                        </ul>
                    </div>
                    <input type="hidden" id="group_type_latest" name="group_type_latest" value="">

                </div>
                <div class="d-flex justify-content-md-between align-items-md-start flex-column flex-md-row">
                    <div class="sub-head">Find your dream job with the best in Business</div>
                </div>
            </div>
        </div>
        <div class="row gx-5">
            <div class="col-md-12  col-lg-4">
                <div class="d-flex flex-column justify-content-between h-100">
                    <hr class="style-1">
                    <h3 class="heading-2 text-dark">
                        Find your dream <br>job with the<br>best in Business
                    </h3>
                    <div class="slider-arrows arrow-1 me-0 mt-3 mb-3  mt-lg-0 mb-lg-0 "> <span
                            class="prev-btn slick-arrow slick-hidden" aria-disabled="true" tabindex="-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-chevron-left" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z">
                                </path>
                            </svg>
                        </span> <span class="next-btn slick-arrow slick-hidden" aria-disabled="true" tabindex="-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-chevron-right" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z">
                                </path>
                            </svg>
                        </span> </div>
                </div>
            </div>
            <div class="col-md-12  col-lg-8">
                <input type="hidden" name="_token" id="token_latest_job_form"
                    value="48o4F2VKAWLAsDh91E2wEqw4IE5EqzJ5W7xzFTXK">


                <div class="row reports-slider-new sliderGap-30" id="latest_jobs">
                    <?php for ($x = 0; $x <= 10; $x++) { ?>
                    <div class="item">
                        <div class="card card-style-1">
                            <a href="#">
                                <div class="card-body2"><span class="text-grey text-small">4 months ago
                                    </span><br><span class="text-grey text-small">Mumbai</span> </div>
                            </a><a href="now" target="_blank" <span="" class="arrow-link"> <svg
                                    xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                    viewBox="0 0 15.28 15.256">
                                    <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                        <line id="Line_60" data-name="Line 60" y1="13.182"
                                            transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                            stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></line>
                                        <path id="Path_51" data-name="Path 51"
                                            d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                            transform="translate(-7.26 -3.929)" fill="none" stroke="#fff"
                                            stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></path>
                                    </g>
                                </svg>
                            </a>
                            <div class="card-body"> <span class="text-grey text-small">Test1</span>
                                <p class="heading-3 text-white mb-3 mt-3">Analyst</p> <span class="text-grey">4</span>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>

            <div class="col-md-12 offset-lg-4 col-lg-8 pl13">
                <a href="job-listing" class="btn btn-primary mt-5">VIEW ALL</a>
            </div>

        </div>
    </div>
</section>

<section class="padding-100 why-work-with-us-sec career-work why-wok-new">
    <div class="container-fluid">
        <div class="row justify-content-center">

            <div class="col-md-11">

                <div class="row align-items-center">
                    <div class="col-md-12">
                        <h3 class="heading-1 text-black text-center pb-5">Why Work With Us?</h3>
                        <!-- <hr class="style-1 mt-xl-5 mb-xl-5 opacity-05" /> -->
                        <div class="why-work-img-sec text-center">
                            <img src="https://www.careratingsafrica.com/public/frontend-assets/images/why-work-img.svg"
                                class="img-fluid w-40 text-center">
                        </div>
                    </div>
                </div>

                <div class="row gx-5">
                    <div class="col-md-12 col-lg-6">
                        <div class="activity-list pt-0">
                            <ul id="renderHtmlWhyWorkWithUsdata">


















                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12 col-lg-6">
                        <div class="activity-list pt-0">
                            <ul id="renderHtmlWhyWorkWithUsfffdata">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

<?php include "components/footer.php" ?>