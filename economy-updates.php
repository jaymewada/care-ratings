<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlInsightsBreadcrumSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">INSIGHTS</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Economy Updates</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">Economy Updates</h1>
            </div>
        </div>
    </div>
</section>

<section class="padding-100 updates-sec">
    <div class="container-fluid">
        <div class="row justify-content-center d-flex-header">
            <div class="col-xxl-6 col-xl-6 col-lg-6 col-sm-5 col-6">
                <h3 class="heading-1 text-black mb-5">Updates</h3>
            </div>
            <div class="col-xxl-5 col-xl-5 col-lg-5 col-sm-7 col-6 btn-col">

                <div class="row float-right me-5">

                    <div class="d-flex sasifb flex-row-reverse yearDropDown-main">
                        <div class="yearToggle" style="margin-left:10px;">

                            <select class="empInput form-control" name="month_Id" id="month_Id"
                                style="border: 1px solid #858796;" jf-ext-cache-id="10">
                                <option value="">Select Month</option>
                                <option value="1">January</option>
                                <option value="2">February</option>
                                <option value="3">March</option>
                                <option value="4" selected="selected">April</option>
                                <option value="5">May</option>
                                <option value="6">June</option>
                                <option value="7">July</option>
                                <option value="8">August</option>
                                <option value="9">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                        </div>
                        <div class="yearToggle" style="margin-left:5px;">

                            <select class="empInput form-control" name="Year_Id" id="Year_Id"
                                style="border: 1px solid #858796;" jf-ext-cache-id="11">
                                <option value="">Select Year</option>
                                <option value="2000">2000</option>
                                <option value="2015">2015</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024" selected="selected">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row g-5 mutli-img-boxes mt-2" id="renderHtmlInsightsEventsSectiondata">
                <div>No data Found.....</div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>