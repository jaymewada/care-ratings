</main>

<footer>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12 col-xxl-11">
                <div class="row gx-5 gy-3">
                    <div class="col-lg-6 col-xl-8 col-md-6">
                        <div class="quick-links">
                            <h4 class="footer-title"><span>CARE Ratings (Africa) Pvt. Ltd.</span></h4>
                        </div>
                    </div>

                    <div class="col-lg-6 col-xl-4 col-md-6">
                        <div class="footer-link-list">
                            <div class="footer-social-media">
                                <h4 class="footer-title">Follow Us On</h4>
                                <div class="social-media-links">
                                    <a href="https://www.linkedin.com/company/credit-analysis-&amp;-research-ltd--care-ratings-"
                                        target="_blank">

                                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50"
                                            viewBox="0 0 50 50">
                                            <g id="Linked_In" data-name="Linked In" transform="translate(-1386 -9808)">
                                                <g id="Ellipse_27" data-name="Ellipse 27"
                                                    transform="translate(1386 9808)" fill="none" stroke="#ffffff"
                                                    stroke-width="1.5">
                                                    <circle cx="25" cy="25" r="25" stroke="none" />
                                                    <circle cx="25" cy="25" r="24.25" fill="none" />
                                                </g>
                                                <g id="Group_11" data-name="Group 11" transform="translate(1404 9826)">
                                                    <g id="linkedin" transform="translate(0 0)">
                                                        <path id="Path_16" data-name="Path 16"
                                                            d="M18.955,18.662h0V12.72c0-2.907-.626-5.146-4.024-5.146A3.527,3.527,0,0,0,11.758,9.32h-.047V7.845H8.489V18.661h3.355V13.305c0-1.41.267-2.774,2.014-2.774,1.721,0,1.746,1.609,1.746,2.864v5.266Z"
                                                            transform="translate(-4.109 -3.812)" fill="#ffffff" />
                                                        <path id="Path_17" data-name="Path 17"
                                                            d="M.4,7.977H3.755V18.793H.4Z"
                                                            transform="translate(-0.129 -3.942)" fill="#ffffff" />
                                                        <path id="Path_18" data-name="Path 18"
                                                            d="M1.945,0A1.955,1.955,0,1,0,3.891,1.945,1.946,1.946,0,0,0,1.945,0Z"
                                                            fill="#ffffff" />
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>

                                    </a>
                                    <a href="https://twitter.com/CareEdge_Group" target="_blank">

                                        <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50"
                                            viewBox="0 0 50 50">
                                            <g id="Twitter" transform="translate(-1446 -9808)">
                                                <g id="Ellipse_28" data-name="Ellipse 28"
                                                    transform="translate(1446 9808)" fill="none" stroke="#ffffff"
                                                    stroke-width="1.5">
                                                    <circle cx="25" cy="25" r="25" stroke="none" />
                                                    <circle cx="25" cy="25" r="24.25" fill="none" />
                                                </g>
                                                <g id="Group_10" data-name="Group 10"
                                                    transform="translate(1463.086 9826.762)">
                                                    <path id="twitter-2" data-name="twitter"
                                                        d="M14.85,49.428a6.347,6.347,0,0,1-1.754.481,3.027,3.027,0,0,0,1.339-1.683,6.084,6.084,0,0,1-1.931.737,3.044,3.044,0,0,0-5.266,2.082,3.135,3.135,0,0,0,.071.694,8.617,8.617,0,0,1-6.275-3.184,3.045,3.045,0,0,0,.936,4.069,3.006,3.006,0,0,1-1.375-.375v.033a3.058,3.058,0,0,0,2.439,2.991,3.039,3.039,0,0,1-.8.1,2.692,2.692,0,0,1-.576-.052A3.073,3.073,0,0,0,4.5,57.443a6.117,6.117,0,0,1-3.775,1.3A5.706,5.706,0,0,1,0,58.7a8.571,8.571,0,0,0,4.67,1.366A8.605,8.605,0,0,0,13.335,51.4c0-.135,0-.264-.011-.394A6.073,6.073,0,0,0,14.85,49.428Z"
                                                        transform="translate(0 -48)" fill="#ffffff" />
                                                </g>
                                            </g>
                                        </svg>

                                    </a>
                                    <a href="https://www.instagram.com/careedge_group/" target="_blank">

                                        <svg xmlns="http://www.w3.org/2000/svg"
                                            xmlns:xlink="http://www.w3.org/1999/xlink" width="50" height="50"
                                            viewBox="0 0 50 50">
                                            <defs>
                                                <clipPath id="clip-path">
                                                    <rect id="Rectangle_4171" data-name="Rectangle 4171" width="14.998"
                                                        height="15" fill="#ffffff" />
                                                </clipPath>
                                            </defs>
                                            <g id="Facebook" transform="translate(-1506 -9808)">
                                                <g id="Ellipse_29" data-name="Ellipse 29"
                                                    transform="translate(1506 9808)" fill="none" stroke="#ffffff"
                                                    stroke-width="1.5">
                                                    <circle cx="25" cy="25" r="25" stroke="none" />
                                                    <circle cx="25" cy="25" r="24.25" fill="none" />
                                                </g>
                                                <g id="Group_27058" data-name="Group 27058"
                                                    transform="translate(1523.491 9826)">
                                                    <g id="Group_27058-2" data-name="Group 27058"
                                                        transform="translate(0 0)" clip-path="url(#clip-path)">
                                                        <path id="Path_12761" data-name="Path 12761"
                                                            d="M15,3.457v8.084a8.178,8.178,0,0,1-.182.84A3.74,3.74,0,0,1,11.225,15q-3.726.011-7.453,0a3.624,3.624,0,0,1-2.192-.7A3.706,3.706,0,0,1,0,11.154Q0,7.522,0,3.891a4.336,4.336,0,0,1,.056-.743A3.645,3.645,0,0,1,2.4.267,7.59,7.59,0,0,1,3.458,0h8.084c.105.018.21.036.316.056a3.669,3.669,0,0,1,2.866,2.316A7.286,7.286,0,0,1,15,3.457M10.625,7.5a3.125,3.125,0,1,0-3.117,3.126A3.134,3.134,0,0,0,10.625,7.5m.937-3.127a.935.935,0,1,0-.933-.941.938.938,0,0,0,.933.941"
                                                            transform="translate(0 0)" fill="#ffffff" />
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>

                                    </a>

                                    <a href="https://www.youtube.com/user/CARERatingsOnline" target="_blank">

                                        <svg xmlns="http://www.w3.org/2000/svg"
                                            xmlns:xlink="http://www.w3.org/1999/xlink" width="50" height="50"
                                            viewBox="0 0 50 50">
                                            <defs>
                                                <clipPath id="clip-path2">
                                                    <rect id="Rectangle_4172" data-name="Rectangle 4172" width="19.971"
                                                        height="14.014" fill="#ffffff" />
                                                </clipPath>
                                            </defs>
                                            <g id="Facebook" transform="translate(-1506 -9808)">
                                                <g id="Ellipse_29" data-name="Ellipse 29"
                                                    transform="translate(1506 9808)" fill="none" stroke="#ffffff"
                                                    stroke-width="1.5">
                                                    <circle cx="25" cy="25" r="25" stroke="none" />
                                                    <circle cx="25" cy="25" r="24.25" fill="none" />
                                                </g>
                                                <g id="Group_27059" data-name="Group 27059"
                                                    transform="translate(1521.014 9826.493)">
                                                    <g id="Group_27059-2" data-name="Group 27059"
                                                        transform="translate(0 0)" clip-path="url(#clip-path2)">
                                                        <path id="Path_12762" data-name="Path 12762"
                                                            d="M9.935,0c2.05.081,4.1.153,6.151.247A10.709,10.709,0,0,1,17.53.418a2.526,2.526,0,0,1,2.118,2.211,23.894,23.894,0,0,1,.3,5.054c-.039,1.183-.153,2.363-.268,3.542a2.65,2.65,0,0,1-1.2,2.089,3.357,3.357,0,0,1-1.522.442,56.35,56.35,0,0,1-6.211.258C8.67,14,6.584,13.94,4.5,13.872a17.589,17.589,0,0,1-2-.2A2.507,2.507,0,0,1,.34,11.543,21.135,21.135,0,0,1,.015,7.987a30.15,30.15,0,0,1,.2-4.69A8.5,8.5,0,0,1,.41,2.256,2.55,2.55,0,0,1,2.664.364a54.465,54.465,0,0,1,5.876-.3c.464-.008.928,0,1.393,0,0-.021,0-.042,0-.064M8.007,4.052v5.972l5.186-2.986L8.007,4.052"
                                                            transform="translate(0 0)" fill="#ffffff" />
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>

                                    </a>
                                </div>
                            </div>
                            <!-- end -->
                            <div class="news-letter-subscribe">
                                <h4 class="footer-title"><span>Subscribe To Our Newsletter</span></h4>
                                <form name="newsletter_form" id="newsletter_form"
                                    action="https://www.careratingsafrica.com/newsletter" method="post">
                                    <div class="row">
                                        <div class="alert alert-success print-footer-success-msg" style="display:none">
                                            <ul></ul>
                                        </div>
                                        <div class="alert alert-danger print-footer-error-msg" style="display:none">
                                            <ul></ul>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class=" mb-4">
                                            <input type="email" name="email" id="email" class="form-control"
                                                placeholder="Enter Your Email ID" required
                                                data-parsley-trigger="focusout"
                                                data-parsley-required-message="Enter Email">
                                        </div>
                                    </div>
                                    <input type="hidden" name="_token" id="token_newsletter_form"
                                        value="vZWAu33NBWwhpop4yuJApHVSQqcyBlZ6H9vghL4i">
                                    <div class="form-group">
                                        <button type="button" id="newsletter_btn"
                                            class="btn btn-primary btn-default">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="copyright-footer container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-3">
                    <div class="col-xl-4">
                        <p>&copy; 2024, CARE Edge ESG Pvt. Ltd. All Rights Reserved.</p>
                    </div>
                    <div class="col-xl-5 text-xl-center">
                        <div class="extra-link">
                            <a href="sitemap.php">SiteMap</a>
                            <a href="regulatory-disclosure.php">Regulatory Disclosures</a>
                            <a href="disclaimer.php">Disclaimer</a>
                            <a href="privacy-policy.php">Privacy Policy</a>
                        </div>
                    </div>
                    <div class="col-xl-3 text-xl-end mt-xl-0 mt-3">
                        <p> Designed & Developed By <a href="https://digitalpatterns.co.in/">EvolutionCo</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>


<span class="scrolltoTop ">
    <svg xmlns="http://www.w3.org/2000/svg" width="44" height="44" viewBox="0 0 44 44">
        <g id="Group_209" data-name="Group 209" transform="translate(-1728 -1881)">
            <circle id="Ellipse_11" data-name="Ellipse 11" cx="22" cy="22" r="22" transform="translate(1728 1881)"
                fill="#00d3c3"></circle>
            <g id="Group_135" data-name="Group 135" transform="translate(1213 1811)">
                <rect id="Rectangle_64" data-name="Rectangle 64" width="24" height="24" transform="translate(525 80)"
                    fill="#fff" opacity="0"></rect>
                <path id="Union_11" data-name="Union 11"
                    d="M.293,13.021a1,1,0,0,1,0-1.415l4.95-4.95L.293,1.707A1,1,0,0,1,1.707.293L7.364,5.95a1,1,0,0,1,0,1.414L1.707,13.021a1,1,0,0,1-1.415,0Z"
                    transform="translate(530.515 95.828) rotate(-90)" fill="#fff"></path>
            </g>
        </g>
    </svg>
</span>


<script src="public/frontend-assets/js/all.js"></script>
<script src="public/frontend-assets/js/gsap.min.js"></script>
<script src="public/frontend-assets/js/swiper.js"></script>
<script src="public/frontend-assets/js/yt-player.js"></script>
<script src="public/frontend-assets/js/slick.js"></script>
<script src="public/frontend-assets/js/ScrollTrigger.min.js"></script>
<script src="public/frontend-assets/js/smoothscroll.min.js"></script>
<script src="public/frontend-assets/js/menu.js"></script>
<script src="public/frontend-assets/js/audio.js"></script>
<script src="public/frontend-assets/js/typeahead.js"></script>
<script src="public/frontend-assets/js/fancybox.umd.js"></script>

<script src="public/frontend-assets/js/parsley.min.js"></script>
<script src="public/frontend-assets/js/custom.js"></script>
<script src="public/app-assets/assets/js/moment.min.js"></script>
<script src="public/app-assets/assets/js/bootstrap-datepicker.min.js"></script>


<script>

</script>

</body>

</html>