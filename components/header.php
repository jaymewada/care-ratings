<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <title>CareEdge | Ratings</title>
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <link rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.13.2/themes/base/jquery-ui.min.css"
            integrity="sha512-ELV+xyi8IhEApPS/pSj66+Jiw+sOT1Mqkzlh8ExXihe4zfqbWkxPRi8wptXIO9g73FSlhmquFlUOuMSoXz5IRw=="
            crossorigin="anonymous" referrerpolicy="no-referrer" />
        <link href="public/frontend-assets/css/all.css" rel="stylesheet" />
        <link href="public/frontend-assets/css/swiper-bundle.min.css" rel="stylesheet" />
        <link href="public/frontend-assets/css/style.css" rel="stylesheet" />
        <link href="public/frontend-assets/css/new-custom-backend.css" rel="stylesheet" />
        <link href="public/frontend-assets/css/responsive.css" rel="stylesheet" />

        <link href="public/frontend-assets/css/mapbox-gl.css" rel="stylesheet" />
        <link href="public/frontend-assets/css/mapbox-gl-geocoder.css" rel="stylesheet" />

        <link href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@6.1.1/css/fontawesome.min.css"
            rel="stylesheet">
        <link rel='stylesheet'
            href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
        <!-- Docs styles -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/CDNSFree2/Plyr/plyr.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css" />
        <link rel="stylesheet" href="public/app-assets/assets/css/bootstrap-datepicker.min.css">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>
    </head>

    <body>
        <!-- Desktop Header -->
        <header class="desktop_header">
            <div class="top_header container-fluid desktop-top_header">
                <ul class="top-list left-nav">
                    <li><a href="https://www.careedge.in/" target="_blank"> CareEdge Group</a></li>
                    <li>
                        <a href="https://www.careratings.com/" target="_blank">
                            CareEdge Ratings
                        </a>
                    </li>
                    <li>
                        <a href="http://careedgeadvisory.com/" target="_blank">
                            CareEdge Advisory
                        </a>
                    </li>
                    <li>
                        <a href="https://caapl.in/" target="_blank">
                            CareEdge Analytics
                        </a>
                    </li>
                    <li>
                        <a href="https://www.careratingsnepal.com/" target="_blank">
                            CareEdge Nepal
                        </a>
                    </li>
                    <li class="active">
                        <a href="/" target="_blank">
                            CareEdge Africa
                        </a>
                    </li>
                </ul>
                <ul class="list-inline m-0 right-nav">
                    <li class="list-inline-item">
                        <select name="country" id="country" class="">
                            <option value="1">Mauritius</option>
                        </select>
                    </li>
                    <li class="list-inline-item">
                        <a href="careers">Careers</a>
                    </li>
                </ul>
            </div>

            <nav class="navbar navbar-expand-lg navbar-light desktop-menu">
                <div class="container-fluid h-100 pe-0">

                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation"
                        onclick="this.classList.toggle('opened');this.setAttribute('aria-expanded', this.classList.contains('opened'))">
                        <svg width="100" height="100" viewBox="0 0 100 100">
                            <path class="line line1"
                                d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058">
                            </path>
                            <path class="line line2" d="M 20,50 H 80"></path>
                            <path class="line line3"
                                d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942">
                            </path>
                        </svg>
                    </button>

                    <a class="navbar-brand" href="/">
                        <img src="public/frontend-assets/images/CareEdge_ESG.png" alt="CareEdge_ratings_logo"
                            width="170" height="58" />
                    </a>
                    <button type="button" class="loginCta">
                        <span class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-person" viewBox="0 0 16 16">
                                <path
                                    d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z" />
                            </svg>
                        </span>
                    </button>

                    <!--DESKTOP MENU-->
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav ms-auto mb-2 mb-lg-0">

                            <li class="nav-item dropdown">
                            <li><a class="nav-link" href="find-ratings">Find Ratings</a></li>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    Get Rated
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="rating_process">Rating Process</a></li>
                                    <li><a class="dropdown-item" href="criteria_methodologies">Criteria /
                                            Methodologies</a></li>
                                    <li><a class="dropdown-item" href="rating_faqs">Rating FAQs</a></li>
                                    <li><a class="dropdown-item" href="fee_structure">Fee Structure Africa</a></li>
                                    <li><a class="dropdown-item" href="rating-symbols-and-definition">Rating
                                            Symbols & Definition</a></li>

                                    <li><a class="dropdown-item" href="regulatory-guidelines">Regulatory
                                            Guidelines</a></li>
                                    <li><a class="dropdown-item" href="rating-committee">Rating Committee</a></li>


                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    CAPABILITIES
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="corporate-sector">CORPORATE SECTOR</a></li>
                                    <li><a class="dropdown-item" href="financial-sector">FINANCIAL SECTOR</a></li>
                                    <li>
                                        <a class="dropdown-item" href="structured-finance">STRUCTURED FINANCE</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="infrastructure-sector">INFRASTRUCTURE
                                            SECTOR</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="sustainability-solutions">SUSTAINABILITY
                                            SOLUTIONS</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="sme-solutions">SME SOLUTIONS</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    INSIGHTS
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light dropdown-display dropdown-newDegn"
                                    aria-labelledby="navbarDropdown">
                                    <li>
                                        <h5>economy</h5>
                                        <ul class="dropDown-inner">
                                            <li>
                                                <a class="dropdown-item"
                                                    href="https://www.careratings.com/economy-updates">
                                                    Economy Updates
                                                </a>
                                            </li>

                                            <li><a class="dropdown-item" href="debt-market-update">Debt Market
                                                    Update</a></li>
                                            <li><a class="dropdown-item" href="bond-market-update">Bond Market
                                                    Update</a></li>
                                        </ul>
                                    </li>
                                    <li>
                                        <h5>industry research</h5>
                                        <ul class="dropDown-inner">
                                            <li>
                                                <a class="dropdown-item"
                                                    href="https://www.careratings.com/industry-research">Overview</a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item dropdown hide-in-s-lap show-in-s-lap">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    MEDIA & EVENTS
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="events-and-webinars">Events and Webinars</a>
                                    </li>
                                    <li><a class="dropdown-item" href="media-coverage">Media Coverage</a></li>
                                    <li><a class="dropdown-item" href="publications">Publications</a></li>
                                </ul>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    Investors
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light" aria-labelledby="navbarDropdown">

                                    <li>
                                        <a class="dropdown-item" href="shareholding-patterns">Shareholding Pattern</a>
                                    </li>
                                    <li><a class="dropdown-item" href="annual-reports">Annual Reports</a></li>
                                    <li><a class="dropdown-item" href="corporate-governance">Corporate Governance</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item hide-in-s-lap show-in-s-lap">
                                <a class="nav-link" href="about-us">About us</a>
                            </li>
                            <li class="nav-item hide-in-s-lap show-in-s-lap">
                                <a class="nav-link" href="contact-us">Contact Us</a>
                            </li>
                        </ul>
                        <div class="secondary-menu">
                            <button class="menu-icon"
                                onclick="this.classList.toggle('opened');this.setAttribute('aria-expanded', this.classList.contains('opened'))"
                                aria-label="Main Menu">
                                <svg width="100" height="100" viewBox="0 0 100 100">
                                    <path class="line line1"
                                        d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058" />
                                    <path class="line line2" d="M 20,50 H 80" />
                                    <path class="line line3"
                                        d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942" />
                                </svg>
                            </button>
                            <ul>
                                <li>
                                    <a href="javascript:void(0)" class="dropdown-toggle">
                                        MEDIA & EVENTS</a>
                                    <ul class="secondary-menu-list ">
                                        <li><a href="events-and-webinars">Events and Webinars</a></li>
                                        <li><a href="media-coverage">Media Coverage</a></li>
                                        <li><a class="dropdown-item" href="publications">Publications</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="about-us">
                                        ABOUT US
                                    </a>
                                </li>
                                <li>
                                    <a href="contact-us">
                                        CONTACT US
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class=" d-none d-xl-block ">
                            <ul class="search-call-icon">
                                <li>
                                    <div class="searchbar1">
                                        <input type="text" placeholder="Find Ratings / Company / Research"
                                            id="CompanyInput" oninput="myFunc(this.value)" value=""
                                            class="searchBarInput">
                                        <div class="search"></div>
                                    </div>
                                </li>
                                <!-- <li>
                                    <a href="shoppingcart">
                                        <img src="public/frontend-assets/images/Cart_Icon.svg">
                                        <span class='badge badge-warning shopping-cart-count'>0</span>
                                    </a>
                                </li> -->
                                <li></li>
                            </ul>
                        </div>
                        <a class="btn btn-primary login-cta" data-bs-toggle="modal" href="#exampleModalToggle"
                            role="button">
                            Login/Register
                        </a>
                    </div>
                </div>
            </nav>
        </header>

        <header class="mobile_header">
            <div class="top_header container-fluid font-medium mobile-top_header">
                <form>
                    <select name="Others Sites" onchange="location = this.value;">
                        <option value="https://www.careedge.in/"> CareEdge Group </option>
                        <option value="https://www.careratingsindia.com/">CareEdge Ratings</option>
                        <option value="http://careedgeadvisory.com/">CareEdge Advisory & Research</option>
                        <option value="https://www.carerisksolutions.com/">CareEdge Risk Solutions</option>
                        <option value="https://careratingsnepal.com/">CareEdge Nepal</option>
                        <option value="http://www.careratingsafrica.com/" selected>CareEdge Africa</option>
                    </select>
                </form>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light mobile-menu">
                <div class="container-fluid h-100 pe-0">
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation"
                        onclick="this.classList.toggle('opened');this.setAttribute('aria-expanded', this.classList.contains('opened'))">
                        <svg width="100" height="100" viewBox="0 0 100 100">
                            <path class="line line1"
                                d="M 20,29.000046 H 80.000231 C 80.000231,29.000046 94.498839,28.817352 94.532987,66.711331 94.543142,77.980673 90.966081,81.670246 85.259173,81.668997 79.552261,81.667751 75.000211,74.999942 75.000211,74.999942 L 25.000021,25.000058">
                            </path>
                            <path class="line line2" d="M 20,50 H 80"></path>
                            <path class="line line3"
                                d="M 20,70.999954 H 80.000231 C 80.000231,70.999954 94.498839,71.182648 94.532987,33.288669 94.543142,22.019327 90.966081,18.329754 85.259173,18.331003 79.552261,18.332249 75.000211,25.000058 75.000211,25.000058 L 25.000021,74.999942">
                            </path>
                        </svg>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img src="public/frontend-assets/images/logo_careedge_ratings_africa.png"
                            alt="CareEdge_ratings_logo" />
                    </a>
                    <div class=" d-xl-block call-login-icon">
                        <ul class="search-call-icon">
                            <li>
                                <div class="searchbar1">
                                    <input type="text" placeholder="Find Ratings / Company / Research"
                                        id="CompanyInputmobile" oninput="myFunc(this.value)" value=""
                                        class="searchBarInput">
                                    <div class="search"></div>
                                </div>

                            </li>
                        </ul>
                    </div>
                    <button class="loginCta" data-bs-toggle="modal" href="#exampleModalToggle" role="button">
                        <span class="">
                            <img src="public/frontend-assets/images/icons/noun-login.svg">
                        </span>
                    </button>


                    <!--MOBILE MENU-->
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="nav navbar-nav dropdown ms-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link" href="find-ratings">Find Ratings</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    Get Rated
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light mb-3" aria-labelledby="navbarDropdown">
                                    <li>
                                        <a class="dropdown-item" href="rating_process">Rating Process</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="criteria_methodologies">Criteria /
                                            Methodologies</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="rating_faqs">Rating FAQs</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="fee_structure">Fee Structure</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="rating-symbols-and-definition">Rating
                                            Symbols & Definition</a>
                                    </li>

                                    <li>
                                        <a class="dropdown-item" href="regulatory-guidelines">Regulatory
                                            Guidelines</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    CAPABILITIES
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light mb-3" aria-labelledby="navbarDropdown">
                                    <li>
                                        <a class="dropdown-item" href="corporate-sector">CORPORATE SECTOR</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="financial-sector">FINANCIAL SECTOR</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="structured-finance">STRUCTURED FINANCE</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="infrastructure-sector">INFRASTRUCTURE
                                            SECTOR</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="sustainability-solutions">SUSTAINABILITY
                                            SOLUTIONS</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="sme-solutions">SME SOLUTIONS</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    INSIGHTS
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light mb-3" aria-labelledby="navbarDropdown">
                                    <li>
                                        <h5>Economy</h5>
                                    <li>
                                        <a class="dropdown-item" href="https://www.careratings.com/economy-updates">
                                            Economy Updates
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="debt-market-update">
                                            Debt Market Update
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="bond-market-update">
                                            Bond Market Update
                                        </a>
                                    </li>
                                    <li>
                                        <h5 class="p-0 mt-3">Industry research</h5>
                                        <a class="dropdown-item" href="industry-research">Overview</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    MEDIA & EVENTS
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light mb-3" aria-labelledby="navbarDropdown">
                                    <li>
                                        <a class="dropdown-item" href="events-and-webinars">Events and Webinars</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="media-coverage">Media Coverage</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="publications">Publications</a>
                                    </li>
                                </ul>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link dropdown-toggle" href="javascript:void(0)" role="button"
                                    data-bs-toggle="dropdown" aria-expanded="false">
                                    INVESTORS
                                </a>
                                <ul class="dropdown-menu dropdown-menu-light mb-3" aria-labelledby="navbarDropdown">

                                    <li>
                                        <a class="dropdown-item" href="shareholding-patterns">Shareholding
                                            Pattern</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="annual-reports">Annual Reports</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="corporate-governance">Corporate Governance</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="about-us">About US</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link" href="contactus">Contact US</a>
                            </li>
                        </ul>
                    </div>
                    <!--MOBILE MENU ENDS-->

                </div>
            </nav>
        </header>

        <!-- Modal popup for desktop-->
        <div class="modal fade login-popup" id="exampleModalToggle" aria-hidden="true"
            aria-labelledby="exampleModalToggleLabel" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalToggleLabel">Subscribe for the Rating Rational</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">X</button>
                    </div>
                    <div class="modal-body">
                        <div class="login-regi-form">
                            <form class="row form-style-1" name="get_otp_login_form" id="get_otp_login_form">
                                <div class="row">
                                    <div class="alert alert-success print-success-msgg" style="display:none">
                                        <ul></ul>
                                    </div>
                                    <div class="alert alert-danger print-error-msgg" style="display:none">
                                        <ul></ul>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-floating">
                                        <input type="email" name="EMAILADDRESS" id="EMAILADDRESS" class="form-control"
                                            placeholder="name@example.com" value="" required
                                            data-parsley-trigger="focusout" data-parsley-required-message="Enter Email">
                                        <label for="floatingInput">Email ID*</label>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-floating">
                                        <input id="password-field" type="password" class="form-control" name="PASSWORD"
                                            placeholder="password" required data-parsley-trigger="focusout"
                                            data-parsley-required-message="Enter Your Password">
                                        <span toggle="#password-field"
                                            class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                        <label for="floatingInput">Password*</label>

                                    </div>
                                </div>
                                <input type="hidden" name="_token" id="token_get_otp"
                                    value="vZWAu33NBWwhpop4yuJApHVSQqcyBlZ6H9vghL4i">
                                <button type="button" class="btn btn-primary mt-3 view-report-btn font-semi-bold"
                                    id="get_otp_login_btn">Login with OTP</button>
                                <div class="content d-flex justify-content-between p-0">
                                    <a href="forgot_password" class="btn-link register-text">Forgot Password</a>
                                    <a href="register" class="btn-link ms-3 register-text">Register</a>
                                </div>
                                <img id="loader" src="public/frontend-assets/images/loader.gif">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade login-otp" id="exampleModalToggle2" aria-hidden="true"
            aria-labelledby="exampleModalToggleLabel2" tabindex="-1">
            <div class="modal-dialog modal-dialog-centered">
                <div class="container height-100 d-flex justify-content-center align-items-center">
                    <form name="otp_validate_form" data-parsley-validate="" id="otp_validate_form" class="digit-group"
                        data-autosubmit="false" autocomplete="off">
                        <div class="position-relative">
                            <div class="card p-2 text-center">
                                <div class="row">
                                    <div class="alert alert-success print-success-msgg" style="display:none">
                                        <ul></ul>
                                    </div>
                                    <div class="alert alert-danger print-error-msgg" style="display:none">
                                        <ul></ul>
                                    </div>
                                </div>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close">X</button>
                                <h6>Please enter the one time password <br> to verify your account</h6>
                                <div>
                                    <span>A code has been sent to</span>
                                    <small id="code_sent_user_email">*******9897</small>
                                </div>
                                <div class="digit-group">
                                    <input class="otp" onkeyup="tabChange(1)" type="text" id="digit-1" name="OTP[]" />
                                    <input class="otp" onkeyup="tabChange(2)" type="text" id="digit-2" name="OTP[]" />
                                    <input class="otp" onkeyup="tabChange(3)" type="text" id="digit-3" name="OTP[]" />
                                    <input class="otp" onkeyup="tabChange(4)" type="text" id="digit-4" name="OTP[]" />
                                </div>
                                <span class="err error-sign" id="otp_error"></span>

                                <div class="mt-4 mb-4">
                                    <button type="button" id="otp_validate_btn"
                                        class="btn btn-primary px-4 validate view-report-btn font-semi-bold">Validate</button>
                                </div>
                                <input type="hidden" name="EMAILADDRESS" id="email_address_validate_otp" value="">
                                <input type="hidden" name="PHONENUMBER" id="mobile_number_validate_otp" value="">
                                <input type="hidden" name="_token" id="token_validate_otp"
                                    value="vZWAu33NBWwhpop4yuJApHVSQqcyBlZ6H9vghL4i">
                                <div class="content d-flex justify-content-center align-items-center">
                                    <span>Didn't get the code?</span>
                                    <a id="resend_otp" class="btn-link ms-3 resend-text">Resend</a>
                                </div>
                                <img id="loader_resend" src="public/frontend-assets/images/loader.gif">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div id="res" class="results hideResults results_php">
            <div class="callout" data-closable>
                <button class="close-button" aria-label="Close alert" type="button" data-close>
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="row" id="CompanyRowInput">

            </div>
        </div>

        <section class="cookies" id="cookieNotice" style="display:none;">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-lg-11 col-md-12">
                        <div class="row gx-5">
                            <div class="col-md-12">
                                <div class="cookie-banner">
                                    <p><img src="public/frontend-assets/images/cookies.svg"
                                            class="img-fluid cookie-img">We use cookies to improve your journey and to
                                        personalize your web experience. By continuing to use this site, you are
                                        accepting the our <a href="javascript:void(0)">cookie policy.</a></p>
                                    <button class="cookie-close" onclick="acceptCookieConsent();">Accept</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <main class="get-rated-page">