<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlInsightsBreadcrumSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">INSIGHTS</a></li>
                        <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">RATINGS</a></li> -->
                        <li class="breadcrumb-item active" aria-current="page">Bond Market Update</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">Bond Market Update</h1>
            </div>
        </div>
    </div>
</section>

<section class="care-edge-slider-new">
    <div class="container-fluid p-0">
        <div class="row justify-content-center m-0">
            <div class="col-md-12 p-0">
                <div class="d-flex justify-content-md-between flex-column flex-md-row">
                    <h3 class="heading-1 text-dark Title mb-5">Mauritius sovereign bond market</h3>
                    <div class="slider-arrows me-2">
                        <span class="prev-btn swiper-button-lock swiper-button-disabled" tabindex="-1" role="button"
                            aria-label="Previous slide" aria-controls="renderHTMLChildSectionData" aria-disabled="true">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-chevron-left" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z">
                                </path>
                            </svg>
                        </span>
                        <span class="next-btn swiper-button-lock swiper-button-disabled" tabindex="-1" role="button"
                            aria-label="Next slide" aria-controls="renderHTMLChildSectionData" aria-disabled="true">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-chevron-right" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z">
                                </path>
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="swiper swiper-publication mt-3">
                    <div class="swiper-wrapper">
                        <?php for ($x = 0; $x <= 10; $x++) { ?>
                        <div class="swiper-slide">
                            <div class="card card-style-1">
                                <div class="card-body align-items-center">
                                    <p class="heading-3 text-white mb-1">
                                        Bank of Mauritius ECAI Guidelines
                                    </p>
                                    <a href="https://www.careratingsafrica.com/admin/pdf/Bank of Mauritius ECAI Guidelines_1699009998.pdf"
                                        target="_blank"
                                        class="btn btn-link primary p-0 d-block text-start mt-4">DOWNLOAD
                                        NOW</a>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "components/footer.php" ?>