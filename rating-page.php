<?php include 'components/header.php' ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item">
                            <a href="index-2.html">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="index-2.html">FIND RATINGS </a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="index-2.html">LATEST RATING </a>
                        </li>

                    </ol>
                </nav>

            </div>
        </div>
    </div>
</section>

<section class="register-form-sec">
    <div class="container-fluid">
        <div class="latest-area">
            <div class="latest-Inn">
                <form action="https://www.careratingsafrica.com/rating-page" method="get">
                    <select name="industry">
                        <option value="">Select Sector</option>
                        <option value="Banks">Banks</option>
                        <option value="Beverages">Beverages</option>
                        <option value="Ceramics &amp; Sanitaryware">Ceramics &amp; Sanitaryware</option>
                        <option value="Holding Company">Holding Company</option>
                        <option value="Hospitality">Hospitality</option>
                        <option value="Hotels &amp; Hospitality">Hotels &amp; Hospitality</option>
                        <option value="NBFI">NBFI</option>
                        <option value="Not Define">Not Define</option>
                        <option value="Real Estate">Real Estate</option>
                        <option value="Retail">Retail</option>
                        <option value="Sugar">Sugar</option>
                    </select>

                    <select name="ratings">
                        <option value="">Select Rating</option>
                        <option value="CARE MAU A (SO); Stable">CARE MAU A (SO); Stable</option>
                        <option value="CARE MAU A (SO); Stable / CARE MAU A1 (SO)">CARE MAU A (SO); Stable / CARE
                            MAU A1 (SO)</option>
                        <option value="CARE MAU A (SO); Stable / CARE MAU A1(SO)">CARE MAU A (SO); Stable / CARE MAU
                            A1(SO)</option>
                        <option value="CARE MAU A- (SO); Stable">CARE MAU A- (SO); Stable</option>
                        <option value="CARE MAU A- (SO); Stable /CARE MAU BBB+; Stable">CARE MAU A- (SO); Stable
                            /CARE MAU BBB+; Stable</option>
                        <option value="CARE MAU A-; CWD">CARE MAU A-; CWD</option>
                        <option value="CARE MAU A-; Stable">CARE MAU A-; Stable</option>
                        <option value="CARE MAU A-; Stable/ CARE MAU A2+">CARE MAU A-; Stable/ CARE MAU A2+</option>
                        <option value="CARE MAU A-;Stable">CARE MAU A-;Stable</option>
                        <option value="CARE MAU A; Positive">CARE MAU A; Positive</option>
                        <option value="CARE MAU A; Stable">CARE MAU A; Stable</option>
                        <option value="CARE MAU A; Stable / CARE MAU A1">CARE MAU A; Stable / CARE MAU A1</option>
                        <option value="CARE MAU A; Stable / CARE MAU BBB+; stable">CARE MAU A; Stable / CARE MAU
                            BBB+; stable</option>
                        <option value="CARE MAU A; Stable/ CARE MAU A-; Stable">CARE MAU A; Stable/ CARE MAU A-;
                            Stable</option>
                        <option value="CARE MAU A+ (SO); Stable">CARE MAU A+ (SO); Stable</option>
                        <option value="CARE MAU A+ (SO); Stable / CARE MAU A1 (SO)">CARE MAU A+ (SO); Stable / CARE
                            MAU A1 (SO)</option>
                        <option value="CARE MAU A+; Stable">CARE MAU A+; Stable</option>
                        <option value="CARE MAU A+; Stable / CARE MAU A1">CARE MAU A+; Stable / CARE MAU A1</option>
                        <option value="CARE MAU A+; Stable /Withdrawn">CARE MAU A+; Stable /Withdrawn</option>
                        <option value="CARE MAU A+; Stable/ CARE MAU A1+">CARE MAU A+; Stable/ CARE MAU A1+</option>
                        <option value="CARE MAU A1">CARE MAU A1</option>
                        <option value="CARE MAU A1 (SO)">CARE MAU A1 (SO)</option>
                        <option value="CARE MAU A1+">CARE MAU A1+</option>
                        <option value="CARE MAU A2">CARE MAU A2</option>
                        <option value="CARE MAU A2+">CARE MAU A2+</option>
                        <option value="CARE MAU A2+/ CARE MAU A-; Stable">CARE MAU A2+/ CARE MAU A-; Stable</option>
                        <option value="CARE MAU AA (SO);Stable">CARE MAU AA (SO);Stable</option>
                        <option value="CARE MAU AA- (SO);Stable">CARE MAU AA- (SO);Stable</option>
                        <option value="CARE MAU AA-; Positive">CARE MAU AA-; Positive</option>
                        <option value="CARE MAU AA-; Stable">CARE MAU AA-; Stable</option>
                        <option value="CARE MAU AA-; Stable/ CARE MAU A1+">CARE MAU AA-; Stable/ CARE MAU A1+
                        </option>
                        <option value="CARE MAU AA-;Stable">CARE MAU AA-;Stable</option>
                        <option value="CARE MAU AA; Positive">CARE MAU AA; Positive</option>
                        <option value="CARE MAU AA; Positive/ CARE MAU A1+">CARE MAU AA; Positive/ CARE MAU A1+
                        </option>
                        <option value="CARE MAU AA; Stable">CARE MAU AA; Stable</option>
                        <option value="CARE MAU AA; Stable/ CARE MAU A1+">CARE MAU AA; Stable/ CARE MAU A1+</option>
                        <option value="CARE MAU AA+ (IS); Stable">CARE MAU AA+ (IS); Stable</option>
                        <option value="CARE MAU AAA (Is); Stable">CARE MAU AAA (Is); Stable</option>
                        <option value="CARE MAU AAA; Stable">CARE MAU AAA; Stable</option>
                        <option value="CARE MAU BBB; Stable">CARE MAU BBB; Stable</option>
                        <option value="CARE MAU BBB+; CWD">CARE MAU BBB+; CWD</option>
                        <option value="CARE MAU BBB+; Stable">CARE MAU BBB+; Stable</option>
                        <option value="Withdrawn">Withdrawn</option>
                    </select>

                    <select name="countrynames">
                        <option value="">Select Country</option>
                        <option value="Mauritius">Mauritius</option>
                    </select>
                    <button class="btn btn-primary btn-default">Search</button>
                </form>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Company</th>
                                <th>Rating</th>
                                <th>Latest Review Date</th>
                                <th>Country</th>
                                <th>Sector</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="searchc90b.html?Id=xAOOnQB3BfE9QYWzbTVqeg==">Alteo Limited</a></td>
                                <td>CARE MAU A (SO); Stable</td>
                                <td> May 11, 2023</td>
                                <td>Mauritius</td>
                                <td>Sugar</td>
                            </tr>
                            <tr>
                                <td><a href="search6e55.html?Id=Ndf6D35i0E9eSCFDYlQ6NQ==">Anahita Hotel Limited</a>
                                </td>
                                <td>CARE MAU A; Stable</td>
                                <td> Dec 21, 2023</td>
                                <td>Mauritius</td>
                                <td>Hospitality</td>
                            </tr>
                            <tr>
                                <td><a href="searchc117.html?Id=wfsKJ+j1l9LKaT82S/t9rQ==">Ascencia Limited</a></td>
                                <td>CARE MAU AA-;Stable</td>
                                <td> Nov 9, 2023</td>
                                <td>Mauritius</td>
                                <td>Real Estate</td>
                            </tr>
                            <tr>
                                <td><a href="searche6e9.html?Id=CO2nmgKE5y8deHlzDfgfiw==">Attitude Hospitality
                                        Ltd</a></td>
                                <td>CARE MAU A-; Stable</td>
                                <td> Mar 14, 2024</td>
                                <td>Mauritius</td>
                                <td>Hotels &amp; Hospitality</td>
                            </tr>
                            <tr>
                                <td><a href="searchb7f1.html?Id=PZ86TN0+jCiYLvRzlR03lw==">Attitude Property Ltd</a>
                                </td>
                                <td>CARE MAU A-; Stable</td>
                                <td> Mar 19, 2024</td>
                                <td>Mauritius</td>
                                <td>Hospitality</td>
                            </tr>
                            <tr>
                                <td><a href="searcha10d.html?Id=K64xYSBtQkTMFLvHkxRHSA==">Axess Limited</a></td>
                                <td>CARE MAU A+; Stable/ CARE MAU A1+</td>
                                <td> Dec 20, 2023</td>
                                <td>Mauritius</td>
                                <td>Not Define</td>
                            </tr>
                            <tr>
                                <td><a href="searchd308.html?Id=GEMxHrMFNdMwtmohXxkPvA==">Bank One Limited</a></td>
                                <td>CARE MAU A+; Stable</td>
                                <td> Jun 29, 2023</td>
                                <td>Mauritius</td>
                                <td>Banks</td>
                            </tr>
                            <tr>
                                <td><a href="search4f94.html?Id=p4Hrkv3OFMDVBrzov4VaLg==">BH Property Investments
                                        Limited</a></td>
                                <td>CARE MAU A; Positive</td>
                                <td> Aug 4, 2023</td>
                                <td>Mauritius</td>
                                <td>Real Estate</td>
                            </tr>
                            <tr>
                                <td><a href="search6110.html?Id=mQP94IMTL6pQyJrhTGU29Q==">C-Care (Mauritius) Ltd</a>
                                </td>
                                <td>CARE MAU AA-; Stable/ CARE MAU A1+</td>
                                <td> Nov 30, 2023</td>
                                <td>Mauritius</td>
                                <td>Hospitality</td>
                            </tr>
                            <tr>
                                <td><a href="search7ea8.html?Id=416t2GnDK02Am2AYy0XDMA==">Chartreuse Group Ltd</a>
                                </td>
                                <td>CARE MAU BBB; Stable</td>
                                <td> Jun 10, 2023</td>
                                <td>Mauritius</td>
                                <td>Not Define</td>
                            </tr>
                            <tr>
                                <td><a href="search18d2.html?Id=Kp1HFQyRMtQwKY2OBZYpyQ==">CIEL Finance Limited</a>
                                </td>
                                <td>CARE MAU A; Stable</td>
                                <td> Nov 30, 2023</td>
                                <td>Mauritius</td>
                                <td>Banks</td>
                            </tr>
                            <tr>
                                <td><a href="searchf75f.html?Id=zhHKODvZuMHt/vN6Xc5VZQ==">CIEL Limited</a></td>
                                <td>CARE MAU AA; Stable/ CARE MAU A1+</td>
                                <td> Nov 30, 2023</td>
                                <td>Mauritius</td>
                                <td>Banks</td>
                            </tr>
                            <tr>
                                <td><a href="searchbc19.html?Id=ZRr1eOreaL97AAuksfeFvQ==">CIM Financial Services
                                        Ltd</a></td>
                                <td>CARE MAU AA; Positive/ CARE MAU A1+</td>
                                <td> Feb 20, 2024</td>
                                <td>Mauritius</td>
                                <td>NBFI</td>
                            </tr>
                            <tr>
                                <td><a href="search6edf.html?Id=PXUt0kwHIXbKs3wvwtTM3A==">City and Beach Hotels
                                        (Mauritius) Limited</a></td>
                                <td>CARE MAU A; Stable</td>
                                <td> Sep 29, 2023</td>
                                <td>Mauritius</td>
                                <td>Hotels &amp; Hospitality</td>
                            </tr>
                            <tr>
                                <td><a href="search2c3a.html?Id=USJSL1DQkWL4cJJX1dPdQA==">CM Diversified Credit
                                        Ltd</a></td>
                                <td>CARE MAU A (SO); Stable / CARE MAU A1 (SO)</td>
                                <td> Dec 20, 2023</td>
                                <td>Mauritius</td>
                                <td>Ceramics &amp; Sanitaryware</td>
                            </tr>
                            <tr>
                                <td><a href="search93bc.html?Id=ToXmumzqPpEsfD0+I4vACw==">CM Structured Finance (1)
                                        Ltd</a></td>
                                <td>Withdrawn</td>
                                <td> Feb 16, 2024</td>
                                <td>Mauritius</td>
                                <td>NBFI</td>
                            </tr>
                            <tr>
                                <td><a href="search53f3.html?Id=199Hye2h8uvhKmLormcIIA==">CM Structured Finance (2)
                                        Ltd</a></td>
                                <td>CARE MAU A (SO); Stable / CARE MAU A1 (SO)</td>
                                <td> Nov 9, 2023</td>
                                <td>Mauritius</td>
                                <td>Ceramics &amp; Sanitaryware</td>
                            </tr>
                            <tr>
                                <td><a href="search8f83.html?Id=OEIcgKK0yPZ2/DDSRbXrHQ==">CM Structured Products (1)
                                        Ltd</a></td>
                                <td>CARE MAU A (SO); Stable</td>
                                <td> Aug 7, 2023</td>
                                <td>Mauritius</td>
                                <td>Not Define</td>
                            </tr>
                            <tr>
                                <td><a href="search83be.html?Id=orVhxFIpMgeQIQum1QC8EQ==">CM Structured Products (2)
                                        Ltd</a></td>
                                <td>CARE MAU A (SO); Stable / CARE MAU A1 (SO)</td>
                                <td> Nov 9, 2023</td>
                                <td>Mauritius</td>
                                <td>Ceramics &amp; Sanitaryware</td>
                            </tr>
                            <tr>
                                <td><a href="search2f78.html?Id=/thGg/MRzJFwcvNv61YAqA==">Commercial Investment
                                        Property Fund Limited</a></td>
                                <td>CARE MAU A-; Stable</td>
                                <td> Nov 7, 2023</td>
                                <td>Mauritius</td>
                                <td>Not Define</td>
                            </tr>
                        </tbody>

                    </table>



                </div>

            </div>

        </div>
    </div>
    <div class="d-flex justify-content-center pagina">
        <nav role="navigation" aria-label="Pagination Navigation" class="flex items-center justify-between">
            <div class="flex justify-between flex-1 sm:hidden">
                <span
                    class="relative inline-flex items-center px-4 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default leading-5 rounded-md">
                    &laquo; Previous
                </span>

                <a href="rating-page4658.html?page=2"
                    class="relative inline-flex items-center px-4 py-2 ml-3 text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 rounded-md hover:text-gray-500 focus:outline-none focus:ring ring-gray-300 focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150">
                    Next &raquo;
                </a>
            </div>

            <div class="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
                <div>
                    <p class="text-sm text-gray-700 leading-5">
                        Showing
                        <span class="font-medium">1</span>
                        to
                        <span class="font-medium">20</span>
                        of
                        <span class="font-medium">71</span>
                        results
                    </p>
                </div>

                <div>
                    <span class="relative z-0 inline-flex shadow-sm rounded-md">

                        <span aria-disabled="true" aria-label="&amp;laquo; Previous">
                            <span
                                class="relative inline-flex items-center px-2 py-2 text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default rounded-l-md leading-5"
                                aria-hidden="true">
                                <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                    <path fill-rule="evenodd"
                                        d="M12.707 5.293a1 1 0 010 1.414L9.414 10l3.293 3.293a1 1 0 01-1.414 1.414l-4-4a1 1 0 010-1.414l4-4a1 1 0 011.414 0z"
                                        clip-rule="evenodd" />
                                </svg>
                            </span>
                        </span>





                        <span aria-current="page">
                            <span
                                class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-500 bg-white border border-gray-300 cursor-default leading-5">1</span>
                        </span>
                        <a href="rating-page4658.html?page=2"
                            class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 hover:text-gray-500 focus:z-10 focus:outline-none focus:ring ring-gray-300 focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150"
                            aria-label="Go to page 2">
                            2
                        </a>
                        <a href="rating-page9ba9.html?page=3"
                            class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 hover:text-gray-500 focus:z-10 focus:outline-none focus:ring ring-gray-300 focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150"
                            aria-label="Go to page 3">
                            3
                        </a>
                        <a href="rating-pagefdb0.html?page=4"
                            class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-700 bg-white border border-gray-300 leading-5 hover:text-gray-500 focus:z-10 focus:outline-none focus:ring ring-gray-300 focus:border-blue-300 active:bg-gray-100 active:text-gray-700 transition ease-in-out duration-150"
                            aria-label="Go to page 4">
                            4
                        </a>


                        <a href="rating-page4658.html?page=2" rel="next"
                            class="relative inline-flex items-center px-2 py-2 -ml-px text-sm font-medium text-gray-500 bg-white border border-gray-300 rounded-r-md leading-5 hover:text-gray-400 focus:z-10 focus:outline-none focus:ring ring-gray-300 focus:border-blue-300 active:bg-gray-100 active:text-gray-500 transition ease-in-out duration-150"
                            aria-label="Next &amp;raquo;">
                            <svg class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20">
                                <path fill-rule="evenodd"
                                    d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z"
                                    clip-rule="evenodd" />
                            </svg>
                        </a>
                    </span>
                </div>
            </div>
        </nav>

    </div>
</section>

<?php include "components/footer.php" ?>