<?php include "components/header.php" ?>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center" id="renderHtmlInsightsBreadcrumSectiondata">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0)">GET RATED</a></li>
                        <!-- <li class="breadcrumb-item"><a href="javascript:void(0)">RATINGS</a></li> -->
                        <li class="breadcrumb-item active" aria-current="page">Publications</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">Publications</h1>
            </div>
        </div>
    </div>
</section>

<section class="care-edge-slider-new" id="renderHtmlRatingReportsSectiondata">
    <div class="container-fluid p-0">
        <div class="row justify-content-center m-0">
            <div class="col-md-12 p-0">
                <div class="d-flex justify-content-md-between flex-column flex-md-row">
                    <div>
                        <h3 class="heading-1 text-dark Title">Publications</h3>
                        <p class="heading-3 mb-4 Text-para"></p>
                    </div>
                    <div class="slider-arrows me-2">
                        <span class="prev-btn swiper-button-lock swiper-button-disabled" tabindex="-1" role="button"
                            aria-label="Previous slide" aria-controls="renderHTMLChildSectionData" aria-disabled="true">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-chevron-left" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z">
                                </path>
                            </svg>
                        </span>
                        <span class="next-btn swiper-button-lock swiper-button-disabled" tabindex="-1" role="button"
                            aria-label="Next slide" aria-controls="renderHTMLChildSectionData" aria-disabled="true">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                class="bi bi-chevron-right" viewBox="0 0 16 16">
                                <path fill-rule="evenodd"
                                    d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z">
                                </path>
                            </svg>
                        </span>
                    </div>
                </div>
                <div
                    class="swiper swiper-publication mt-3 swiper-initialized swiper-horizontal swiper-pointer-events swiper-backface-hidden">
                    <div class="swiper-wrapper" id="renderHTMLChildSectionData" aria-live="polite">
                        <div class="swiper-slide swiper-slide-active" style="width: 257px; margin-right: 45px;">
                            <div class="card card-style-1">
                                <div class="card-body align-items-center">
                                    <p class="heading-3 text-white mb-1">
                                        BIZweek 28 May - Mandatory Ratings of capital market instruments - Saurav
                                    </p>
                                    <a href="https://www.careratingsafrica.com/admin/pdf/BIZweek 28 May - Mandatory Ratings of capital market instruments - Saurav_1698669707.pdf"
                                        target="_blank"
                                        class="btn btn-link primary p-0 d-block text-start mt-4">DOWNLOAD
                                        NOW</a>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide swiper-slide-next" style="width: 257px; margin-right: 45px;">
                            <div class="card card-style-1">
                                <div class="card-body align-items-center">
                                    <p class="heading-3 text-white mb-1">
                                        ESG Article By Aishwarya
                                    </p>
                                    <a href="https://www.careratingsafrica.com/admin/pdf/ESG Article - Aishwarya_1698670943.pdf"
                                        target="_blank"
                                        class="btn btn-link primary p-0 d-block text-start mt-4">DOWNLOAD
                                        NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include "components/footer.php" ?>