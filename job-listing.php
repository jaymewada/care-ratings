<?php include "components/header.php" ?>
<div class="find-ratings job-listing-parent-sec">
    <div class="container-fluid ">
        <form>
            <h3 class="heading-3 text-white m-0 font-regular pe-5">Job Openings</h3>
            <div class="custom-dropdown filter-dropdown btn-white dropdown-menu-outiline dropdown me-5">
                <button class="btn btn-secondary dropdown-toggle" type="button" jf-ext-button-ct="select">
                    <span class="selected-value">Select</span>
                </button>
                <ul class="dropdown-menu" id="dtopdown_gp">
                    <li class="active" data-val="CareEdge Ratings"><a class="dropdown-item" href="javascript:void(0)"
                            data-slide="CareEdgeRatings">CareEdge
                            Ratings</a></li>
                    <li data-val="CareEdge Group"><a class="dropdown-item" href="javascript:void(0)"
                            data-slide="CareEdgeGroup">CareEdge
                            Group</a></li>
                    <li data-val="CareEdge Africa"><a class="dropdown-item" href="javascript:void(0)"
                            data-slide="CareEdgeAfrica">CareEdge
                            Africa</a></li>
                    <li data-val="CareEdge Nepal"><a class="dropdown-item" href="javascript:void(0)"
                            data-slide="CareEdgeNepal">CareEdge
                            Nepal</a></li>
                </ul>
            </div>
            <input type="hidden" id="group_type" name="group_type" value="group_type">
            <input type="hidden" name="_token" id="token_main_job_search_form"
                value="48o4F2VKAWLAsDh91E2wEqw4IE5EqzJ5W7xzFTXK">
            <input type="text" id="main_job_search" class="form-control company-name typeahead"
                placeholder="Search for your position by Function , Branch , Role , Experience " jf-ext-cache-id="7">
        </form>

        <div class="pms-cons-copm d-none">
            <ul class="p-0 m-0">
                <li class="d-flex border-0">
                    <div class="flex-shrink-0">
                        <svg xmlns="http://www.w3.org/2000/svg" width="12.167" height="14.833"
                            viewBox="0 0 12.167 14.833">
                            <g id="ic-actions-file" transform="translate(0.75 0.75)">
                                <path id="Path_18" data-name="Path 18"
                                    d="M11.24,2H5.333A1.333,1.333,0,0,0,4,3.333V14a1.333,1.333,0,0,0,1.333,1.333h8A1.333,1.333,0,0,0,14.667,14V6.613a.627.627,0,0,0-.12-.38L11.78,2.287A.667.667,0,0,0,11.24,2Z"
                                    transform="translate(-4 -2)" fill="none" stroke="#fff" stroke-linecap="round"
                                    stroke-linejoin="bevel" stroke-width="1.5" fill-rule="evenodd"></path>
                            </g>
                        </svg>


                    </div>
                    <div class="flex-grow-1">
                        <a class="m-0 d-block" href="javascript:void(0)">PMS Construction Company</a>
                        <span class="text-small text-light">24TH MAY 2021</span>
                    </div>
                </li>
            </ul>
        </div>

    </div>
</div>

<section class="inner-banner bg-secondary jumbotron ">
    <div class="container-fluid py-5">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-5">
                        <li class="breadcrumb-item"><a href="./">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Job Openings</li>
                    </ol>
                </nav>
                <h1 class="heading-1 text-white">Job Openings</h1>
            </div>
        </div>
    </div>
</section>

<section class="padding-100 find-rating-tab-sec current-opening">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <h3 class="heading-1 text-black ">Current Openings</h3>
                    </div>

                </div>
                <div class="row align-items-center">
                    <div class="col-md-12 col-xl-12 mt-5 pt-2">
                        <ul id="portfolio-rate">
                            <li class="careedge ratings">
                                <a href="https://www.careratingsafrica.com/joblisting/123">
                                </a>
                                <div class="card card-style-1"><a
                                        href="https://www.careratingsafrica.com/joblisting/123">
                                    </a>
                                    <div class="upper-top-part"><a
                                            href="https://www.careratingsafrica.com/joblisting/123">
                                            <span class="text-grey text-small text-uppercase font-semi-bold">4 months
                                                ago</span><br>
                                            <span
                                                class="text-grey text-small text-capitalize font-semi-bold mt-3">mumbai</span>
                                        </a><a href="" target="__blank"><span class="arrow-link float-right m-t20"> <svg
                                                    xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                                    viewBox="0 0 15.28 15.256">
                                                    <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                                        <line id="Line_60" data-name="Line 60" y1="13.182"
                                                            transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                                            stroke-linecap="round" stroke-linejoin="bevel"
                                                            stroke-width="2"></line>
                                                        <path id="Path_51" data-name="Path 51"
                                                            d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                                            transform="translate(-7.26 -3.929)" fill="none"
                                                            stroke="#fff" stroke-linecap="round" stroke-linejoin="bevel"
                                                            stroke-width="2"></path>
                                                    </g>
                                                </svg>
                                            </span></a>
                                    </div>
                                    <div class="card-body">
                                        <span class="text-grey text-small text-capitalize font-semi-bold">test</span>
                                        <p class="heading-3 text-white mb-2 mt-2 font-regular">Analyst</p>
                                        <h4 class="text-16 text-grey mb-2 mt-2 font-regular">1</h4>
                                        <!-- <a href="https://www.careratingsafrica.com/joblisting/123"><h4 class="text-16 text-grey mb-2 mt-2 font-regular">Read More</h4></a> -->
                                    </div>
                                </div>

                            </li>
                            <li class="careedge ratings">
                                <a href="https://www.careratingsafrica.com/joblisting/12345">
                                </a>
                                <div class="card card-style-1"><a
                                        href="https://www.careratingsafrica.com/joblisting/12345">
                                    </a>
                                    <div class="upper-top-part"><a
                                            href="https://www.careratingsafrica.com/joblisting/12345">
                                            <span class="text-grey text-small text-uppercase font-semi-bold">4 months
                                                ago</span><br>
                                            <span
                                                class="text-grey text-small text-capitalize font-semi-bold mt-3">Pune</span>
                                        </a><a href="" target="__blank"><span class="arrow-link float-right m-t20"> <svg
                                                    xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                                    viewBox="0 0 15.28 15.256">
                                                    <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                                        <line id="Line_60" data-name="Line 60" y1="13.182"
                                                            transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                                            stroke-linecap="round" stroke-linejoin="bevel"
                                                            stroke-width="2"></line>
                                                        <path id="Path_51" data-name="Path 51"
                                                            d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                                            transform="translate(-7.26 -3.929)" fill="none"
                                                            stroke="#fff" stroke-linecap="round" stroke-linejoin="bevel"
                                                            stroke-width="2"></path>
                                                    </g>
                                                </svg>
                                            </span></a>
                                    </div>
                                    <div class="card-body">
                                        <span class="text-grey text-small text-capitalize font-semi-bold">test1</span>
                                        <p class="heading-3 text-white mb-2 mt-2 font-regular">Business Development</p>
                                        <h4 class="text-16 text-grey mb-2 mt-2 font-regular">2</h4>
                                        <!-- <a href="https://www.careratingsafrica.com/joblisting/12345"><h4 class="text-16 text-grey mb-2 mt-2 font-regular">Read More</h4></a> -->
                                    </div>
                                </div>

                            </li>
                            <li class="careedge ratings">
                                <a href="https://www.careratingsafrica.com/joblisting/1122">
                                </a>
                                <div class="card card-style-1"><a
                                        href="https://www.careratingsafrica.com/joblisting/1122">
                                    </a>
                                    <div class="upper-top-part"><a
                                            href="https://www.careratingsafrica.com/joblisting/1122">
                                            <span class="text-grey text-small text-uppercase font-semi-bold">4 months
                                                ago</span><br>
                                            <span
                                                class="text-grey text-small text-capitalize font-semi-bold mt-3">Mumbai</span>
                                        </a><a href="now" target="__blank"><span class="arrow-link float-right m-t20">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                                    viewBox="0 0 15.28 15.256">
                                                    <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                                        <line id="Line_60" data-name="Line 60" y1="13.182"
                                                            transform="translate(3.871 0.001)" fill="none" stroke="#fff"
                                                            stroke-linecap="round" stroke-linejoin="bevel"
                                                            stroke-width="2"></line>
                                                        <path id="Path_51" data-name="Path 51"
                                                            d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                                            transform="translate(-7.26 -3.929)" fill="none"
                                                            stroke="#fff" stroke-linecap="round" stroke-linejoin="bevel"
                                                            stroke-width="2"></path>
                                                    </g>
                                                </svg>
                                            </span></a>
                                    </div>
                                    <div class="card-body">
                                        <span class="text-grey text-small text-capitalize font-semi-bold">Test1</span>
                                        <p class="heading-3 text-white mb-2 mt-2 font-regular">Analyst</p>
                                        <h4 class="text-16 text-grey mb-2 mt-2 font-regular">4</h4>
                                        <!-- <a href="https://www.careratingsafrica.com/joblisting/1122"><h4 class="text-16 text-grey mb-2 mt-2 font-regular">Read More</h4></a> -->
                                    </div>
                                </div>

                            </li>
                        </ul>

                        <!-- <div class="pagination-div" aria-label="Page navigation example">
<ul class="pagination">
<li class="page-item">
<a class="page-link" href="#" aria-label="Previous">
<span aria-hidden="true"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-left" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M11.354 1.646a.5.5 0 0 1 0 .708L5.707 8l5.647 5.646a.5.5 0 0 1-.708.708l-6-6a.5.5 0 0 1 0-.708l6-6a.5.5 0 0 1 .708 0z"></path></svg>
</span></span>
</a>
</li>
<li class="page-item"><a class="page-link active" href="#">1</a></li>
<li class="page-item"><a class="page-link" href="#">2</a></li>
<li class="page-item"><a class="page-link" href="#">3</a></li>
<li class="page-item">
<a class="page-link" href="#" aria-label="Next">
<span aria-hidden="true"><span class="next-btn">
<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chevron-right" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z"></path></svg>
</span></span>
</a>
</li>
</ul>
</div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="get-rated-sec" style="background-image: url(images/not-finding-bg.png);" id="job-listing-find-pos">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-11">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="heading-1 text-white ">Not finding your Position?</h2>
                        <hr class="style-2">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-xl-3 col-xs-3 col-sm-3">
                        <div class="card card-style-1 green">
                            <a href="javascript:void(0)" class="arrow-link">
                                <svg xmlns="http://www.w3.org/2000/svg" width="15.28" height="15.256"
                                    viewBox="0 0 15.28 15.256">
                                    <g id="ic-arrows-top" transform="translate(7.998 1.784) rotate(45)">
                                        <line id="Line_60" data-name="Line 60" y1="13.182"
                                            transform="translate(3.871 0.001)" fill="none" stroke="#5dd4c4"
                                            stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></line>
                                        <path id="Path_51" data-name="Path 51"
                                            d="M15.035,7.523,11.678,4.167a.817.817,0,0,0-1.152,0L7.26,7.433"
                                            transform="translate(-7.26 -3.929)" fill="none" stroke="#5dd4c4"
                                            stroke-linecap="round" stroke-linejoin="bevel" stroke-width="2"></path>
                                    </g>
                                </svg>
                            </a>
                            <div class="card-body">
                                <p class="heading-3 text-primary mt-5 font-regular">We help you convert your <br>
                                    data into
                                    actionable <br>
                                    insights
                                </p>
                                <a href="https://app604.workline.hr/Candidate/SignUp.aspx" target="_blank"
                                    class="btn btn-primary mt-2">POST RESUME</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 col-xl-8 offset-xl-1">
                        <p class="heading-2 text-white font-regular">Don't worry ! We have your back in this situation.
                            Post your Resume<br> in our Database and we will get back to
                            you whenever your<br> dream position is vacant.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about-contact-sec padding-100 bg-img"
    style="background-image:url(https://www.careratingsafrica.com/public/frontend-assets/images/about-contact-bg.jpeg);"
    <div="">
    <div class="row justify-content-center">
        <div class="col-lg-11 col-md-12">
            <div class="row gx-5">
                <div class="col-lg-5 col-md-12">
                    <h2 class="heading-1 text-white mt-5 pt-5">Get Started</h2>

                </div>

                <div class="col-lg-7  col-md-12">
                    <div class="contact-form bg-secondary ">


                        <div class="row">
                            <div class="alert alert-success print-contactus-success-msg" style="display:none">
                                <ul></ul>
                            </div>
                            <div class="alert alert-danger print-contactus-error-msg" style="display:none">
                                <ul></ul>
                            </div>
                        </div>
                        <form class="row form-style-1" id="form_contact_us" name="form_contact_us"
                            action="https://www.careratingsafrica.com/contactus" method="post" novalidate="">

                            <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
                                <h4 class="heading-2 text-white">Ask a Query</h4>
                            </div>

                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="text" value="sulabha naware" name="full_name" id="full_name"
                                        class="form-control" placeholder="name@example.com" required=""
                                        data-parsley-trigger="focusout" data-parsley-required-message="Enter Full Name"
                                        jf-ext-cache-id="8">
                                    <label for="full_name">Full Name</label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="text" value="9822540715" name="mobile" id="mobile" class="form-control"
                                        placeholder="1234567890" required="" data-parsley-trigger="focusout"
                                        data-parsley-required-message="Enter Mobile Number" jf-ext-cache-id="9">
                                    <label for="mobile">Mobile Number</label>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-floating">
                                    <input type="email" value="sulabha.naware@evolutionco.com" name="email" id="email"
                                        class="form-control" placeholder="name@example.com" required=""
                                        data-parsley-trigger="focusout" data-parsley-required-message="Enter Email"
                                        jf-ext-cache-id="10">
                                    <label for="floatingInput">Email</label>
                                </div>
                            </div>



                            <div class="col-md-6">
                                <div class="form-floating select-drop">
                                    <select name="company_type" id="company_type" class="form-select" required=""
                                        data-parsley-trigger="focusout"
                                        data-parsley-required-message="Enter Company Type" jf-ext-cache-id="16">
                                        <option value="" selected="">Select Company Type</option>
                                        <option value="Careedge Ratings">Careedge Ratings</option>
                                        <option value="Carredge Group">Carredge Group</option>
                                        <option value="Carredge Advisory and Research">Carredge Advisory and Research
                                        </option>
                                        <option value="Care Risk Solutions">Care Risk Solutions</option>
                                        <option value="Care Ratings Nepal">Care Ratings Nepal</option>
                                        <option value="Care Ratings Africa">Care Ratings Africa</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-floating ">
                                    <input type="text" value="evolution co" name="company_name" id="company_name"
                                        class="form-control" placeholder="1234567890" required=""
                                        data-parsley-trigger="focusout"
                                        data-parsley-required-message="Enter Company Name" jf-ext-cache-id="11">
                                    <label for="floatingInput">Company Name</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-floating select-drop">
                                    <select name="query_type" id="query_type" class="form-select" required=""
                                        data-parsley-trigger="focusout" data-parsley-required-message="Enter Query Type"
                                        jf-ext-cache-id="17">
                                        <option value="" selected="">Select Query Type</option>
                                        <option value="ankur.sachdeva@careratings.com">Ratings</option>
                                        <option value="madan.sabnavis@careratings.com">Gradings</option>
                                        <option value="madan.sabnavis@careratings.com">Economics</option>
                                        <option value="prashant.borole@careratings.com">SME/SSI</option>
                                        <option value="ankur.sachdeva@careratings.com">Research</option>
                                        <option value="meenal.Sikchi@careratings.com">Customer Complaints</option>
                                        <option value="suchita.shirgaonkar@careratings.com">Othsers</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-floating">
                                    <input type="text" name="message" id="message" class="form-control"
                                        placeholder="1234567890" required="" data-parsley-trigger="focusout"
                                        data-parsley-required-message="Enter Message" jf-ext-cache-id="12">
                                    <label for="floatingInput">Message</label>
                                </div>
                            </div>
                            <input type="hidden" name="_token" id="token_contactus_form"
                                value="48o4F2VKAWLAsDh91E2wEqw4IE5EqzJ5W7xzFTXK">


                            <input type="hidden" name="token" id="token">
                            <input type="hidden" name="action" id="action">
                            <div class="form-floating mt-5">
                                <button id="btn_contact_us" type="button" class="btn btn-primary btn-default"
                                    jf-ext-button-ct="submit">Submit</button>
                                <img id="loader_contactus"
                                    src="https://www.careratingsafrica.com/public/frontend-assets/images/loader.gif"
                                    style="display:none;">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<?php include "components/footer.php" ?>